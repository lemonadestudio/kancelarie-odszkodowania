<?php

namespace Lm\CmsBundle\Controller;

use Lm\CmsBundle\Menu\Builder;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;

class MenuController extends Controller {

    public function managedAction($location = 'menu_gorne1') {


        $menu = new Builder();
        $response = $this->render('LmCmsBundle:Menu:' . ($location ? $location : "managed") . '.html.twig', array('location' => $location));

        $menu_cfg = $this->container->getParameter('lm_cms.menu');

        $cache_time = intval($menu_cfg['locations'][$location]['cache']);
        
        return $response;
    }

}
