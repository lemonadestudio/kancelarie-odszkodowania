<?php

namespace Lm\CmsBundle\Controller;

use Lm\CmsBundle\Repository\OfferRepository;
use Lm\CmsBundle\Form\Type\OfferCreateType;
use Lm\CmsBundle\Form\Type\OfferSearchType;
use Lm\CmsBundle\Form\Type\OfferShareType;
use Lm\CmsBundle\Entity\Offer;
use Lm\CmsBundle\Entity\OfferCategory;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Lm\CmsBundle\Entity\Coordinates;
use Lm\CmsBundle\Helper\Cms;
use Lm\CmsBundle\Helper\OfferSearchQuery;
use Lm\CmsBundle\Helper\OfferToJsonConverter;
use Lm\CmsBundle\Util\GeolocationFinder;
use Lm\CmsBundle\Util\GeolocationResultStatus;
use GeoIp2\Database\Reader;

    class OfferController extends Controller 
{
    public function mainAction($page) 
    {
		$request = $this->getRequest();
		
        $ip = $this->container->get('request')->getClientIp();
		$page = $request->get('page');
        
        if ($ip == '::1') {
            $ip = '91.196.10.5';
        }
        $searchData = $this->getSearchData();
        
        $city = $this->getIpLocation($ip);
        if ($searchData['city']) {
            $city = $searchData['city'];
        }
        
        $searchQuery = new OfferSearchQuery($this->getDoctrine(), 
                $this->get('knp_paginator'),
                $this->container->getParameter('google_places_api_key'),
                $city);
        
        
        $searchResults = $searchQuery
                ->searchByKeyword($searchData['keyword'])
                ->searchByCategory($searchData['category'])
                ->searchByCityAndRadius($searchData['city'], $searchData['radius'])
                ->searchByBeeingPromoted($searchData['onlyPromoted'])
                ->getResults($page, 10);
        
        $showSearchRadiusOnMap = !empty($searchData['radius']) && !empty($searchData['city']);
        
        $breadcrumbs = $this->get("white_october_breadcrumbs");
    	$breadcrumbs->addItem("Strona główna", $this->get("router")->generate("lm_cms_main_homepage"));
    	$breadcrumbs->addItem("Firmy", $this->get("router")->generate("lm_cms_offer_main"));
        $offerConverter = new OfferToJsonConverter();
        
        return $this->render(
            'LmCmsBundle:Offer:main.html.twig',
            array(
                'offersList' => $searchResults['offers'],
                'offersJson' => $offerConverter->convertOffers($searchResults['offers']->getItems()),
                'readableSearchParts' => $searchResults['readableSearchParts'],
                'searchRadius' => $showSearchRadiusOnMap ? $searchData['radius'] : '',
                'city' => $searchData['city'],
                'category' => $searchResults['category'],
				'keyword' => $searchData['keyword']
            )
        );
    }
    
    /**
     * @Template()
     */
    public function searchFormAction()
    {
        $categories = $this->getOfferCategoryRepository()
                ->createQueryBuilder('c')
                ->select('c.id, c.name')
                ->orderBy('c.position', 'ASC')
                ->getQuery()
                ->getResult();
        
        $form = $this->createForm(
                new OfferSearchType($this->container, $categories), array());
        
        return array(
            'form' => $form->createView()
        );
    }
    
    /**
     * @Template()
     */
    public function searchByCategoryAction()
    {
        $categories = $this->getOfferCategoryRepository()
                ->createQueryBuilder('c')
                ->select('partial c.{id, name, slug, position}, COUNT(o.id) as offersCount')
                ->join('c.offers', 'o')
                ->andWhere('o.active = 1')
                ->groupBy('c.id')
                ->orderBy('c.position', 'ASC')
                ->setMaxResults(5)
                ->getQuery()
                ->getResult();
        
        return $this->render('LmCmsBundle:Offer:searchByCategory.html.twig', array(
            'categories' => $categories
        ));
    }
    
    /**
     * @Template()
     */
    public function searchByCitiesAction()
    {
        $sql = '
            SELECT c.city, COUNT(c.city) as citiesCount 
            FROM
            (
                SELECT city 
                FROM offer_main_address 

                UNION ALL SELECT city 
                FROM offer_additional_address
            ) c 
            GROUP BY c.city 
            ORDER BY COUNT(c.city) DESC 
            LIMIT 0, 10
        ';
        
        $stmt = $this->getDoctrine()
                ->getManager()
                ->getConnection()
                ->prepare($sql);
        $stmt->execute();
        $cities = $stmt->fetchAll();
        $citiesCount = count($cities);
        
        for ($i = 0; $i < $citiesCount; $i++) {
            $cities[$i]['originalPosition'] = $i + 1;
        }
        
        shuffle($cities);
        
        return array(
            'cities' => $cities
        );
    }
    
    /**
     * @Template
     */
    public function showAction($slug) 
    {
        $offer = $this->getOfferRepository()->findOneBy(array(
            'slug' => $slug,
            'active' => true
        ));
        $request = $this->getRequest();
        $em = $this->getDoctrine()->getManager();
        
        if (!$offer) {
            return $this->redirect($this->get('router')->generate('lm_cms_offer_main'));
        }

        $offer->setView($offer->getView() + 1);
        $em->flush();
        
        $category = null;
        if (count($offer->getCategories()) === 1) {
            $category = $this->getOfferCategory($offer);
        }
        
        $visibleProperties = $this->getOfferVisibleProperties($offer);
        $commentForm = $this->createForm(new \Lm\CmsBundle\Form\Type\OfferCommentType());
        $isCommentPosted = $request->cookies->get('ocmt' . $offer->getId()) == 1 ? true : false;
        $shareForm = $this->createForm(new OfferShareType($this->container));
        
        $today = new \DateTime();
        
        $breadcrumbs = $this->get("white_october_breadcrumbs");
    	$breadcrumbs->addItem("Strona główna", $this->get("router")->generate("lm_cms_main_homepage"));
    	$breadcrumbs->addItem("Firmy", $this->get("router")->generate("lm_cms_offer_main"));
    	$breadcrumbs->addItem(
                $offer->getTitle(), 
                $this->get("router")->generate("lm_cms_offer_show", array(
                    'id' => $offer->getId(),
                    'slug' => $offer->getSlug()
                )));
        $offerConverter = new OfferToJsonConverter();
        
        return array(
            'offer' => $offer,
            'offersJson' => $offerConverter->convertOffers(array($offer)),
            'commentForm' => $commentForm->createView(),
            'isCommentPosted' => $isCommentPosted,
            'shareForm' => $shareForm->createView(),
            'category' => $category,
            'visibleProperties' => $visibleProperties,
            'today' => $today
        );
    }
    
    /**
     * 
     * @Template
     */
    public function showcaseAction($slug, $page) 
    {
        $ip = $this->container->get('request')->getClientIp();
        
        if ($ip == '::1') {
            $ip = '91.196.10.5';
        }
        
        $category = $this->getOfferCategoryRepository()
            ->findOneBy(array(
                'slug' => $slug
            )
        );
        
        $city = $this->getIpLocation($ip);
        
        if (! $category) {
            $city = $slug;
        }
        $searchData = $this->getSearchData();
        $searchQuery = new OfferSearchQuery($this->getDoctrine(), 
                $this->get('knp_paginator'),
                $this->container->getParameter('google_places_api_key'),
                $city);
        if ($searchData['city']) {
            $city = $searchData['city'];
        }
        
        $searchResults = $searchQuery
                ->searchByKeyword($searchData['keyword'])
                ->searchByCategory($category ? $category->getId() : $searchData['category'])
                ->searchByCityAndRadius($searchData['city'], $searchData['radius'])
                ->searchByBeeingPromoted($searchData['onlyPromoted'])
                ->getResults($page, 10);
        
        $showSearchRadiusOnMap = !empty($searchData['radius']) && !empty($searchData['city']);
        $offerConverter = new OfferToJsonConverter();
        
        return $this->render(
            'LmCmsBundle:Offer:main.html.twig',
            array(
                'offersList' => $searchResults['offers'],
                'offersJson' => $offerConverter->convertOffers($searchResults['offers']->getItems()),
                'readableSearchParts' => $searchResults['readableSearchParts'],
                'searchRadius' => $showSearchRadiusOnMap ? $searchData['radius'] : '',
                'category' => $category,
                'city' => $city,
				'keyword' => $searchData['keyword']
            )
        );
    }
    
    public function promoteAction($slug) 
    {
        $request = $this->getRequest();
        $em = $this->getDoctrine()->getManager();
        
        if (!$request->isXmlHttpRequest())
            return new Response();
        
        $offer = $this->getOfferRepository()->findOneBy(array(
            'slug' => $slug,
            'active' => true
        ));
        $result = array(
            'success' => false,
            'message' => ''
        );
        
        if (!$offer) {
            $result['message'] = 'Firma nie istnieje.';
            
            return new JsonResponse($result);
        }
        
        $offer->setPromoted(new \DateTime("now"));
        $offer->setUpdated(new \DateTime("now"));
        $em->flush();
        $result['message'] = 'Firma została oznaczona jako promowana.';
        $result['success'] = true;
        
        return new JsonResponse($result);
    }
    
    public function editOfferRequestAction($slug) 
    {
        $request = $this->getRequest();
        $em = $this->getDoctrine()->getManager();
        
        if (!$request->isXmlHttpRequest())
            return new Response();
        
        $offer = $this->getOfferRepository()->findOneBy(array(
            'slug' => $slug,
            'active' => true
        ));
        $result = array(
            'success' => false,
            'message' => ''
        );
        
        if (!$offer) {
            $result['message'] = 'Firma nie istnieje.';
            return new JsonResponse($result);
        }
        
        $email = $offer->getMainAddress()->getEmail();
        
        if (!isset($email) || empty($email)) {
            $result['message'] = 'Firma nie ma przypisanego adresu e-mail.';
            return new JsonResponse($result);
        }

        if (!$offer->canGenerateNewEditToken()) {
            $result['message'] = $this->get('cms_config')->get('ust_wpis_edycja_info_2');
            return new JsonResponse($result);
        }
        
        $offer->setEditLinkLastRequest(new \DateTime());
        $offer->setUpdated(new \DateTime("now"));
        $editToken = $offer->getEditToken();

        if (empty($editToken)) {
            $editToken = Cms::generateNewRandomToken();
            $offer->setEditToken($editToken);
        }

        $em->flush();
        $result['message'] = $this->get('cms_config')->get('ust_wpis_edycja_info_1');
        $result['success'] = true;
        $this->sendEmailWithNewEditToken($offer, $editToken, $request);
        
        return new JsonResponse($result);
    }
    
    public function deleteOfferRequestAction($slug) 
    {
        $request = $this->getRequest();
        $em = $this->getDoctrine()->getManager();
        
        if (!$request->isXmlHttpRequest())
            return new Response();
        
        $offer = $this->getOfferRepository()->findOneBy(array(
            'slug' => $slug,
            'active' => true
        ));
        $result = array(
            'success' => false,
            'message' => ''
        );
        
        if (!$offer) {
            $result['message'] = 'Firma nie istnieje.';
            return new JsonResponse($result);
        }
        
        $email = $offer->getMainAddress()->getEmail();
        
        if (!isset($email) || empty($email)) {
            $result['message'] = 'Firma nie ma przypisanego adresu e-mail. Aby usunąć firmę skontaktuj się z nami przez formularz kontaktowy.';
            return new JsonResponse($result);
        }
        
        if (!$offer->canGenerateNewDeleteToken()) {
            $result['message'] = $this->get('cms_config')->get('ust_wpis_usun_info_2');
            return new JsonResponse($result);
        }
        
        $offer->setRemoveLinkLastRequest(new \DateTime());
        $offer->setUpdated(new \DateTime("now"));
        $removeToken = $offer->getRemoveToken();

        if (empty($removeToken)) {
            $removeToken = Cms::generateNewRandomToken();
            $offer->setRemoveToken($removeToken);
        }

        $em->flush();
        $result['message'] = $this->get('cms_config')->get('ust_wpis_usun_info_1');
        $result['success'] = true;
        $this->sendEmailWithNewRemoveToken($offer, $removeToken, $request);
        
        return new JsonResponse($result);
    }
    
    public function sendCommentAction($slug) 
    {
        $request = $this->getRequest();
        $em = $this->getDoctrine()->getManager();
        
        if (!$request->isXmlHttpRequest() &&!$request->isMethod('POST')) {
            return new Response();
        }
        
        $offer = $this->getOfferRepository()->findOneBy(array(
            'slug' => $slug,
            'active' => true
        ));
        
        if (!$offer) {
            return new Response();
        }
        
        $response = new Response();
        $comment = new \Lm\CmsBundle\Entity\OfferComment();
        $comment->setOffer($offer);
        $comment_form = $this->createForm(new \Lm\CmsBundle\Form\Type\OfferCommentType(), $comment);
        $isCommentPosted = $request->cookies->get('ocmt' . $offer->getId()) == 1 ? true : false;
        
        if (!$isCommentPosted) {
            $comment_form->bind($request);
            
            if ($comment_form->isValid()) {
                $comment->setClientIp($request->getClientIp());
                $comment->setApproved(false);
                $em->persist($comment);
                $response->headers->setCookie(new \Symfony\Component\HttpFoundation\Cookie('ocmt' . $offer->getId(), '1', time() + (3600 * 24 * 30)));
                $em->flush();
                $isCommentPosted = true;
                $this->sendEmailWithNewCommentInfo($offer, $comment);
            }
        }
        
        $parameters = array(
            'offer' => $offer,
            'isCommentPosted' => $isCommentPosted,
            'commentForm' => $comment_form->createView()
        );
        
        $response->setContent($this->renderView('LmCmsBundle:Offer:comment_form.html.twig', $parameters));
        
        return $response;
    }
    
    public function shareAction($slug) 
    {
        $request = $this->getRequest();
        $em = $this->getDoctrine()->getManager();
        $success = false;
        
        if (!$request->isXmlHttpRequest() &&!$request->isMethod('POST')) {
            return new Response();
        }
        
        $offer = $this->getOfferRepository()->findOneBy(array(
            'slug' => $slug,
            'active' => true
        ));
        
        if (!$offer) {
            return new Response();
        }
        
        $form = $this->createForm(new OfferShareType($this->container));
        $form->bind($request);

        if ($form->isValid()) {
            $data = $form->getData();
            $success = true;
            $this->sendEmailWithOfferLink($offer, $data['email']);
        }
        
        return $this->render(
                'LmCmsBundle:Offer:shareForm.html.twig', 
                array(
                    'offer' => $offer,
                    'form' => $form->createView(),
                    'success' => $success
                ));
    }

    /**
     * @Template
     */
    public function addAction() 
    {
        $form = $this->createForm(new OfferCreateType($this->container), new Offer());
        $request = $this->getRequest();

        if ($request->getMethod() == 'POST') {
            $form->bind($request);
            
            if ($form->isValid()) {
                $em = $this->getDoctrine()->getManager();
                $offer = $form->getData();
                
                // TODO: wywalic local, home z encji
                $address = $offer->local != '' ? $offer->home . '/' . $offer->local : $offer->home;
                
                $offer->getMainAddress()->setStreet($offer->getMainAddress()->getStreet() . ' ' . $address);
                $offer->setConfirmationToken(Cms::generateNewRandomToken());
                $em->persist($offer);
                $em->flush();

                $zmienne = array(
                    'EDYCJA_URL' => $this->get('router')->generate(
                            'admin_lm_cms_offer_edit', array('id' => $offer->getId()), true),
                    'AKTYWACJA_URL' => $this->get('router')->generate(
                            'lm_cms_offer_confirm', array(
                                'id' => $offer->getId(),
                                'token' => $offer->getConfirmationToken()
                            ), true),
                    'FIRMA' => $offer->getTitle(),
                    'WOJEWODZTWO' => $offer->getMainAddress()->getProvince(),
                    'MIASTO' => $offer->getMainAddress()->getCity(),
                    'KOD_POCZTOWY' => $offer->getMainAddress()->getPostCode(),
                    'ADRES' => $offer->getMainAddress()->getStreet(),
                    'EMAIL' => $offer->getMainAddress()->getEmail(),
                    'EMAIL2' => $offer->getMainAddress()->getEmail2(),
                    'WWW' => $offer->getWww(),
                    'TELEFON' => $offer->getMainAddress()->getPhone(),
                    'TELEFON2' => $offer->getMainAddress()->getPhone2(),
                    'SLOWA_KLUCZOWE' => $offer->getKeywords(),
                    'BRANZE' => $offer->getCategoriesAsString(),
                    'OPIS' => $offer->getContent(),
                    'OFERTA' => $offer->getContent2(),
                    'LOGO' => $offer->getWebPath() ? '<img src="http://' . ($request->getHttpHost() . $this->get('templating.helper.assets')->getUrl($offer->getWebPath())) . '" />' : 'brak logo'
                );

                // mail z linkiem aktywacyjnym
                $template = $this->get('cms_email_templates')->getParsed('nowy_wpis_aktywacja', $zmienne);
                $email_do = $offer->getMainAddress()->getEmail();
                $email_od = $this->get('cms_config')->get('ust_mailer_wiadomosc_od');
                $email_kopia = $this->get('cms_config')->get('ust_nowy_wpis_aktywacja_kopia_email');

                if ($email_do) {
                    $message_email = \Swift_Message::newInstance()
                            ->setSubject($template->getTopic())
                            ->setFrom($this->container->getParameter('mailer_user'), $email_od)
                            ->setTo($email_do)
                            ->setBody($template->getContent(), 'text/html')
                            ->addPart(strip_tags($template->getContentTxt()), 'text/plain');

                    if ($email_kopia) {
                        $message_email->setBcc(explode(',', $email_kopia));
                    }

                    try {
                        $ret = $this->get('mailer')->send($message_email);
                    } catch (Exception $e) {}
                }

                // powiadomienie o nowym wpisie
                $template = $this->get('cms_email_templates')->getParsed('nowy_wpis_powiadomienie_admin', $zmienne);
                $email_do = $this->get('cms_config')->get('ust_nowy_wpis_powiadomienie_email');
                $email_od = $this->get('cms_config')->get('ust_mailer_wiadomosc_od');
                $email_kopia = null;

                if ($email_do) {
                    $message_email = \Swift_Message::newInstance()
                            ->setSubject($template->getTopic())
                            ->setFrom($this->container->getParameter('mailer_user'), $email_od)
                            ->setTo($email_do)
                            ->setBody($template->getContent(), 'text/html')
                            ->addPart(strip_tags($template->getContentTxt()), 'text/plain');

                    if ($email_kopia) {
                        $message_email->setBcc(explode(',', $email_kopia));
                    }

                    try {
                        $ret = $this->get('mailer')->send($message_email);
                    } catch (Exception $e) {}
                }
                
                return $this->redirect($this->get('router')->generate('lm_cms_offer_addSuccess'));
            }
        }
        
        $breadcrumbs = $this->get("white_october_breadcrumbs");
    	$breadcrumbs->addItem("Strona główna", $this->get("router")->generate("lm_cms_main_homepage"));
    	$breadcrumbs->addItem("Firmy", $this->get("router")->generate("lm_cms_offer_main"));
    	$breadcrumbs->addItem("Dodaj firmę", $this->get("router")->generate("lm_cms_offer_add"));
        
        return array(
            'form' => $form->createView()
        );
    }
    
    /**
     * @Template
     */
    public function addSuccessAction() 
    {
        return array();
    }

    /**
     * @Template
     */
    public function add_thanksAction() 
    {
        return array();
    }

    /**
     * @Template
     */
    public function editAction($id, $token = null) 
    {
        $offer = $this->getOfferRepository()->findOneBy(array(
            'id' => $id,
            'active' => true
        ));

        if (!$offer) {
            return $this->redirect($this->get('router')->generate('lm_cms_offer_main'));
        }

        $edit_token = $offer->getEditToken();

        if (empty($edit_token)) {
            return $this->redirect($this->get('router')->generate('lm_cms_offer_show', array(
                'id' => $offer->getId(),
                'slug' => $offer->getSlug()
            )));
        }

        $token_valid = false;
        if (!empty($token)) {
            // poprawny token, zapamiętanie
            if ($token == $edit_token) {
                $this->get('session')->set('edit_token_' . $offer->getId(), $token);
                // przekierowanie na link bez parametru token
                return $this->redirect($this->get('router')->generate('lm_cms_offer_edit', array('id' => $offer->getId())));
            }
        } else {
            // jeżeli request bez tokenu w linku
            // bierzemy zapisany w sesji token edycji
            $token_session = $this->get('session')->get('edit_token_' . $offer->getId());

            if ($token_session == $edit_token) {
                $token_valid = true;
            }
        }

        if (!$token_valid) {
            $this->get('session')->getFlashBag()->add('notice', $this->get('cms_config')->get('ust_wpis_edycja_info_3'));

            return $this->redirect($this->get('router')->generate('lm_cms_offer_show', array(
                'id' => $offer->getId(),
                'slug' => $offer->getSlug()
            )));
        }

        $street = array_reverse(explode(' ', $offer->getMainAddress()->getStreet()));
        $home = $street[0];
        unset($street[0]);
        $street = implode(' ', array_reverse($street));
        $home = explode('/', $home);
        $offer->getMainAddress()->setStreet($street);
        $local = count($home) > 1 ? $home[1] : '';
        $home = $home[0];

        $offer->local = $local;
        $offer->home = $home;
        $offer->setUpdated(new \DateTime());

        $form = $this->createForm(new OfferCreateType($this->container, true), $offer);
        $rules = $form->get('rules')->setData(true);
        $request = $this->getRequest();
        $message_status = '';

        if ($request->getMethod() == 'POST') {
            $form->bind($request);

            if ($form->isValid()) {
                $em = $this->getDoctrine()->getManager();
                $offer = $form->getData();
                $address = $offer->local != '' ? $offer->home . '/' . $offer->local : $offer->home;
                $offer->getMainAddress()->setStreet($offer->getMainAddress()->getStreet() . ' ' . $address);
                $em->merge($offer);

                $em->flush();
                $this->get('session')->getFlashBag()->add('notice', $this->get('cms_config')->get('ust_wpis_edycja_info_4'));

                return $this->redirect($this->get('router')->generate('lm_cms_offer_edit', array('id' => $offer->getId())));
            }
        }
        
        $breadcrumbs = $this->get("white_october_breadcrumbs");
    	$breadcrumbs->addItem("Strona główna", $this->get("router")->generate("lm_cms_main_homepage"));
    	$breadcrumbs->addItem("Firmy", $this->get("router")->generate("lm_cms_offer_main"));
    	$breadcrumbs->addItem(
                'Edycja firmy "' . $offer->getTitle() . '"', 
                $this->get("router")->generate("lm_cms_offer_edit", array(
                    'id' => $offer->getId(),
                    'token' => $edit_token
                )));
        
        return array(
            'offer' => $offer,
            'form' => $form->createView(),
            'message' => $message_status
        );
    }

    public function removeAction($id, $token = null) 
    {
        $offer = $this->getOfferRepository()->findOneBy(array(
            'id' => $id,
            'active' => true
        ));

        if (!$offer) {
            return $this->redirect($this->get('router')->generate('lm_cms_offer_main'));
        }

        $remove_token = $offer->getRemoveToken();

        if (empty($remove_token)) {
            return $this->redirect($this->get('router')->generate('lm_cms_offer_show', array(
                'id' => $offer->getId(),
                'slug' => $offer->getSlug()
            )));
        }

        $token_valid = false;

        if (!empty($token)) {
            // poprawny token, zapamiętanie
            if ($token == $remove_token) {
                // można usunąć / ukryć dane
                $offer->setActive(false);
                $offer->setUpdated(new \DateTime("now"));
                $em = $this->getDoctrine()->getManager();
                $em->flush();

                /*
                 *  <li>{EDYCJA_URL} - Link do edycji wpisu</li>
                  <li>{FIRMA} - Nazwa firmy </li>
                  <li>{IP} - Adres IP z którego dokonano kliknięcia w link usuwający dane</li>
                 */

                $zmienne = array(
                    'EDYCJA_URL' => $this->get('router')->generate(
                            'admin_lm_cms_offer_edit', array('id' => $offer->getId(),
                            ), true),
                    'FIRMA' => $offer->getTitle(),
                    'IP' => $this->getRequest()->getClientIp(),
                );

                // powiadomienie o usuniętym (ukrytym wpisie)
                $template = $this->get('cms_email_templates')->getParsed('usun_wpis_powiadomienie_admin', $zmienne);
                $email_do = $this->get('cms_config')->get('ust_wpis_usun_powiadomienie_email');
                $email_od = $this->get('cms_config')->get('ust_mailer_wiadomosc_od');
                $email_kopia = null;

                if ($email_do) {
                    $message_email = \Swift_Message::newInstance()
                            ->setSubject($template->getTopic())
                            ->setFrom($this->container->getParameter('mailer_user'), $email_od)
                            ->setTo($email_do)
                            ->setBody($template->getContent(), 'text/html')
                            ->addPart(strip_tags($template->getContentTxt()), 'text/plain');

                    if ($email_kopia) {
                        $message_email->setBcc(explode(',', $email_kopia));
                    }

                    try {
                        $ret = $this->get('mailer')->send($message_email);
                    } catch (Exception $e) {
                        
                    }
                }

                // przekierowanie na stronę główną
                return $this->redirect($this->get('router')->generate('lm_cms_offer_main'));
            } else {
                $this->get('session')->getFlashBag()->add('notice', $this->get('cms_config')->get('ust_wpis_usun_info_3'));
            }
        }

        return $this->redirect($this->get('router')->generate('lm_cms_offer_show', array(
            'id' => $offer->getId(),
            'slug' => $offer->getSlug()
        )));
    }

    public function confirmAction($id, $token) 
    {
        $offer = $this->getOfferRepository()->findOneBy(array(
            'id' => $id
        ));

        if (!$offer) {
            return $this->redirect($this->get('router')->generate('lm_cms_offer_main'));
        }

        if ($offer->getConfirmationToken() == $token) {
            if ($offer->getActive() == false) {
                $offer->setActive(true);
                $offer->setUpdated(new \DateTime("now"));
                $em = $this->getDoctrine()->getManager();
                $em->flush();
                
                $this->get('session')->getFlashBag()->add('notice', $this->get('cms_config')->get('ust_nowy_wpis_info_1'));
            } else {
                $this->get('session')->getFlashBag()->add('notice', $this->get('cms_config')->get('ust_nowy_wpis_info_2'));
            }
        } else {
            $this->get('session')->getFlashBag()->add('notice', $this->get('cms_config')->get('ust_nowy_wpis_info_3'));
        }

        return $this->redirect($this->get('router')->generate('lm_cms_offer_show', array(
            'id' => $offer->getId(),
            'slug' => $offer->getSlug()
        )));
    }
    
    private function getSearchData() 
    {
        $request = $this->getRequest();
        
        // $fakeCategories to tablica z 1 kategoria jezeli user wyszukuje po kategorii.
        // Ta tablica zostaje przekazana do OfferSearchType zamiast wszystkich kategorii
        // (tak jak w metodzie "searchFormAction"), zeby niepotrzebnie nie pobierac elementow
        // z tabeli kategorii
        $fakeCategories = array();
        $category = $request->query->get('category');
        
        if (isset($category) && !empty($category)) {
            $fakeCategories[$category] = array(
                'id' => $category,
                'name' => ''
            );
        }
        
        // formularz potrzebny tylko do zbindowania danych
        $searchForm = $this->createForm(
                new OfferSearchType($this->container, $fakeCategories, ''), 
                array()
            );
        $searchForm->bind($request);
        
        // array keys:
        // city, category, radius, keyword, onlyPromoted
        return $searchForm->getData();
    }
    
    private function sendEmailWithNewEditToken($offer, $token, $request) 
    {
        $zmienne = array(
            'POKAZ_URL' => $this->get('router')->generate(
                    'lm_cms_offer_show', array('id' => $offer->getId(), 'slug' => $offer->getSlug()), true),
            'EDYCJA_URL' => $this->get('router')->generate(
                    'lm_cms_offer_edit', array('id' => $offer->getId(),
                'token' => $token
                    ), true),
            'FIRMA' => $offer->getTitle(),
            'EMAIL' => $offer->getMainAddress()->getEmail(),
            'IP' => $request->getClientIp()
        );

        // mail z linkiem aktywacyjnym
        $template = $this->get('cms_email_templates')->getParsed('wpis_edycja_link', $zmienne);
        $email_do = $offer->getMainAddress()->getEmail();
        $email_od = $this->get('cms_config')->get('ust_mailer_wiadomosc_od');
        $email_kopia = $this->get('cms_config')->get('ust_wpis_edycja_link_kopia_email');

        if ($email_do) {
            $message_email = \Swift_Message::newInstance()
                    ->setSubject($template->getTopic())
                    ->setFrom($this->container->getParameter('mailer_user'), $email_od)
                    ->setTo($email_do)
                    ->setBody($template->getContent(), 'text/html')
                    ->addPart(strip_tags($template->getContentTxt()), 'text/plain');

            if ($email_kopia) {
                $message_email->setBcc(explode(',', $email_kopia));
            }

            try {
                $ret = $this->get('mailer')->send($message_email);
            } catch (Exception $e) {}
        }
    }
    
    private function sendEmailWithNewRemoveToken($offer, $token, $request) 
    {
        $zmienne = array(
            'POKAZ_URL' => $this->get('router')->generate(
                    'lm_cms_offer_show', array('id' => $offer->getId(),'slug' => $offer->getSlug()), true),
            'USUN_URL' => $this->get('router')->generate(
                    'lm_cms_offer_remove', array('id' => $offer->getId(),
                'token' => $token
                    ), true),
            'FIRMA' => $offer->getTitle(),
            'EMAIL' => $offer->getMainAddress()->getEmail(),
            'IP' => $request->getClientIp()
        );

        // mail z linkiem aktywacyjnym
        $template = $this->get('cms_email_templates')->getParsed('wpis_usun_link', $zmienne);

        $email_do = $offer->getMainAddress()->getEmail();
        $email_od = $this->get('cms_config')->get('ust_mailer_wiadomosc_od');
        $email_kopia = $this->get('cms_config')->get('ust_wpis_usun_link_kopia_email');

        if ($email_do) {
            $message_email = \Swift_Message::newInstance()
                    ->setSubject($template->getTopic())
                    ->setFrom($this->container->getParameter('mailer_user'), $email_od)
                    ->setTo($email_do)
                    ->setBody($template->getContent(), 'text/html')
                    ->addPart(strip_tags($template->getContentTxt()), 'text/plain');

            if ($email_kopia) {
                $message_email->setBcc(explode(',', $email_kopia));
            }

            try {
                $ret = $this->get('mailer')->send($message_email);
            } catch (Exception $e) {}
        }
    }
    
    private function sendEmailWithNewCommentInfo($offer, $comment) 
    {
        $zmienne = array(
            'EDYCJA_URL' => $this->get('router')->generate(
                    'admin_lm_cms_offercomment_edit', array('id' => $comment->getId(),
                    ), true),
            'FIRMA' => $offer->getTitle(),
            'FIRMA_URL' => $this->get('router')->generate(
                    'lm_cms_offer_show', array('id' => $offer->getId(),'slug' => $offer->getSlug(),
                    ), true),
            'KOMENTARZ' => $comment->getComment(),
            'DATA_ZAKUPU' => $comment->getBuyDate(),
            'PRZEDMIOT_ZAKUPU' => $comment->getBuyObject(),
            'IP' => $comment->getClientIp(),
            'POZYTYWNY' => $comment->getPositive() ? 'Tak' : 'Nie',
            'DATA_DODANIA' => $comment->getCreated()->format('Y-m-d'),
        );
        
        // mail z informacją o komentarzu do admina
        $template = $this->get('cms_email_templates')->getParsed('nowy_komentarz_powiadomienie', $zmienne);
        $email_do = $this->get('cms_config')->get('ust_komentarz_powiadomienie_email');
        $email_od = $this->get('cms_config')->get('ust_mailer_wiadomosc_od');
        
        if ($email_do) {
            $message_email = \Swift_Message::newInstance()
                    ->setSubject($template->getTopic())
                    ->setFrom($this->container->getParameter('mailer_user'), $email_od)
                    ->setTo($email_do)
                    ->setBody($template->getContent(), 'text/html')
                    ->addPart(strip_tags($template->getContentTxt()), 'text/plain');

            try {
                $ret = $this->get('mailer')->send($message_email);
            } catch (Exception $e) {}
        }
        
        // mail z informacją o komentarzu do admina
        $template = $this->get('cms_email_templates')->getParsed('nowy_komentarz_powiadomienie_klient', $zmienne);
        $email_do = $offer->getMainAddress()->getEmail();
        $email_od = $this->get('cms_config')->get('ust_mailer_wiadomosc_od');
        
        if ($email_do && $template->getContent()) {
            $message_email = \Swift_Message::newInstance()
                    ->setSubject($template->getTopic())
                    ->setFrom($this->container->getParameter('mailer_user'), $email_od)
                    ->setTo($email_do)
                    ->setBody($template->getContent(), 'text/html')
                    ->addPart(strip_tags($template->getContentTxt()), 'text/plain');

            try {
                $ret = $this->get('mailer')->send($message_email);
            } catch (Exception $e) {}
        }
    }
    
    private function sendEmailWithOfferLink($offer, $sendToEmail) 
    {
        $zmienne = array(
            'COMPANY_NAME' => $offer->getTitle(),
            'COMPANY_CITY' => $offer->getMainAddress()->getCity(),
            'COMPANY_URL' => $this->get('router')->generate(
                    'lm_cms_offer_show', array('id' => $offer->getId(),'slug' => $offer->getSlug(),
                    ), true)
        );

        $template = $this->get('cms_email_templates')->getParsed('offer_share', $zmienne);
        $email_od = $this->get('cms_config')->get('ust_mailer_wiadomosc_od');

        if ($sendToEmail) {
            $message_email = \Swift_Message::newInstance()
                    ->setSubject($template->getTopic())
                    ->setFrom($this->container->getParameter('mailer_user'), $email_od)
                    ->setTo($sendToEmail)
                    ->setBody($template->getContent(), 'text/html')
                    ->addPart(strip_tags($template->getContentTxt()), 'text/plain');

            try {
                $ret = $this->get('mailer')->send($message_email);
            } catch (Exception $e) {}
        }
    }
    
    private function getOfferCategory($offer) 
    {   
        return $this->getOfferCategoryRepository()
                ->createQueryBuilder('c')
                ->select('c, COUNT(o.id) AS HIDDEN offersCount')
                ->join('c.offers', 'o')
                ->where('o.id = :offerId')
                ->orderBy('offersCount', 'DESC')
                ->setMaxResults(1)
                ->setParameter('offerId', $offer->getId())
                ->getQuery()
                ->getOneOrNullResult();
    }
    
    private function getOfferVisibleProperties($offer) 
    {   
        $properties = $this->getOfferRepository()
                ->createQueryBuilder('o')
                ->select('vp.type')
                ->join('o.visibleProperties', 'vp')
                ->where('o.id = :offerId')
                ->setParameter('offerId', $offer->getId())
                ->getQuery()
                ->getArrayResult();
        
        return array_map('current', $properties);
    }
    
    private function getOfferRepository() 
    {
        return $this->getDoctrine()->getRepository('LmCmsBundle:Offer');
    }
    
    private function getOfferCategoryRepository() 
    {
        return $this->getDoctrine()->getRepository('LmCmsBundle:OfferCategory');
    }
    
    /**
     * Returns name of the city, depending on ip from given parameter.
     * @param string $ip
     * @return string
     */
    public function getIpLocation($ip) 
    {
        $reader = new Reader('../vendor/geoip2/geoip2/maxmind-db/GeoLite2-City.mmdb');

        $location = $reader->city($ip);

        $city = $location->city->name;
        
        return $city;
    }
}
