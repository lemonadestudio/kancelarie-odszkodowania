<?php

namespace Lm\CmsBundle\Controller;


use Lm\CmsBundle\Entity\Box;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;

/**
 * @Template()
 */
class BoxController extends Controller
{

	/**
	 * @Template()
	 */
	public function showAction($slug) {

		$box = $this->getDoctrine()->getRepository('LmCmsBundle:Box')
		->findOneBy(array('slug' => $slug));

		if(!$box) {


			$box = new Box();
			$box->setTitle($slug);

			$box->setContent('<p>Box '.$slug.'</p><p>edytuj w panelu administracyjnym</p>');
			$box->setPublished(true);
			$box->setSlug($slug);
			$this->getDoctrine()->getEntityManager()->persist($box);
			$this->getDoctrine()->getEntityManager()->flush();

		}

		return array(
				'box' => $box,
		);

	}

}
