<?php

namespace Lm\CmsBundle\Controller;

use Lm\CmsBundle\Entity\Page;
use Lm\CmsBundle\Entity\Categories;
use Lm\CmsBundle\Repository\PageRepository;
use Lm\CmsBundle\Form\Type\ContactType;
use Lm\CmsBundle\Entity\ContactFormMessage;
use Symfony\Component\Finder\SplFileInfo;
use Symfony\Component\Finder\Finder;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\Response;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Serializer\Encoder\JsonEncoder;
use Lm\CmsBundle\Entity\News;
use Lm\CmsBundle\Menu\Builder;
use Lm\CmsBundle\Repository\OfferRepository;
use Lm\CmsBundle\Entity\Offer;
use Lm\CmsBundle\Entity\OfferCategory;
use Symfony\Component\Locale\Locale;

class ImportController extends Controller {
    private $createdCategories;
    private $notFoundOffers;
    private $offerDuplicates;
    private $foundOffersCount;
    private $maxLinesToParse;
    
    public function importCategoriesAction($fileType) {
        $this->createdCategories = array();
        $this->notFoundOffers = array();
        $this->offerDuplicates = array();
        $this->foundOffersCount = 0;
        $this->maxLinesToParse = 300;
        
        $startTime = microtime(true);
        //$this->doImport($fileType);
        $endTime = microtime(true);
        $diff = round($endTime - $startTime, 4);
        
        return new Response('Czas importu to: ' . $diff . 'sek');
    }

    private function doImport($fileType)
    {
        $doctrine = $this->getDoctrine();
        $em = $doctrine->getEntityManager();
        $offerRepository = $doctrine->getRepository('LmCmsBundle:Offer');
        $offerCategoryRepository = $doctrine->getRepository('LmCmsBundle:OfferCategory');
        
        $testServicesFileInfo = new \SplFileInfo('../app/categoriesToImport/mazowieckie__us_ugi_2013-07-051_short.csv');
        $tradeFileInfo = new \SplFileInfo('../app/categoriesToImport/mazowieckie___handel_2013-07-05.csv');
        $services1FileInfo = new \SplFileInfo('../app/categoriesToImport/mazowieckie___us_ugi_2013-07-050.csv');
        $services2FileInfo = new \SplFileInfo('../app/categoriesToImport/mazowieckie__us_ugi_2013-07-051.csv');
        
        switch ($fileType) {
            case 'test':
                if ($testServicesFileInfo->isFile()) {
                    $this->importFromFile($testServicesFileInfo, $offerRepository, $offerCategoryRepository, $em, OfferCategory::TYPE_SERVICE, 'testServices');
                }
                break;
            case 'trade':
                if ($tradeFileInfo->isFile()) {
                    $this->importFromFile($tradeFileInfo, $offerRepository, $offerCategoryRepository, $em, OfferCategory::TYPE_TRADE, 'trade');
                }
                break;
            case 'service1':
                if ($services1FileInfo->isFile()) {
                    $this->importFromFile($services1FileInfo, $offerRepository, $offerCategoryRepository, $em, OfferCategory::TYPE_SERVICE, 'service1');
                }
                break;
            case 'service2':
                if ($services2FileInfo->isFile()) {
                    $this->importFromFile($services2FileInfo, $offerRepository, $offerCategoryRepository, $em, OfferCategory::TYPE_SERVICE, 'service2');
                }
                break;
        }
    }
    
    private function importFromFile(\SplFileInfo $fileInfo, $offerRepository, $offerCategoryRepository, $em, $categoryType, $logFilePrefix) {
        $file = new \SplFileObject($fileInfo->getRealpath());
        $file->setFlags(\SplFileObject::SKIP_EMPTY | \SplFileObject::DROP_NEW_LINE | \SplFileObject::READ_CSV);
        $file->setCsvControl(';');
        
        $startLineFileName = $logFilePrefix . '_startFromLine.txt';
        $startLineFile = new \SplFileObject('../app/categoriesToImport/' . $startLineFileName, 'c+');
        $startLine = $startLineFile->current();
        
        if (empty($startLine)) {
            // pierwsza linijka to nazwy kolumn
            $startLine = 2;
        }
        
        echo 'starting from line: ' . $startLine . '<br>';
        
        $file->seek($startLine - 1);
        
        if (!$file->valid()) {
            echo 'imported whole file';
            return;
        }
        
        $currentLine = $startLine;
        $importToLine = $startLine + $this->maxLinesToParse;
        
        while (!$file->eof() && $currentLine < $importToLine) {
            list($companyName, $street, $city, $postCode, $phone, $email, $website, 
                    $province, $category, $subCategory) = $file->current();
            
            $companyName = iconv('ISO-8859-2', 'UTF-8//TRANSLIT', trim($companyName));
            $category = iconv('ISO-8859-2', 'UTF-8//TRANSLIT', trim($category));

            if (!empty($companyName) && !empty($category)) {
                $offerCategory = $this->getOrCreateCategory($offerCategoryRepository, $category, $em, $categoryType);
                $this->assignCategoryToOffers($companyName, $offerRepository, $em, $offerCategory);
                
                //echo $companyName . '<br>';
            }
            
            $file->next();
            $currentLine++;
        }
        
        $startLineFile->rewind();
        $startLineFile->fwrite($currentLine);
        
        $this->createLog($logFilePrefix);
    }
    
    private function getOrCreateCategory($offerCategoryRepository, $categoryName, $em, $categoryType) {
        $offerCategory = $offerCategoryRepository->findOneBy(array(
            'name' => $categoryName
        ));
        
        if (!$offerCategory) {
            $offerCategory = new OfferCategory();
            $offerCategory->setName($categoryName);
            $offerCategory->setType($categoryType);
            $em->persist($offerCategory);
            $em->flush();
            
            $this->createdCategories[] = $categoryName;
        }
        
        return $offerCategory;
    }
    
    private function assignCategoryToOffers($offerName, $offerRepository, $em, $offerCategory) {
        $offers = $offerRepository->findBy(array(
            'title' => $offerName
        ));
        $offersCount = count($offers);
        
        if ($offersCount == 0) {
            $this->notFoundOffers[] = $offerName;
        }
        
        if ($offersCount > 1) {
            $this->offerDuplicates[] = array(
                'count' => $offersCount,
                'title' => $offerName
            );
        } else {
            $this->foundOffersCount++;
        }
        
        // zdublowane nazwy firm beda mialy przypisane takie same kategorie
        foreach ($offers as $offer) {
            if (count($offer->getCategories()) > 0) {
                continue;
            }
            
            $offer->addCategorie($offerCategory);
            $em->persist($offer);
        }
        
        $em->flush();
    }
    
    private function createLog($logFilePrefix) {
        $logFileName = $logFilePrefix . '_import-log' . time() . '.txt';
        $logFile = new \SplFileObject("../app/categoriesToImport/logs/{$logFileName}", 'c+');
        $logMessage = "Utworzonych kategorii: " . count($this->createdCategories) . "\r\n";
        
        foreach ($this->createdCategories as $newCategory) {
            $logMessage .= $newCategory . ', ';
        }
        
        $logMessage .= '\r\n\r\n';
        $logMessage.= "Nie znalezionych firm: " . count($this->notFoundOffers) . "\r\n";
        
        foreach ($this->notFoundOffers as $notFoundOffer) {
            $logMessage .= $notFoundOffer . ', ';
        }
        
        $logMessage .= '\r\n\r\n';
        $logMessage.= "Duplikatow firm: " . count($this->offerDuplicates) . "\r\n";
        
        foreach ($this->offerDuplicates as $duplicate) {
            $logMessage .= $duplicate['title'] . ' = ' . $duplicate['count'] . ', ';
        }
        
        $logMessage .= '\r\n\r\n';
        $logMessage.= "Zaktualizowanych firm: " . $this->foundOffersCount . "\r\n";
        $logFile->fwrite($logMessage);
    }
}
