<?php

namespace Lm\CmsBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\RequestStack;
use Lm\CmsBundle\Entity\PopupForm;
use Lm\CmsBundle\Form\Type\PopupFormType;

class PopupFormController extends Controller
{
    public function showAction()
    {
        $form = $this->createForm(new PopupFormType());
        
        return $this->render('LmCmsBundle:PopupForm:show.html.twig', array(
            'form' => $form->createView(),
        ));
    }

    public function sendAction(Request $request)
    {
        $form = $this->createForm(new PopupFormType());
        $template = $this->container->get('templating');
        $data = $request->request->get('PopupFormType');
        
        $contact = $data['contact'];
        $name = $data['name'];
        
        $problem = $data['message'];

        $message = \Swift_Message::newInstance()
            ->setSubject('Zgłoszenie szkody')
            ->setFrom($this->container->getParameter('mailer_user'))
            ->setTo('admin@kancelarie-odszkodowania.pl')
            ->setBody(
                $template->render('LmCmsBundle:PopupForm:popupMail.html.twig', array(
                    'name' => $name,
                    'problem' => $problem,
                    'contact' => $contact
                )),
            'text/html'
        );
        
        // Saving route to admin
        $currentRoute = $request->attributes->get('_route');
        $referer = $request->headers->get('referer');
        $currentUrl = $this->get('router')->generate($currentRoute, array(), true);
        
        $em = $this->getDoctrine()->getManager();
        
        $emailMessage = new PopupForm();
        
        $emailMessage->setName($name);
        $emailMessage->setContact($contact);
        $emailMessage->setUrl($currentUrl);
        
        if (preg_match('/\.|@/', $contact) || preg_match('/\[0-9]|\-|\ /', $contact)) {
            $send = $this->get('mailer')->send($message);
        } else {
            $this->get('session')
                    ->getFlashBag()
                    ->add('informationPopup', 'Wprowadź poprawny numer telefonu lub adres mailowy');
            
            return $this->redirect($referer);
        }
        
        if ($send) {
            
            $em->persist($emailMessage);
            $em->flush();
            
            $this->get("session")
                    ->getFlashBag()
                    ->add("informationPopup", "Wiadomość została wysłana.");
        } else {
            $this->get("session")
                    ->getFlashBag()
                    ->add("informationPopup", "Wystąpił błąd, prosimy spróbować później.");
        }
        
        return $this->redirect($referer);
    }
    
}
