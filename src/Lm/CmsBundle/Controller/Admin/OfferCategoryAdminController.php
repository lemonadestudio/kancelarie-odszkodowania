<?php

namespace Lm\CmsBundle\Controller\Admin;

use Sonata\AdminBundle\Controller\CRUDController as Controller;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\RedirectResponse;

class OfferCategoryAdminController extends Controller
{
    public function editAction($id = null)
    {
        // the key used to lookup the template
        $templateKey = 'edit';

        $id = $this->get('request')->get($this->admin->getIdParameter());

        $object = $this->admin->getObject($id);

        if (!$object) {
            throw new NotFoundHttpException(sprintf('unable to find the object with id : %s', $id));
        }

        if (false === $this->admin->isGranted('EDIT', $object)) {
            throw new AccessDeniedException();
        }

        $this->admin->setSubject($object);

        /** @var $form \Symfony\Component\Form\Form */
        $form = $this->admin->getForm();
        $form->setData($object);

        if ($this->get('request')->getMethod() == 'POST') {
            $form->bind($this->get('request'));

            $isFormValid = $form->isValid();

            // persist if the form was valid and if in preview mode the preview was approved
            if ($isFormValid && (!$this->isInPreviewMode() || $this->isPreviewApproved())) {
                $object->setUpdated(new \DateTime());
                $this->admin->update($object);
                $this->get('session')->setFlash('sonata_flash_success', 'flash_edit_success');

                if ($this->isXmlHttpRequest()) {
                    return $this->renderJson(array(
                        'result'    => 'ok',
                        'objectId'  => $this->admin->getNormalizedIdentifier($object)
                    ));
                }

                // redirect to edit mode
                return $this->redirectTo($object);
            }

            // show an error message if the form failed validation
            if (!$isFormValid) {
                if (!$this->isXmlHttpRequest()) {
                    $this->get('session')->setFlash('sonata_flash_error', 'flash_edit_error');
                }
            } elseif ($this->isPreviewRequested()) {
                // enable the preview template if the form was valid and preview was requested
                $templateKey = 'preview';
            }
        }

        $view = $form->createView();

        // set the theme for the current Admin Form
        $this->get('twig')->getExtension('form')->renderer->setTheme($view, $this->admin->getFormTheme());

        return $this->render($this->admin->getTemplate($templateKey), array(
            'action' => 'edit',
            'form'   => $view,
            'object' => $object,
        ));
    }
    
    public function moveDownAction()
    {
        $request = $this->get('request');
        $em = $this->getDoctrine()->getManager();
        $offerCategoryToMoveDown = $em->getRepository('LmCmsBundle:OfferCategory')
                ->findOneById($request->attributes->get('id'));
        
        $itemsCount = $this->getDoctrine()
            ->getManager()
            ->createQueryBuilder()
            ->select('COUNT(c.id)')
            ->from('LmCmsBundle:OfferCategory', 'c')
            ->getQuery()
            ->getSingleScalarResult();
        
        $maxPosition = $itemsCount + 1;
        $oldPosition = $offerCategoryToMoveDown->getPosition();
        $newPosition = $oldPosition + 1;
        
        if ($newPosition < $maxPosition) {
            $offerCategoryToMoveUp = $em
                    ->getRepository('LmCmsBundle:OfferCategory')
                    ->findOneByPosition($newPosition);
            
            $offerCategoryToMoveDown->setPosition($newPosition);
            $offerCategoryToMoveUp->setPosition($oldPosition);
            
            $em->persist($offerCategoryToMoveUp);
            $em->persist($offerCategoryToMoveDown);
            $em->flush();
        }

        return new RedirectResponse($this->admin->generateUrl('list'));
    }

    public function moveUpAction()
    {
        $request = $this->get('request');
        $em = $this->getDoctrine()->getManager();
        $offerCategoryToMoveUp = $em->getRepository('LmCmsBundle:OfferCategory')
                ->findOneById($request->attributes->get('id'));
        
        $oldPosition = $offerCategoryToMoveUp->getPosition();
        $newPosition = $oldPosition - 1;
        
        if ($newPosition > 0) {
            $offerCategoryToMoveDown = $em
                    ->getRepository('LmCmsBundle:OfferCategory')
                    ->findOneByPosition($newPosition);
            
            $offerCategoryToMoveUp->setPosition($newPosition);
            $offerCategoryToMoveDown->setPosition($oldPosition);
            
            $em->persist($offerCategoryToMoveUp);
            $em->persist($offerCategoryToMoveDown);
            $em->flush();
        }

        return new RedirectResponse($this->admin->generateUrl('list'));
    }
}
