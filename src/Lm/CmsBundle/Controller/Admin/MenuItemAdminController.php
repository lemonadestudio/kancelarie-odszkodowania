<?php

namespace Lm\CmsBundle\Controller\Admin;


use Symfony\Bundle\DoctrineBundle\Registry;

use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Request;
use Sonata\AdminBundle\Controller\CRUDController as Controller;

class MenuItemAdminController extends Controller
{

	public function saveOrderAction() {

		$request = $this->getRequest();
		$elements = $request->get('elements');

		$em = $this->getDoctrine()->getEntityManager();



		$menu_repo = $em->getRepository('LmCmsBundle:MenuItem');

		$order = 0;
		if(is_array($elements)) {
			foreach($elements as $element) {
				$menu_item = $menu_repo -> find($element['item_id']);


				if($menu_item) {
					$menu_item->setLft($element['left']);
					$menu_item->setRgt($element['left']);
					$menu_item->setDepth($element['depth']);

					$parent = $menu_repo->find($element['parent_id']);

					if(!$parent || $element['parent_id'] == 'root') {
						//$menu_item -> setAsRoot();
					} else {
						$menu_item -> setParentItem($parent);
					}
				}

				$em->flush();
			}


		}

		$response = new Response();
		$response->setContent('ok');



		return $response;

	}


	public function routeParametersAction() {

		$route = $this->getRequest()->request->get('route');
		$search = $this->getRequest()->request->get('search');

		$services = $this->admin->getSearchServices();

		$html = '';

		if(array_key_exists($route, $services)) {
			$service = $services[$route];
			if($service != null) {
				if($this->container->has($service));
				$elements = $this->container->get($service)->search($search);


				if(is_array($elements)) {
					$html = $this->get('templating')->render('LmCmsBundle:CRUD:menu_routeparameters_choice.html.twig', array('elements' => $elements));
				}


			}
		}

		$response = new Response();
		$response->setContent($html);

		return $response;

	}



	/**
	 * TODO: zoptymalizować listę do 1 zapytania
	 *
	 * @return Response
	 */
	public function listAction()
	{
		if (false === $this->admin->isGranted('LIST')) {
			throw new AccessDeniedException();
		}

		$datagrid = $this->admin->getDatagrid();
		$formView = $datagrid->getForm()->createView();

		// set the theme for the current Admin Form
		// $this->get('twig')->getExtension('form')->setTheme($formView, $this->admin->getFilterTheme());


		$menu_cfg = $this->container->getParameter('lm_cms.menu');
//                echo "<pre>";
//                print_r($menu_cfg);
//                echo "</pre>";

		$menus = $menu_cfg['locations'];

		foreach( $menus as $loc_name => $location) {
			$menus[$loc_name]['menuItems'] = $this->getDoctrine()->getEntityManager()->createQuery("
				SELECT i FROM LmCmsBundle:MenuItem i
				WHERE 	i.location = :location
					AND ( i.depth = 1 OR i.depth IS NULL)
				ORDER BY i.location ASC, i.lft ASC
			")
			->setParameter('location', $loc_name)
			->execute();
		}

//                echo "<pre>";
//                print_r($menus);
//                echo "</pre>";


		return $this->render($this->admin->getTemplate('list'), array(
				'action'   => 'list',
				'menus'		=> $menus,

// 				'form'     => $formView,
// 				'datagrid' => $datagrid
		));
	}

}
