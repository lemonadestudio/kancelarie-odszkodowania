<?php

namespace Lm\CmsBundle\Controller;

use Lm\CmsBundle\Entity\Page;
use Lm\CmsBundle\Entity\Categories;
use Lm\CmsBundle\Repository\PageRepository;
use Lm\CmsBundle\Form\Type\ContactType;
use Lm\CmsBundle\Entity\ContactFormMessage;
use Symfony\Component\Finder\SplFileInfo;
use Symfony\Component\Finder\Finder;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\Response;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Serializer\Encoder\JsonEncoder;
use Lm\CmsBundle\Entity\News;
use Lm\CmsBundle\Menu\Builder;
use Lm\CmsBundle\Repository\OfferRepository;
use Lm\CmsBundle\Entity\Offer;
use Lm\CmsBundle\Repository\ProductCategoryRepository;
use Symfony\Component\Locale\Locale;
use Lm\CmsBundle\Helper\OfferToJsonConverter;
use GeoIp2\Database\Reader;

class MainController extends Controller {

    /**
     * @Template
     */
    public function homepageAction() {
        $ip = $this->container->get('request')->getClientIp();
        
        if ($ip == '127.0.0.1') {
            $ip = '91.196.10.5';
        }

        $offerTitle = '';
        $city = $this->getIpLocation($ip);
        
        $offers = $this->getOfferRepository()->getPromotedOffersForHomepage();
        
        if (count($offers)) {
            $offerTitle = 'Firmy promowane';
        } else {
            $offers = $this->getOfferRepository()->getNewestOffersForHomepage();
            
            if (count($offers)) {
                $offerTitle = 'Najnowsze firmy';
            }
        }
        
        $breadcrumbs = $this->get("white_october_breadcrumbs");
    	$breadcrumbs->addItem("Strona główna", $this->get("router")->generate("lm_cms_main_homepage"));
        $offerConverter = new OfferToJsonConverter();
        
        return array(
            'offers' => $offers,
            'offersJson' => $offerConverter->convertOffers($offers),
            'offersTitle' => $offerTitle
        );
    }

    /**
     * @Template
     */
    public function homepage_langAction($slug) {
        $page = $this->getPageRepository()->findOneBy(array('slug' => $slug));

        if (!$page) {
            $page = new Page();
            $page->setTitle($slug);
            $page->setContent('<p>Strona językowa ' . $slug . ' </p><p>edytuj w panelu administracyjnym</p>');
            $page->setPublished(false);
            $page->setSlug($slug);
            $this->getDoctrine()->getEntityManager()->persist($page);
            $this->getDoctrine()->getEntityManager()->flush();
        }

        return array(
            'page' => $page
        );
    }

    /**
     */
    public function browseAction() {
        $_SESSIO['KCFINDER'] = array();
        $_SESSIO['KCFINDER']['disabled'] = false;
        $_SESSIO['fold_type'] = 'pliki';
        $_SESSIO['KCFINDER']['uploadURL'] = $this->get('request')->getBasePath() . "/uploads";
        $get_parameters = $this->get('request')->query->all();

        $response = new RedirectResponse(
                $this->get('request')->getBasePath() . '/kcfinder/browse.php?'
                . http_build_query($get_parameters));

        return $response;
    }

    /**
     * @Template
     */
    public function sliderAction() {
        $photos = $this->getDoctrine()
                ->getRepository('LmCmsBundle:SliderPhoto')
                ->findBy(array(), array('kolejnosc' => 'ASC'));

        return array(
            'photos' => $photos
        );
    }

    /**
     * @Template()
     */
    public function contactAction() {
        $breadcrumbs = $this->get("white_october_breadcrumbs");
        $breadcrumbs->addItem("Strona główna", $this->get("router")->generate("lm_cms_main_homepage"));
        $breadcrumbs->addItem('Kontakt', $this->get('router')->generate('lm_cms_main_contact'));

        $form = $this->createForm(new ContactType($this->container), array());
        $request = $this->getRequest();
        
        if ($request->getMethod() == 'POST') {
            $form->bind($request);

            if ($form->isValid()) {
                $em = $this->getDoctrine()->getEntityManager();
                $data = $form->getData();
                $message_txt = '<h3>Wiadomość z formularza:</h3>';
                
                if (isset($data['content'])) {
                    $data['content'] = strip_tags($data['content'], '<br><p><b><strong>');
                    $message_txt .= $data['content'] . ' <hr />';
                }

                $message_txt .= '<h3>Pozostałe dane:</h3>';
                
                if (isset($data['title']))
                    $message_txt .= 'Tytuł zapytania: ' . $data['title'] . '<br />';
                
                if (isset($data['email']))
                    $message_txt .= 'Adres e-mail: ' . $data['email'] . '<br />';

                $message = new ContactFormMessage();
                $message->setSenderEmail(isset($data['email']) ? $data['email'] : 'brak e-mail');
                $message->setSenderMessage($message_txt);
                $message->setSenderIP($request->getClientIp());
                $message->setSentAt(new \DateTime());

                $em->persist($message);
                $em->flush();

                $this->get('cms_config')->all();
                $email_to = $this->get('cms_config')->get('formularz_wysylka_email');
                
                $temat = $this->get('cms_config')->get('formularz_wysylka_temat', 'Nowa wiadomość z formularza kontaktowego');
                $wysylka_od = $this->get('cms_config')->get('formularz_wysylka_od', 'Formularz kontaktowy');

                $message_email = \Swift_Message::newInstance()
                        ->setSubject($temat)
                        ->setFrom($this->container->getParameter('mailer_user'), $wysylka_od)
                        ->setTo($email_to)
                        ->setBody($message_txt, 'text/html')
                        ->addPart(strip_tags($message_txt), 'text/plain');

                if (isset($data['email'])) {
                    $message_email->setReplyTo($data['email']);
                }

                try {
                    $ret = $this->get('mailer')->send($message_email);
                } catch (Exception $e) {}

                return $this->redirect($this->get('router')->generate('lm_cms_main_contactSuccess'));
            }
        }
        
        return array(
            'form' => $form->createView()
        );
    }
    
    /**
     * @Template
     */
    public function contactSuccessAction() {
        return array();
    }

    /**
     * @return PageRepository
     */
    private function getPageRepository() {

        return $this->getDoctrine()->getRepository('LmCmsBundle:Page');
    }
    
    /**
     * @return OfferRepository
     */
    private function getOfferRepository() {

        return $this->getDoctrine()->getRepository('LmCmsBundle:Offer');
    }
    
    
    /**
     * Returns name of the city, depending on ip from given parameter.
     * @param string $ip
     * @return string
     */
    public function getIpLocation($ip) 
    {
        $reader = new Reader('../vendor/geoip2/geoip2/maxmind-db/GeoLite2-City.mmdb');

        $location = $reader->city($ip);

        $city = $location->city->name;
        
        return $city;
    }
}
