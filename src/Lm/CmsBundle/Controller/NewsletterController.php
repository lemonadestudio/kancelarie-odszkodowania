<?php

namespace Lm\CmsBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\RequestStack;
use Symfony\Component\HttpFoundation\StreamedResponse;
use Doctrine\ORM\Query;
use Lm\CmsBundle\Repository\NewsletterMessageRepository;
use Lm\CmsBundle\Repository\NewsletterSubscriberRepository;
use Lm\CmsBundle\Entity\NewsletterSubscriber;
use Lm\CmsBundle\Entity\NewsletterMessage;
use Lm\CmsBundle\Entity\NewsletterMessageRecipient;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Lm\CmsBundle\Form\Type\NewsletterSubscriberType;
use Symfony\Component\Form\FormError;
use Lm\CmsBundle\Util\Config;

class NewsletterController extends Controller 
{    
    public function subscribeAction() 
    {
        $request = $this->getRequest();
        
        if (!$request->isXmlHttpRequest()) {
            return new NotFoundHttpException();
        }
        
        $em = $this->getDoctrine()->getManager();
        $form = $this->createForm(new NewsletterSubscriberType());
        $isSubscriptionCreated = false;
        
        if ($request->isMethod('GET')) {
            return $this->render('LmCmsBundle:Newsletter:subscribe.html.twig', array(
                'isSubscriptionCreated' => $isSubscriptionCreated,
                'form' => $form->createView()
            ));
        }

        if ($request->isMethod('POST')) {
            $form->bind($request);

            if ($form->isValid()) {
                $existingSubscriber = $this->getNewsletterSubscriberRepository()->findOneBy(array(
                    'email' => $form->get('email')->getData()
                ));

                $subscriberIsAlreadyActive = $existingSubscriber && $existingSubscriber->getIsActive();
                $subscriberIsInactive = $existingSubscriber && !$existingSubscriber->getIsActive();
                $subscriberDoesNotExist = !$existingSubscriber;

                if ($subscriberIsAlreadyActive) {
                    $form->get('email')->addError(new FormError('Ten adres jest już w naszej bazie'));
                } else if ($subscriberIsInactive) {
                    $this->sendActivationMessageToSubscriber($existingSubscriber);
                    $isSubscriptionCreated = true;
                } else if ($subscriberDoesNotExist) {
                    $newSubscriber = new NewsletterSubscriber();
                    $newSubscriber->setEmail($form->get('email')->getData());
                    $newSubscriber->setIsActive(false);
                    $em->persist($newSubscriber);
                    $em->flush();
                    $this->sendActivationMessageToSubscriber($newSubscriber);
                    $isSubscriptionCreated = true;
                }
            }
            
            // po prawidłowym dodaniu subskrybenta formularz powinien mieć puste pola
            // możliwe, że to hack, bo chyba nie ma metody do resetowania formularza
            if ($isSubscriptionCreated) {
                $form = $this->createForm(new NewsletterSubscriberType());
            }
        }
        
        return $this->render('LmCmsBundle:Newsletter:subscribeForm.html.twig', array(
            'isSubscriptionCreated' => $isSubscriptionCreated,
            'form' => $form->createView()
        ));
    }

    public function confirmSubscriptionAction($token) 
    {
        $subscriber = $this->getNewsletterSubscriberRepository()->findOneBy(array(
            'token' => $token,
            'isActive' => false
        ));
        
        if ($subscriber) {
            $subscriber->setIsActive(true);
            $this->getDoctrine()->getManager()->flush();
            
            $this->get("session")
                    ->getFlashBag()
                    ->add("informationPopup", "Subskrypcja newslettera została aktywowana.");
        }
        
        return $this->redirect($this
                                ->get('router')
                                ->generate('lm_cms_main_homepage'));
    }
    
    public function unsubscribeAction($token) 
    {
        $subscriber = $this->getNewsletterSubscriberRepository()->findOneBy(array(
            'token' => $token,
            'isActive' => true
        ));

        if ($subscriber) {
            $subscriber->setIsActive(false);
            $this->getDoctrine()->getManager()->flush();
            
            $this->get("session")
                    ->getFlashBag()
                    ->add("informationPopup", "Subskrypcja newslettera została anulowana.");
        }

        return $this->redirect($this
                                ->get('router')
                                ->generate('lm_cms_main_homepage'));
    }
    
    private function sendActivationMessageToSubscriber($subscriber) 
    {
        $template = $this->container->get('templating');
        $subscriptionActivationUrl = $this->get('router')->generate('lm_cms_newsletter_confirm_subscription', array(
            'token' => $subscriber->getToken()), true);
        $htmlMessage = $template->render('LmCmsBundle:Newsletter:subscriptionActivationMail.html.twig', array(
            'subscriptionActivationUrl' => $subscriptionActivationUrl
        ));
        
        $settings = $this->get('cms_config');
        $subject = $settings->get('newsletter_activation_mail_subject', 'Kancelarie-odszkodowania.pl - aktywacja subskrypcji newslettera');
        $senderName = $settings->get('newsletter_activation_mail_sender_name', 'Kancelarie-odszkodowania.pl');
        
        $message = \Swift_Message::newInstance()
                ->setSubject($subject)
                ->setFrom($this->container->getParameter('mailer_user'), $senderName)
                ->setTo($subscriber->getEmail())
                ->setBody($htmlMessage, 'text/html');

        try {
            $this->get('mailer')->send($message);
        } catch (Exception $e) {}
    }
    
    private function getNewsletterSubscriberRepository() 
    {
        return $this->getDoctrine()->getRepository('LmCmsBundle:NewsletterSubscriber');
    }
}
