<?php

namespace Lm\CmsBundle\Controller;

use Symfony\Component\Translation\IdentityTranslator;
use Lm\CmsBundle\Repository\PageRepository;
use Lm\CmsBundle\Entity\Page;
use Lm\CmsBundle\Repository\ProductCategoryRepository;
use Lm\CmsBundle\Entity\ProductCategory;
use Symfony\Component\HttpFoundation\Response;
use Doctrine\DBAL\Statement;
use Symfony\Component\Routing\Router;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;

class PageController extends Controller {

    /**
     * @return PageRepository
     */
    public function getPageRepository() {
        return $this->getDoctrine()->getRepository('LmCmsBundle:Page');
    }

    /**
     * @Template
     */
    public function showAction($slug) {

        $page = $this->getPageRepository()->findOneBy(array('slug' => $slug));

        if (!$page) {
            return $this->redirect($this->get('router')->generate('lm_cms_main_homepage'));
        }

        $breadcrumbs = $this->get("white_october_breadcrumbs");
        $breadcrumbs->addItem("Strona główna", $this->get("router")->generate("lm_cms_main_homepage"));
        $breadcrumbs->addItem($page->getTitle(), $this->get('router')->generate('lm_cms_page_show', array('slug' => $page->getSlug())));

        return array(
            'page' => $page
        );
    }

}
