<?php

namespace Lm\CmsBundle\Controller;

use Lm\CmsBundle\Entity\Advert;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Lm\CmsBundle\Models\AdvertLocation;

class AdvertController extends Controller {

    /**
     * @Template()
     */
    public function leftSideBarAdvertsAction() {
        $adverts = $this->getAdvertRepository()
                ->createQueryBuilder('a')
                ->select('a')
                ->where('a.isActive = 1')
                ->andWhere('a.location = :location')
                ->orderBy('a.id', 'DESC')
                ->setParameter('location', AdvertLocation::LEFT_SIDE_BAR)
                ->getQuery()
                ->getResult();
        
        return array(
            'adverts' => $adverts,
        );
    }
    
    /**
     * @Template()
     */
    public function homepageTopUnderBreadcrumbsAdvertsAction() {
        $adverts = $this->getAdvertRepository()
                ->createQueryBuilder('a')
                ->select('a')
                ->where('a.isActive = 1')
                ->andWhere('a.location = :location')
                ->orderBy('a.id', 'DESC')
                ->setParameter('location', AdvertLocation::HOMEPAGE_TOP_UNDER_BREADCRUMBS)
                ->getQuery()
                ->getResult();
        
        return array(
            'adverts' => $adverts,
        );
    }
    
    /**
     * @return OfferRepository
     */
    private function getAdvertRepository() {
        return $this->getDoctrine()->getRepository('LmCmsBundle:Advert');
    }
}
