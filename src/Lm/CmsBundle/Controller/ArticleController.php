<?php

namespace Lm\CmsBundle\Controller;

use Symfony\Component\Translation\IdentityTranslator;

use Lm\CmsBundle\Repository\ArticleRepository;
use Lm\CmsBundle\Entity\Article;
use Lm\CmsBundle\Repository\ProductCategoryRepository;
use Lm\CmsBundle\Entity\ProductCategory;
use Symfony\Component\HttpFoundation\Response;
use Doctrine\DBAL\Statement;
use Symfony\Component\Routing\Router;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;


class ArticleController extends Controller
{


    /**
     * @return ArticleRepository
     */
	public function getArticleRepository() {

		return $this->getDoctrine()->getRepository('LmCmsBundle:Article');

	}

	/**
	 * @Template()
	 * @param unknown $slug
	 * @param unknown $id
	 * @return \Symfony\Component\HttpFoundation\RedirectResponse|multitype:NULL Ambigous <object, NULL>
	 */
    public function categoryAction($type, $page = 1)
    {

    	$page = (int) $page;

    	$locale = $this->get('request')->getLocale();

    	if(!in_array($type, array(
    			Article::TYPE_artykuly,
    			Article::TYPE_wydarzenia,

    			))) {

    		return $this->redirect(
    				$this->get('router')->generate('lm_cms_main_homepage'));
    	}

    	$breadcrumbs = $this->get("white_october_breadcrumbs");
    	$breadcrumbs->addItem("Strona główna", $this->get("router")->generate("lm_cms_main_homepage"));
    	$breadcrumbs->addItem(
    			$this->get('translator')->trans('label.'.$type),

    			$this->get("router")->generate("lm_cms_article_category", array(
    					'type' => $type)));

    	$em = $this->getDoctrine()->getManager();
    	$dql = "SELECT a
    			FROM LmCmsBundle:Article a
					WHERE a.type = :type
    			     AND a.publishDate <= :date
    				 AND a.published = true
    			 ORDER BY a.publishDate DESC ";

    	'  WHERE a.publishDate <= :date AND a.type = :type ';

    	$articles_query = $em->createQuery($dql)->setParameters(array(
    							 ':type' => $type,
    							':date' => new \DateTime()
    			));



    	$paginator = $this->get('knp_paginator');

    	try{
    		$articles = $paginator->paginate($articles_query,$page, 3);
    	} catch(\Exception $e) {

    		return $this->redirect(
    				$this->get("router")->generate("lm_cms_article_category", array(
    					'type' => $type)));

    	}

    	if( $type == 'wydarzenia' )
    		$view = 'LmCmsBundle:Article:category_wydarzenia.html.twig';
    	else
    		$view = 'LmCmsBundle:Article:category_artykuly.html.twig';


        $parameters =  array(
        		'type' => $type,
        		'articles' => $articles
        );

        return $this->render($view, $parameters);


    }

    /**
     *
     * @param unknown $slug
     * @param unknown $id
     * @return \Symfony\Component\HttpFoundation\RedirectResponse|multitype:NULL Ambigous <object, NULL>
     */
    public function showAction($type, $slug, $id)
    {


    	$locale = $this->get('request')->getLocale();

    	if(!in_array($type, array(
    			Article::TYPE_artykuly,
    			Article::TYPE_wydarzenia,
    	))) {

    		return $this->redirect(
    				$this->get('router')->generate('lm_cms_main_homepage'));
    	}


    	$article = $this->getArticleRepository()->findOneBy(array('id' => $id));

    	if(!$article) {
    		return $this->redirect(
    				$this->get('router')->generate('lm_cms_main_homepage'));
    	}

    	if($article->getSlug() != $slug || $type != $article->getType()) {
    		return $this->redirect(
    				$this->get('router')->generate('lm_cms_article_show',
    						array('id' => $article->getId(),
    								'slug' => $article->getSlug(),
    								'type' => $article->getType())));
    	}


    	$breadcrumbs = $this->get("white_october_breadcrumbs");
    	$breadcrumbs->addItem("Strona główna", $this->get("router")->generate("lm_cms_main_homepage"));

    	$breadcrumbs->addItem($this->get('translator')->trans('label.'.$article->getType()),
    			$this->get("router")->generate("lm_cms_article_category", array(
    					'type' => $article->getType())));



    	$breadcrumbs->addItem($article->getTitle(), $this->get('router')->generate('lm_cms_article_show',
    			array('id' => $article->getId(), 'slug' => $article->getSlug(), 'type'=>$article->getType())));


       $parameters =  array(
        	'article' => $article
        );

       switch($article->getType()) {


	       	default:
	       		$view = 'LmCmsBundle:Article:show.html.twig';
				break;

       }

       return $this->render($view, $parameters);




    }

    /**
     * @Template
     */
    public function wydarzenia_right_boxAction() {


    	$wydarzenia = $this->getArticleRepository()->createQueryBuilder('a')
	    	->where('a.published = true')
	    	->andWhere('a.publishDate <= :date')
	    	->andWhere('a.type = :type')
	    	->orderBy('a.publishDate', 'DESC')
	    	->setMaxResults(2)
	    	->getQuery()
	    	->execute(array(
	    			':type' => 'wydarzenia',
	    			':date' => new \DateTime()));

    	return array('wydarzenia' =>  $wydarzenia );

    }




}
