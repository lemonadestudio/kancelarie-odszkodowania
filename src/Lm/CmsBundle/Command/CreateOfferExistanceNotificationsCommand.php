<?php

namespace Lm\CmsBundle\Command;

use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Lm\CmsBundle\Entity\OfferExistenceNotification;

class CreateOfferExistanceNotificationsCommand extends ContainerAwareCommand
{    
    protected function configure()
    {
        $this
            ->setName('offer:create-existence-notifications')
            ->setDescription('Tworzenie powiadomień o istnieniu oferty w systemie od x lat.')
            ->addArgument('max-offers-count', InputArgument::REQUIRED, 'Maksymalna ilość ofert, dla których zostaną utworzone powiadomienia.');
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        // fix dla hostingu linuxpl
        date_default_timezone_set('Europe/Warsaw');
        
        $maxOffersCount = intval($input->getArgument('max-offers-count'), 10);
        $createdNotificationsCount = 0;
        $output->writeln('Maksymalna ilość ofert: ' . $maxOffersCount);
        
        if ($maxOffersCount <= 0) {
            return;
        }
        
        $doctrine = $this->getContainer()->get('doctrine');
        $em = $doctrine->getEntityManager();
        $offers = $this->getOffers($em, $maxOffersCount);
        
        foreach ($offers as $offer) {
            $notification = new OfferExistenceNotification($offer);
            $offer->setYearsInSystemForLastExistenceNotification($notification->getForYearsInSystem());
            
            $em->persist($notification);
            $em->persist($offer);
            $createdNotificationsCount++;
        }
        
        $em->flush();
        $output->writeln("Ilość utworzonych powiadomień: {$createdNotificationsCount}.");
    }
    
    private function getOffers($doctrine, $maxOffersCount)
    {
        $currentDate = new \DateTime();
        $currentYear = $currentDate->format('Y');
        $nearestAnniversary = "DATE_FORMAT(o.created, CONCAT(:currentYear, '-%m-%d %T'))";
        $monthBeforeNearestAnniversary = "DATE_SUB({$nearestAnniversary}, 1, 'month')";
        
        return $doctrine
                ->getRepository('LmCmsBundle:Offer')
                ->createQueryBuilder('o')
                ->select('partial o.{id,created,yearsInSystemForLastExistenceNotification}')
                ->where('o.yearsInSystemForLastExistenceNotification is null or ' .
                        'TIMESTAMPDIFF(YEAR, o.created, :currentDate) > o.yearsInSystemForLastExistenceNotification')
                ->andWhere(":currentDate >= {$monthBeforeNearestAnniversary}")
                ->andWhere(":currentDate <= {$nearestAnniversary}")
                ->orderBy('o.id', 'asc')
                ->setParameter('currentDate', $currentDate)
                ->setParameter('currentYear', $currentYear)
                ->setMaxResults($maxOffersCount)
                ->getQuery()
                ->getResult();
    }
}
