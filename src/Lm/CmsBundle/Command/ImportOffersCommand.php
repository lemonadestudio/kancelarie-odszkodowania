<?php

namespace Lm\CmsBundle\Command;

use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Lm\CmsBundle\Entity\Offer;
use Lm\CmsBundle\Entity\OfferCategory;
use Lm\CmsBundle\Entity\OfferMainAddress;

class ImportOffersCommand extends ContainerAwareCommand
{
    private $doctrine;
    private $logs;
    private $currentFileNumber;
    
    protected function configure()
    {
        $this
            ->setName('offer:import-offers')
            ->setDescription('Import ofert z predefiniowanych plików csv.')
            ->addArgument('fileName', InputArgument::REQUIRED, 'Skrót importowanego pliku');
    }
    
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        // fix dla hostingu linuxpl
        date_default_timezone_set('Europe/Warsaw');
        
        $this->doctrine = $this->getContainer()->get('doctrine');
        $this->logs = array();
        $this->output = $output;
        $this->currentFileNumber = $this->getLastFileNumber() + 1;
        $fileName = $input->getArgument('fileName');
        $filePath = __DIR__ . "/../../../../app/offersToImport/{$fileName}";
        $fileInfo = new \SplFileInfo($filePath);
        
        $this->addLogInfo("Import z '{$filePath}'.");
        
        if (!$fileInfo->isFile()) {
            $this->addLogInfo('Importowany plik nie istnieje.');
            return;
        }
        
        $parsedData = $this->parseFile($fileInfo);
        $categories = $this->getOrCreateCategories($parsedData['categoryNames']);
        $this->createOffers($parsedData['offers'], $categories);
        $this->saveLogs();
    }
    
    private function parseFile(\SplFileInfo $fileInfo)
    {
        $file = new \SplFileObject($fileInfo->getRealpath());
        $file->setFlags(\SplFileObject::SKIP_EMPTY | \SplFileObject::DROP_NEW_LINE | \SplFileObject::READ_CSV);
        $file->setCsvControl(',');
        
        $categoriesStartIndex = 11;
        $line = 1;
        $categoryNames = array();
        $offers = array();
        
        while (!$file->eof()) {
            $currentLine = $file->current();
            
            if ($line === 1) {
                $categoryNames = $this->parseCategories($currentLine, $categoriesStartIndex);
            } else {
                $offer = $this->parseOffer($currentLine, $categoriesStartIndex);
                
                if (empty($offer)) {
                    continue;
                }
                
                $offers[] = $offer;
            }
            
            $file->next();
            $line++;
        }
        
        $offersCount = count($offers);
        $categoriesCount = count($categoryNames);
        $this->addLogInfo("Zaimportowany plik zawiera {$line} linii.");
        $this->addLogInfo("Sparsowano {$offersCount} ofert.");
        $this->addLogInfo("Sparsowano {$categoriesCount} kategorii.");
        
        return array(
            'offers' => $offers,
            'categoryNames' => $categoryNames
        );
    }
    
    private function createOffers($offers, $categories)
    {
        $this->addLogInfo('-------------------------------- Oferty');
        
        $em = $this->doctrine->getManager();
        $createdOffers = 0;
        
        foreach ($offers as $offer) {
            $newOffer = new Offer();
            $newOffer->setTitle($offer['name']);
            $newOffer->setCreatedUsingImport(true);
            $newOffer->setActive(true);
            $newOffer->setImportedFileNumber($this->currentFileNumber);
            $mainAddress = new OfferMainAddress();
            $mainAddress->setCity($offer['city']);
            $mainAddress->setStreet($offer['street']);
            $mainAddress->setPostCode($offer['postCode']);
            $mainAddress->setPhone($offer['phone']);
            $mainAddress->setEmail($offer['email']);
            $newOffer->setMainAddress($mainAddress);
            
            foreach ($offer['categoryKeys'] as $key) {
                $category = $categories[$key];
                
                if (empty($category)) {
                    continue;
                }
                
                $newOffer->addCategorie($category);
            }
            
            $em->persist($newOffer);
            
            $createdOffers++;
            
            $this->addLogInfo('Dodawanie oferty: ' . 
                    $offer['name'] . ', ' . 
                    $offer['city'] . ', ' . 
                    $offer['street'] . ', ' . 
                    $offer['postCode'], false);
        }
        
        $this->addLogInfo('Utworzonych ofert: ' . $createdOffers);
        
        $em->flush();
        
        $this->addLogInfo('Oferty zostały poprawnie zapisane.');
    }
    
    private function getOrCreateCategories($categoryNames)
    {
        $existingCategories = $this->doctrine
                ->getRepository('LmCmsBundle:OfferCategory')
                ->createQueryBuilder('c')
                ->select('c')
                ->where('c.name IN(:categoryNames)')
                ->setParameter('categoryNames', $categoryNames)
                ->getQuery()
                ->getResult();
        $categories = array();
        
        $this->addLogInfo('-------------------------------- Kategorie');
        $this->addLogInfo('Ilość kategorii istniejących w systemie: ' . count($existingCategories));
        
        foreach ($categoryNames as $key => $categoryName) {
            $categories[$key] = $this->getOrCreateCategory($categoryName, $existingCategories);
        }
        
        $this->doctrine->getManager()->flush();
        $this->addLogInfo('Kategorie poprawnie zapisane w systemie.');
        
        return $categories;
    }
    
    private function getOrCreateCategory($categoryName, &$existingCategories)
    {
        $category = $this->findCategory($categoryName, $existingCategories);
        
        if ($category != null) {
            $this->addLogInfo('Kategoria: ' . $categoryName . ' - już istnieje.');
            return $category;
        }
        
        $category = new OfferCategory();
        $category->setName($categoryName);
        $category->setPosition($this->generateNextCategoryPosition($existingCategories));
        $this->doctrine->getManager()->persist($category);
        $existingCategories[] = $category;
        $this->addLogInfo('Kategoria: "' . $categoryName . '" została dodana z pozycją ' . $category->getPosition());

        return $category;
    }
    
    private function parseCategories($currentLine, $categoriesStartIndex)
    {
        $categoryNames = array();
        
        for ($i = $categoriesStartIndex; $i < count($currentLine); $i++) {
            $categoryNames[$i] = $this->betterTrim($currentLine[$i]);
        }
        
        return $categoryNames;
    }
    
    private function parseOffer($currentLine, $categoriesStartIndex)
    {
        $name = $this->betterTrim($currentLine[1]);
        
        if (empty($name)) {
            return null;
        }

        $postCodeAndCity = array();
        preg_match('/([0-9]{2}\s*[-–—]{1}\s*[0-9]{3})(.*)/u', $currentLine[3], $postCodeAndCity);
        
        $offer = array(
            'name' => $name,
            'street' => $this->betterTrim($currentLine[2]),
            'postCode' => isset($postCodeAndCity[1]) ? $this->betterTrim($postCodeAndCity[1]) : null,
            'city' => isset($postCodeAndCity[2]) ? $this->betterTrim($postCodeAndCity[2]) : '',
            'phone' => $this->betterTrim($currentLine[4]),
            'email' => $this->betterTrim($currentLine[5]),
            'categoryKeys' => array()
        );

        for ($i = $categoriesStartIndex; $i < count($currentLine); $i++) {
            $categoryMark = strtolower($this->betterTrim($currentLine[$i]));

            if ($categoryMark !== 'x') {
                continue;
            }

            $offer['categoryKeys'][] = $i;
        }

        return $offer;
    }
    
    private function findCategory($name, $categories)
    {
        foreach ($categories as $category) {
            if ($category->getName() === $name) {
                return $category;
            }
        }
        
        return null;
    }
    
    private function generateNextCategoryPosition($existingCategories)
    {
        $lastCategoryPosition = 1;
        
        foreach ($existingCategories as $category) {
            if ($category->getPosition() > $lastCategoryPosition) {
                $lastCategoryPosition = $category->getPosition();
            }
        }
        
        return $lastCategoryPosition + 1;
    }
    
    private function betterTrim($string)
    {
        return trim($string, " \t\n\r\0\x0B\xC2\xA0");
    }
    
    private function addLogInfo($text, $showInCli = true)
    {
        if ($showInCli) {
            $this->output->writeln($text);
        }
        
        $this->logs[] = $text;
    }
    
    private function saveLogs()
    {
        $logFileName = 'offers_import-log' . time() . '.txt';
        $logFile = new \SplFileObject(__DIR__ . "/../../../../app/offersToImport/logs/{$logFileName}", 'c+');
        $logMessage = '';
        
        foreach ($this->logs as $log) {
            $logMessage .= "$log\r\n";
        }
        
        $logFile->fwrite($logMessage);
    }
    
    private function getLastFileNumber()
    {
        $lastFileNumber = $this->doctrine
                ->getRepository('LmCmsBundle:Offer')
                ->createQueryBuilder('o')
                ->select('o.importedFileNumber')
                ->orderBy('o.importedFileNumber', 'desc')
                ->setMaxResults(1)
                ->getQuery()
                ->getOneOrNullResult();
        
        return empty($lastFileNumber) ? 0 : $lastFileNumber['importedFileNumber'];
    }
}
