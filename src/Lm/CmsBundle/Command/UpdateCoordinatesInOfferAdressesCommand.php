<?php

namespace Lm\CmsBundle\Command;

use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Lm\CmsBundle\Util\GeolocationFinder;
use Lm\CmsBundle\Util\GeolocationResultStatus;

class UpdateCoordinatesInOfferAdressesCommand extends ContainerAwareCommand
{    
    private $updatedAddressesCount;
    
    protected function configure()
    {
        $this
            ->setName('offer:update-coordinates-in-addresses')
            ->setDescription('Aktualizacja koordynatów dla adresów ofert.')
            ->addArgument('max-addresses-count', InputArgument::REQUIRED, 'Maksymalna ilość adresów do zaktualizowania.');
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        // fix dla hostingu linuxpl
        date_default_timezone_set('Europe/Warsaw');
        
        $maxAddressesCount = intval($input->getArgument('max-addresses-count'), 10);
        $this->updatedAddressesCount = 0;
        $output->writeln('Maksymalna ilość adresów: ' . $maxAddressesCount);
        
        if ($maxAddressesCount <= 0) {
            return;
        }
        
        $doctrine = $this->getContainer()->get('doctrine');
        $addresses = $this->getAddresses($doctrine, $maxAddressesCount);
        
        $output->writeln('Ilość adresów do zaktualizowania: ' . count($addresses));
        
        foreach ($addresses as $address) {
            $this->updateAddress($doctrine, $address);
            $this->safeIntervalForNextCallToGoogleApi();
        }
        
        $output->writeln("Ilość zaktualizowanych adresów: {$this->updatedAddressesCount}.");
    }
    
    private function getAddresses($doctrine, $maxAddressesCount)
    {
        $sql = "
            SELECT a.* 
            FROM
            (
                SELECT id, city, street, postCode, 'main' as `type`, created 
                FROM offer_main_address
                WHERE checksumOfLastCoordsCheck is null or 
                checksumOfLastCoordsCheck != md5(CONCAT(city, ' ', street, ' ', postCode))

                UNION ALL SELECT id, city, street, postCode, 'additional' as `type`, created 
                FROM offer_additional_address
                WHERE checksumOfLastCoordsCheck is null or 
                checksumOfLastCoordsCheck != md5(CONCAT(city, ' ', street, ' ', postCode))
            ) a
            ORDER BY a.created ASC
            LIMIT 0, {$maxAddressesCount}";
        
        $stmt = $doctrine
                ->getEntityManager()
                ->getConnection()
                ->prepare($sql);
        $stmt->execute();
        
        return $stmt->fetchAll();
    }
    
    private function updateAddress($doctrine, $address)
    {
        $addressToSearch = $this->prepareSearchString($address);
        $geolocationFinder = new GeolocationFinder($this->getContainer()->getParameter('google_places_api_key'));
        $geolocationResult = $geolocationFinder->find($addressToSearch);
        
        if ($geolocationResult->hasStatus(GeolocationResultStatus::ERROR)) {
            return;
        }
        
        $entityClassNameToUpdate = $this->getEntityNameToUpdate($address);
        $this->updateSpecificAddressEntity($doctrine, $address, $geolocationResult, $entityClassNameToUpdate);
    }
    
    private function prepareSearchString($address)
    {
        $addressPartsToSearch = array();
        
        if ($address['street']) {
            $addressPartsToSearch[] = $address['street'];
        }
        
        if ($address['postCode'] || $address['city']) {
            $addressPartsToSearch[] = $address['postCode'] . ' ' . $address['city'];
        }
        
        $addressPartsToSearch[] = 'Polska';
        
        return implode(', ', $addressPartsToSearch);
    }
    
    private function getEntityNameToUpdate($address)
    {
        $entityClassNameToUpdate = '';
        
        if ($address['type'] === 'main') {
            $entityClassNameToUpdate = 'OfferMainAddress';
        } else if ($address['type'] === 'additional') {
            $entityClassNameToUpdate = 'OfferAdditionalAddress';
        }
        
        return $entityClassNameToUpdate;
    }
    
    private function updateSpecificAddressEntity($doctrine, $address, $geolocationResult, $entityClassName)
    {
        $q = $doctrine->getEntityManager()
                ->createQuery('UPDATE \Lm\CmsBundle\Entity\\' . $entityClassName . ' a SET '
                        . 'a.latitude = :latitude, '
                        . 'a.longitude = :longitude, '
                        . 'a.checksumOfLastCoordsCheck = :checksum, '
                        . 'a.updated = :updated '
                        . 'WHERE a.id = :id');
        
        $q->setParameters(array(
            'latitude' => $geolocationResult->getLatitude(),
            'longitude' => $geolocationResult->getLongitude(),
            'checksum' => md5($address['city'] . ' ' . $address['street'] . ' ' . $address['postCode']),
            'updated' => new \DateTime(),
            'id' => $address['id']
        ));
        
        $q->getResult();
        $this->updatedAddressesCount++;
    }
    
    /**
     * Google Geocode API w darmowe wersji ma limit do 10 requestow na 1sek.
     * To taki wesoly hack.
     */
    private function safeIntervalForNextCallToGoogleApi()
    {
        // 0.1s
        usleep(100000);
    }
}
