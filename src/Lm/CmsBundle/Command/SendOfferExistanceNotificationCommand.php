<?php

namespace Lm\CmsBundle\Command;

use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Lm\CmsBundle\Util\DateIntervalUtils;

class SendOfferExistanceNotificationCommand extends ContainerAwareCommand
{    
    protected function configure()
    {
        $this
            ->setName('offer:send-existence-notifications')
            ->setDescription('Wysyłanie powiadomień o istnieniu oferty w systemie od x lat.')
            ->addArgument('emails-count-to-send', InputArgument::REQUIRED, 'Ilość emaili do jednorazowaj wysyłki.');
    }
    
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        // fix dla hostingu linuxpl
        date_default_timezone_set('Europe/Warsaw');
        
        $mailsCountToSend = intval($input->getArgument('emails-count-to-send'), 10);
        $sentMailsCount = 0;
        $output->writeln('Maksymalna ilość maili do wysłania: ' . $mailsCountToSend);
        
        if ($mailsCountToSend <= 0) {
            return;
        }
        
        $doctrine = $this->getContainer()->get('doctrine');
        $em = $doctrine->getEntityManager();
        $notifications = $this->getNotifications($doctrine, $mailsCountToSend);
        
        foreach ($notifications as $notification) {
            if ($this->sendMessage($notification, $em, $output)) {
                $sentMailsCount++;
            }
        }
        
        $output->writeln("Ilość wysłanych maili: {$sentMailsCount}.");
    }
    
    private function getNotifications($doctrine, $mailsCountToSend)
    {
        return $doctrine
                ->getRepository('LmCmsBundle:OfferExistenceNotification')
                ->createQueryBuilder('n')
                ->select('partial n.{id,forYearsInSystem,sentDate}, partial o.{id,title,created}')
                ->innerJoin('n.offer', 'o')
                ->where('n.sentDate is null')
                ->orderBy('n.id', 'asc')
                ->setMaxResults($mailsCountToSend)
                ->getQuery()
                ->getResult();
    }
    
    private function sendMessage($notification, $em, $output) 
    {
        $mailer = $this->getContainer()->get('mailer');
        $senderMail = $this->getContainer()->getParameter('mailer_user');
        $recipientMail = $this->getContainer()->get('cms_config')->get('formularz_wysylka_email');
        
        $messageEmail = \Swift_Message::newInstance()
                ->setSubject('Powiadomienie o czasie istnienia w serwisie firmy ' . $notification->getOffer()->getTitle())
                ->setFrom($senderMail, 'Katalog kancelarie-odszkodowania')
                ->setTo($recipientMail)
                ->setBody($this->getEmailContent($notification), 'text/html');

        try {
            if ($mailer->send($messageEmail)) {
                //http://stackoverflow.com/questions/11393667/symfony2-swift-mailer-command-email-body-displayed-into-the-console
                //http://stackoverflow.com/questions/13122096/unable-to-send-e-mail-from-within-custom-symfony2-command-but-can-from-elsewhere
                //wykonuje sie po analizie kazdego elementu w petli, zeby odrazu wylapac wyjatki np. dlugi czas dostepu do serwera
                $spool = $mailer->getTransport()->getSpool();
                $transport = $this->getContainer()->get('swiftmailer.transport.real');
                $spool->flushQueue($transport);
                
                $output->writeln("Wysłane od {$senderMail} do {$recipientMail}");
                
                $notification->setSentDate(new \DateTime());
                $em->persist($notification);
                $em->flush();
                return true;
            }
        } catch (Exception $e) {
            $output->writeln("Błąd wysyłania maila od {$senderMail} do {$recipientMail}. Treść błędu: {$e->toString()}");
            
            return false;
        }
    }
    
    private function getEmailContent($notification)
    {
        $emailTemplateVars = array(
            'EXISTENCE_INFO' => $this->generateExistanceInfo($notification),
            'COMPANY_NAME' => $notification->getOffer()->getTitle()
        );
        
        $emailTemplate = $this->getContainer()
                ->get('cms_email_templates')
                ->getParsed('offer_existence_notification', $emailTemplateVars);
        
        return $emailTemplate->getContent();
    }
    
    /**
     * Metoda zwraca sformatowaną wiadomość o tym, kiedy minie "określona ilość czasu"
     * od kiedy firma istnieje w systemie.
     * np. "Za miesiąc mija rok", "Za 3 dni mijają 4 lata", "Właśnie mija 6 lat"
     * 
     * Jeżeli "określona ilość czasu" jest wartością dodatnią i wynosi więcej niż miesiąc, 
     * to metoda wyrzuci wyjątek. Dopuszczalna wartość to itnerwał <= miesiąc, bo
     * pod takim warunkiem tworzone są powiadomienia.
     */
    private function generateExistanceInfo($notification)
    {
        $now = new \DateTime();
        $offerCreatedDate = $notification->getOffer()->getCreated();
        $offerCreatedDate->setDate(
                $notification->getOffer()->getCreated()->format('Y') + $notification->getForYearsInSystem(), 
                $notification->getOffer()->getCreated()->format('m'), 
                $notification->getOffer()->getCreated()->format('d'));
        
        $dateDiff = $now->diff($offerCreatedDate);
        $isPositive = $dateDiff->invert === 0;
        $hasZeroYears = $dateDiff->y === 0;
        $isMaximumOneMonth = $dateDiff->m === 0 || 
                ($dateDiff->m === 1 && DateIntervalUtils::AreEqual($dateDiff, new \DateInterval('P1M')));
        
        if ($notification->getForYearsInSystem() === 0) {
            throw new SendOfferExistanceNotificationCommandException('Notification forYearsInSystem value must be greater than 0.');
        }
        
        if ($isPositive && !$hasZeroYears) {
            throw new SendOfferExistanceNotificationCommandException('Not supported DateInterval value.');
        }
        
        if ($isPositive && !$isMaximumOneMonth) {
            throw new SendOfferExistanceNotificationCommandException('Not supported DateInterval value.');
        }
        
        $text = '';
        if ($dateDiff->invert === 1) {
            $text .= 'Właśnie';
        } else if (DateIntervalUtils::AreEqual($dateDiff, new \DateInterval('P1M'))) {
            $text .= 'Za miesiąc';
        } else if ($dateDiff->d > 1) {
            $text .= "Za {$dateDiff->d} dni";
        } else if ($dateDiff->d === 1) {
            $text .= "Jutro";
        } else if ($dateDiff->d === 0) {
            $text .= "Dzisiaj";
        }
        
        if ($notification->getForYearsInSystem() === 1) {
            $text .= " mija rok";
        } else if ($notification->getForYearsInSystem() >= 2 && $notification->getForYearsInSystem() <= 4) {
            $text .= " mijają {$notification->getForYearsInSystem()} lata";
        } else {
            $text .= " mija {$notification->getForYearsInSystem()} lat";
        }
        
        return $text;
    }
}

class SendOfferExistanceNotificationCommandException extends \Exception
{
    public function __construct($message)
    {
        parent::__construct($message, null, null);
    }
}
