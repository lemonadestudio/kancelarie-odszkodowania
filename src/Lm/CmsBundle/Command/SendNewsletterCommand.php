<?php

namespace Lm\CmsBundle\Command;

use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Lm\CmsBundle\Entity\NewsletterSubscriber;
use Lm\CmsBundle\Entity\NewsletterMessage;
use Lm\CmsBundle\Entity\NewsletterMessageRecipientAsEmail;
use Lm\CmsBundle\Entity\NewsletterMessageRecipientAsSubscriber;
use Lm\CmsBundle\Repository\NewsletterMessageRecipientRepository;
use Symfony\Component\DomCrawler\Crawler;
use Lm\CmsBundle\Models\NewsletterMessageComposerForSubscriberRecipient;
use Lm\CmsBundle\Models\NewsletterMessageComposerForEmailRecipient;

/**
 * TODO, mozna zrobic refaktor - wydzielic fasade dla odbiorcow
 * i po ich pobraniu z bazy mapowac na nowy obiekt. Da to wygodniejszy dostep do
 * emaila i metody composeMessage.
 */
class SendNewsletterCommand extends ContainerAwareCommand
{    
    protected function configure()
    {
        $this
            ->setName('newsletter:send')
            ->setDescription('Wysyłanie newslettera.')
            ->addArgument('emails-count-to-send', InputArgument::REQUIRED, 'Ilość emaili do jednorazowaj wysyłki.');
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        // fix dla hostingu linuxpl
        date_default_timezone_set('Europe/Warsaw');
        
        $mailsCountToSend = intval($input->getArgument('emails-count-to-send'), 10);
        $sentMailsCount = 0;
        $output->writeln('Maksymalna ilość maili do wysłania: ' . $mailsCountToSend);
        
        if ($mailsCountToSend <= 0) {
            return;
        }
        
        $doctrine = $this->getContainer()->get('doctrine');
        $em = $doctrine->getEntityManager();
        $recipients = $this->getRecipients($mailsCountToSend, $doctrine);
        
        foreach ($recipients as $recipient) {
            if ($this->sendMessage($recipient, $em, $output)) {
                $sentMailsCount++;
            }
        }
        
        $em->flush();
        $output->writeln("Ilość wysłanych maili: {$sentMailsCount}.");
    }
    
    private function getRecipients($mailsCountToSend, $doctrine)
    {
        $recipientsAsSubscribers = $doctrine
                ->getRepository('LmCmsBundle:NewsletterMessageRecipientAsSubscriber')
                ->getRecipientsToSendMessage($mailsCountToSend);
        $recipientsAsSubscribersCount = count($recipientsAsSubscribers);
        
        if ($recipientsAsSubscribersCount === $mailsCountToSend) {
            return $recipientsAsSubscribers;
        }
        
        $recipientsAsEmails = $doctrine
                ->getRepository('LmCmsBundle:NewsletterMessageRecipientAsEmail')
                ->getRecipientsToSendMessage($mailsCountToSend - $recipientsAsSubscribersCount);
        
        return array_merge($recipientsAsSubscribers, $recipientsAsEmails);
    }
    
    private function sendMessage($recipient, $em, $output) 
    {
        $mailer = $this->getContainer()->get('mailer');
        $senderMail = $this->getContainer()->getParameter('mailer_user');
        $cmsConfig = $this->getContainer()->get('cms_config');
        $recipientMail = $this->getRecipientEmail($recipient);
        $htmlMessage = $this->composeMessage($recipient);

        $messageEmail = \Swift_Message::newInstance()
                ->setSubject($recipient->getNewsletterMessage()->getTitle())
                ->setFrom($senderMail, $cmsConfig->get('ust_mailer_wiadomosc_od', 'Katalog kancelarie-odszkodowania'))
                ->setTo($recipientMail)
                ->setReplyTo('bok@kancelarie-odszkodowania.pl')
                ->setBody($htmlMessage, 'text/html');

        try {
            if ($mailer->send($messageEmail)) {
                //http://stackoverflow.com/questions/11393667/symfony2-swift-mailer-command-email-body-displayed-into-the-console
                //http://stackoverflow.com/questions/13122096/unable-to-send-e-mail-from-within-custom-symfony2-command-but-can-from-elsewhere
                //wykonuje sie po analizie kazdego elementu w petli, zeby odrazu wylapac wyjatki np. dlugi czas dostepu do serwera
                $spool = $mailer->getTransport()->getSpool();
                $transport = $this->getContainer()->get('swiftmailer.transport.real');
                $spool->flushQueue($transport);
                
                $output->writeln("Wysłane od {$senderMail} do {$recipientMail}");

                $recipient->setSentDate(new \DateTime());
                $em->persist($recipient);
                return true;
            }
        } catch (Exception $e) {
            $output->writeln("Błąd wysyłania maila od {$senderMail} do {$recipientMail}. Treść błędu: {$e->toString()}");
            
            return false;
        }
    }
    
    private function getRecipientEmail($recipient)
    {
        if ($recipient instanceof NewsletterMessageRecipientAsSubscriber) {
            return $recipient->getNewsletterSubscriber()->getEmail();
        }
        
        if ($recipient instanceof NewsletterMessageRecipientAsEmail) {
            return $recipient->getEmail();
        }
        
        throw new SendNewsletterCommandException('Invalid recipient or recipient does not have an email.');
    }
    
    private function composeMessage($recipient)
    {
        $router = $this->getContainer()->get('router');
        $templateEngine = $this->getContainer()->get('templating');
        $cmsHost = $this->getContainer()->getParameter('host_cms');
        $messageComposer = null;
        
        if ($recipient instanceof NewsletterMessageRecipientAsSubscriber) {
            $messageComposer = new NewsletterMessageComposerForSubscriberRecipient($router, $templateEngine, $cmsHost);
        }
        
        if ($recipient instanceof NewsletterMessageRecipientAsEmail) {
            $messageComposer = new NewsletterMessageComposerForEmailRecipient($templateEngine, $cmsHost);
        }
        
        if ($messageComposer === null) {
            throw new SendNewsletterCommandException('Unknown newsletter message subscriber.');
        }
        
        return $messageComposer->compose($recipient);
    }
}

class SendNewsletterCommandException extends \Exception {}
