<?php

namespace Lm\CmsBundle\Command;

use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Doctrine\ORM\Query\Expr\Join;
use Lm\CmsBundle\Entity\OfferPromotionExpirationNotification;

class CreateOfferPromotionExpirationNotificationsCommand extends ContainerAwareCommand
{    
    protected function configure()
    {
        $this
            ->setName('offer:create-promotion-expiration-notifications')
            ->setDescription('Tworzenie powiadomień o wygaśnięciu promocji na wyświetlanie niektórych pól.')
            ->addArgument('max-offers-count', InputArgument::REQUIRED, 'Maksymalna ilość ofert, dla których zostaną utworzone powiadomienia.');
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        // fix dla hostingu linuxpl
        date_default_timezone_set('Europe/Warsaw');
        
        $maxOffersCount = intval($input->getArgument('max-offers-count'), 10);
        $createdNotificationsForAdditionalAddressesCount = 0;
        $createdNotificationsForVisiblePropertiesCount = 0;
        $output->writeln('Maksymalna ilość ofert: ' . $maxOffersCount);
        
        if ($maxOffersCount <= 0) {
            return;
        }
        
        $doctrine = $this->getContainer()->get('doctrine');
        $em = $doctrine->getEntityManager();
        
        $offersWithPromotionForAdditionalAddresses = 
                $this->getOffersWithPromotionForAdditionalAddresses($em, $maxOffersCount);
        $maxOffersCount -= count($offersWithPromotionForAdditionalAddresses);
        
        foreach ($offersWithPromotionForAdditionalAddresses as $offer) {
            $notification = new OfferPromotionExpirationNotification(
                    $offer, 
                    OfferPromotionExpirationNotification::FOR_ADDITIONAL_ADDRESSES,
                    $offer->getExpirationOfPromotionForAdditionalAddresses());
            
            $em->persist($notification);
            $createdNotificationsForAdditionalAddressesCount++;
        }
        
        $em->flush();
        $em->clear();
        
        $offersWithPromotionForVisibleProperties = 
                $this->getOffersWithPromotionForVisibleProperties($em, $maxOffersCount);
        
        foreach ($offersWithPromotionForVisibleProperties as $offer) {
            $notification = new OfferPromotionExpirationNotification(
                    $offer, 
                    OfferPromotionExpirationNotification::FOR_VISIBLE_PROPERTIES,
                    $offer->getExpirationOfPromotionForVisibleProperties());
            
            $em->persist($notification);
            $createdNotificationsForVisiblePropertiesCount++;
        }
        
        $em->flush();
        $output->writeln("Ilość utworzonych powiadomień:");
        $output->writeln("Dla dodatkowych adresów: {$createdNotificationsForAdditionalAddressesCount}.");
        $output->writeln("Dla widocznych pól: {$createdNotificationsForVisiblePropertiesCount}.");
    }
    
    private function getOffersWithPromotionForAdditionalAddresses($em, $maxOffersCount)
    {
        $currentDate = new \DateTime();
        $notificationStartDate = $currentDate->add(\DateInterval::createFromDateString('1 months'));
        
        $qb = $em
                ->getRepository('LmCmsBundle:Offer')
                ->createQueryBuilder('o');
        
        return $qb
                ->select('partial o.{id,expirationOfPromotionForAdditionalAddresses}')
                ->leftJoin('o.promotionExpirationNotifications', 'n', Join::WITH, $qb->expr()->andX(
                    $qb->expr()->eq('n.type', ':notificationType'),
                    $qb->expr()->eq('o.expirationOfPromotionForAdditionalAddresses', 'n.forExpirationDate')
                ))
                ->andWhere('n.id is null')
                ->andWhere('o.expirationOfPromotionForAdditionalAddresses <= :notificationStartDate')
                ->orderBy('o.expirationOfPromotionForAdditionalAddresses', 'asc')
                ->setParameter('notificationStartDate', $notificationStartDate)
                ->setParameter('notificationType', OfferPromotionExpirationNotification::FOR_ADDITIONAL_ADDRESSES)
                ->setMaxResults($maxOffersCount)
                ->getQuery()
                ->getResult();
    }
    
    private function getOffersWithPromotionForVisibleProperties($em, $maxOffersCount)
    {
        $currentDate = new \DateTime();
        $notificationStartDate = $currentDate->add(\DateInterval::createFromDateString('1 months'));
        
        $qb = $em
                ->getRepository('LmCmsBundle:Offer')
                ->createQueryBuilder('o');
        
        return $qb
                ->select('partial o.{id,expirationOfPromotionForVisibleProperties}')
                ->leftJoin('o.promotionExpirationNotifications', 'n', Join::WITH, $qb->expr()->andX(
                    $qb->expr()->eq('n.type', ':notificationType'),
                    $qb->expr()->eq('o.expirationOfPromotionForVisibleProperties', 'n.forExpirationDate')
                ))
                ->andWhere('n.id is null')
                ->andWhere('o.expirationOfPromotionForVisibleProperties <= :notificationStartDate')
                ->orderBy('o.expirationOfPromotionForVisibleProperties', 'asc')
                ->setParameter('notificationStartDate', $notificationStartDate)
                ->setParameter('notificationType', OfferPromotionExpirationNotification::FOR_VISIBLE_PROPERTIES)
                ->setMaxResults($maxOffersCount)
                ->getQuery()
                ->getResult();
    }
}
