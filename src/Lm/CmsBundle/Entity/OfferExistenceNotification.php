<?php

namespace Lm\CmsBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Entity()
 * @ORM\Table(name="offer_existence_notification")
 * @ORM\HasLifecycleCallbacks()
 */
class OfferExistenceNotification {

    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity="Offer", inversedBy="existenceNotifications")
     */
    private $offer;
    
    /**
     * @ORM\Column(type="integer", nullable=false)
     */
    private $forYearsInSystem;
    
    /**
     * @Gedmo\Timestampable(on="create")
     * @ORM\Column(type="datetime", nullable=false)
     */
    private $created;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     */
    private $sentDate;

    public function __construct(\Lm\CmsBundle\Entity\Offer $offer)
    {
        $this->offer = $offer;
        
        $now = new \DateTime();
        $this->forYearsInSystem = $now->format('Y') - $offer->getCreated()->format('Y');
    }
    
    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set forYearsInSystem
     *
     * @param integer $forYearsInSystem
     * @return OfferExistenceNotification
     */
    public function setForYearsInSystem($forYearsInSystem)
    {
        $this->forYearsInSystem = $forYearsInSystem;
    
        return $this;
    }

    /**
     * Get forYearsInSystem
     *
     * @return integer 
     */
    public function getForYearsInSystem()
    {
        return $this->forYearsInSystem;
    }

    /**
     * Set created
     *
     * @param \DateTime $created
     * @return OfferExistenceNotification
     */
    public function setCreated($created)
    {
        $this->created = $created;
    
        return $this;
    }

    /**
     * Get created
     *
     * @return \DateTime 
     */
    public function getCreated()
    {
        return $this->created;
    }

    /**
     * Set sentDate
     *
     * @param \DateTime $sentDate
     * @return OfferExistenceNotification
     */
    public function setSentDate($sentDate)
    {
        $this->sentDate = $sentDate;
    
        return $this;
    }

    /**
     * Get sentDate
     *
     * @return \DateTime 
     */
    public function getSentDate()
    {
        return $this->sentDate;
    }

    /**
     * Set offer
     *
     * @param \Lm\CmsBundle\Entity\Offer $offer
     * @return OfferExistenceNotification
     */
    public function setOffer(\Lm\CmsBundle\Entity\Offer $offer = null)
    {
        $this->offer = $offer;
    
        return $this;
    }

    /**
     * Get offer
     *
     * @return \Lm\CmsBundle\Entity\Offer 
     */
    public function getOffer()
    {
        return $this->offer;
    }
}
