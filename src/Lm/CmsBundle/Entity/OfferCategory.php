<?php

namespace Lm\CmsBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Lm\CmsBundle\Helper\Cms;
use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;
use Symfony\Component\Validator\Constraints as Assert;
use Lm\CmsBundle\Util\Province;

/**
 * @ORM\Entity(repositoryClass="Lm\CmsBundle\Repository\OfferCategoryRepository")
 * @ORM\Table(name="offer_category", indexes={
 * @ORM\Index(name="offer_category_name_index", columns={"name"})
 * })
 * @ORM\HasLifecycleCallbacks()
 */
class OfferCategory 
{
    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue
     */
    private $id;

    /**
     * @var string $name
     *
     * @ORM\Column(type="string", length=255)
     */
    private $name;
    
    /**
     * @var string $slug
     * 
     * @Gedmo\Slug(fields={"name"})
     * @ORM\Column(type="string", length=255, nullable=true );
     * @Assert\MaxLength(255);
     */
    private $slug;

    /**
     * @var string $description
     *
     * @ORM\Column(type="text", length=160, nullable=true)
     * @Assert\MaxLength(160);
     */
    private $description;
    
    /**
     * @var string $metaDescription
     *
     * @ORM\Column(type="string", length=255, nullable=true)
     * @Assert\MaxLength(255);
     */
    private $metaDescription;
    
    /** @Assert\File(maxSize="6000000") */
    public $_file_image;

    /**
     * @var string
     *
     * @ORM\Column(name="image", type="string", length=255, nullable=true)
     */
    private $image;
    
    /**
     * @ORM\Column(type="integer", nullable=false)
     */
    private $position;

    /**
     * @var datetime $created
     *
     * @Gedmo\Timestampable(on="create")
     * @ORM\Column(type="datetime", nullable=true)
     */
    private $created;

    /**
     * @var datetime $updated
     *
     * @Gedmo\Timestampable(on="update")
     * @ORM\Column(type="datetime", nullable=true)
     */
    private $updated;
    
    /**
     * @ORM\ManyToMany(targetEntity="Offer", mappedBy="categories")
     **/
    private $offers;
    
    /**
     * @ORM\ManyToOne(targetEntity="OfferCategoryAdvert", cascade={"persist"})
     * @ORM\JoinColumn(name="OfferCategoryAdvertId", referencedColumnName="id", onDelete="SET NULL", nullable=true)
     */
    private $advert;
    
    public function __construct() {
        $this->offers = new ArrayCollection();
    }

    public function __toString() {
        return '' . $this->name;
    }

    /**
     * @ORM\PrePersist();
     * @ORM\PreUpdate();
     */
    public function setCreatedValue() {
        $cms_helper = new Cms();
        $this->setSlug($cms_helper->make_url($this->getName()));
    }

    /**
     * @ORM\PrePersist()
     * @ORM\PreUpdate()
     */
    public function prepareUploadedNames() {
        foreach (array('image') as $_fi) {
            $_file = '_file_' . $_fi;

            if (null !== $this->{$_file}) {
                // do whatever you want to generate a unique name
                $this->{'old' . $_file} = $this->getAbsolutePath($_fi);
                $this->{$_fi} = $_fi . '-' . uniqid() . '.' . $this->{$_file}->guessExtension();
            }
        }
    }

    /**
     *
     * @ORM\PostPersist()
     * @ORM\PostUpdate()
     */
    public function storeUploadedFiles() {
        foreach (array('image') as $_fi) {

            $_file = '_file_' . $_fi;

            if (null === $this->{$_file}) {
                continue;
            }

            $this->{$_file}->move($this->getUploadRootDir($_fi), $this->{$_fi});

            if ($this->{'old' . $_file}) {
                @unlink($this->{'old' . $_file});
            }

            unset($this->{$_file});
        }
    }

    /**
     * @ORM\PostRemove()
     */
    public function removeUploadedFiles() {
        if ($file = $this->getAbsolutePath('image')) {
            @unlink($file);
        }
    }

    public function getAbsolutePath($_fil) {
        $file = null;
        switch ($_fil) {

            case 'image':
            default:
                $file = $this->getImage();
        }
        return !$file ? null : $this->getUploadRootDir() . '/' . $file;
    }

    public function getWebPath($_fil = 'image') {
        $file = null;
        switch ($_fil) {

            case 'image':
            default:
                $file = $this->getImage();
        }
        return !$file ? null : $this->getUploadDir() . '/' . $file;
    }

    protected function getUploadRootDir() {
        // the absolute directory path where uploaded documents should be saved
        return __DIR__ . '/../../../../web/' . $this->getUploadDir();
    }

    protected function getUploadDir() {
        return 'uploads/offer-category/' . $this->getId();
    }
    
    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     * @return OfferCategory
     */
    public function setName($name)
    {
        $this->name = $name;
    
        return $this;
    }

    /**
     * Get name
     *
     * @return string 
     */
    public function getName()
    {
        return $this->name;
    }
    
    /**
     * Set description
     *
     * @param string $description
     * @return OfferCategory
     */
    public function setDescription($description)
    {
        $this->description = $description;
    
        return $this;
    }

    /**
     * Get description
     *
     * @return string 
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * Get meta description
     *
     * @return string 
     */
    public function getMetaDescription()
    {
        return $this->metaDescription;
    }
    /**
     * Set meta description
     *
     * @param string $metaDescription
     * @return OfferCategory
     */
    public function setMetaDescription($metaDescription)
    {
        $this->metaDescription = $metaDescription;
    
        return $this;
    }

    /**
     * Set slug
     *
     * @param string $slug
     * @return OfferCategory
     */
    public function setSlug($slug)
    {
        $this->slug = $slug;
    
        return $this;
    }

    /**
     * Get slug
     *
     * @return string 
     */
    public function getSlug()
    {
        return $this->slug;
    }

    /**
     * Set image
     *
     * @param string $image
     * @return OfferCategory
     */
    public function setImage($image)
    {
        $this->image = $image;
    
        return $this;
    }

    /**
     * Get image
     *
     * @return string 
     */
    public function getImage()
    {
        return $this->image;
    }

    /**
     * Set created
     *
     * @param \DateTime $created
     * @return OfferCategory
     */
    public function setCreated($created)
    {
        $this->created = $created;
    
        return $this;
    }

    /**
     * Get created
     *
     * @return \DateTime 
     */
    public function getCreated()
    {
        return $this->created;
    }

    /**
     * Set updated
     *
     * @param \DateTime $updated
     * @return OfferCategory
     */
    public function setUpdated($updated)
    {
        $this->updated = $updated;
    
        return $this;
    }

    /**
     * Get updated
     *
     * @return \DateTime 
     */
    public function getUpdated()
    {
        return $this->updated;
    }

    /**
     * Add offers
     *
     * @param \Lm\CmsBundle\Entity\Offer $offers
     * @return OfferCategory
     */
    public function addOffer(\Lm\CmsBundle\Entity\Offer $offers)
    {
        $this->offers[] = $offers;
    
        return $this;
    }

    /**
     * Remove offers
     *
     * @param \Lm\CmsBundle\Entity\Offer $offers
     */
    public function removeOffer(\Lm\CmsBundle\Entity\Offer $offers)
    {
        $this->offers->removeElement($offers);
    }

    /**
     * Get offers
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getOffers()
    {
        return $this->offers;
    }

    /**
     * Set advert
     *
     * @param \Lm\CmsBundle\Entity\OfferCategoryAdvert $advert
     * @return OfferCategory
     */
    public function setAdvert(\Lm\CmsBundle\Entity\OfferCategoryAdvert $advert = null)
    {
        $this->advert = $advert;
    
        return $this;
    }

    /**
     * Get advert
     *
     * @return \Lm\CmsBundle\Entity\OfferCategoryAdvert 
     */
    public function getAdvert()
    {
        return $this->advert;
    }

    /**
     * Set position
     *
     * @param integer $position
     * @return OfferCategory
     */
    public function setPosition($position)
    {
        $this->position = $position;
    
        return $this;
    }

    /**
     * Get position
     *
     * @return integer 
     */
    public function getPosition()
    {
        return $this->position;
    }
}