<?php

namespace Lm\CmsBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity()
 * @ORM\Table(name="offer_additional_address")
 * @ORM\HasLifecycleCallbacks()
 */
class OfferAdditionalAddress extends OfferAddress
{
    /**
     * @ORM\ManyToOne(targetEntity="Offer", inversedBy="additionalAddresses")
     */
    private $offer;

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set city
     *
     * @param string $city
     * @return OfferAdditionalAddress
     */
    public function setCity($city)
    {
        $this->city = $city;
    
        return $this;
    }

    /**
     * Get city
     *
     * @return string 
     */
    public function getCity()
    {
        return $this->city;
    }

    /**
     * Set street
     *
     * @param string $street
     * @return OfferAdditionalAddress
     */
    public function setStreet($street)
    {
        $this->street = $street;
    
        return $this;
    }

    /**
     * Get street
     *
     * @return string 
     */
    public function getStreet()
    {
        return $this->street;
    }

    /**
     * Set postCode
     *
     * @param string $postCode
     * @return OfferAdditionalAddress
     */
    public function setPostCode($postCode)
    {
        $this->postCode = $postCode;
    
        return $this;
    }

    /**
     * Get postCode
     *
     * @return string 
     */
    public function getPostCode()
    {
        return $this->postCode;
    }

    /**
     * Set latitude
     *
     * @param float $latitude
     * @return OfferAdditionalAddress
     */
    public function setLatitude($latitude)
    {
        $this->latitude = $latitude;
    
        return $this;
    }

    /**
     * Get latitude
     *
     * @return float 
     */
    public function getLatitude()
    {
        return $this->latitude;
    }

    /**
     * Set longitude
     *
     * @param float $longitude
     * @return OfferAdditionalAddress
     */
    public function setLongitude($longitude)
    {
        $this->longitude = $longitude;
    
        return $this;
    }

    /**
     * Get longitude
     *
     * @return float 
     */
    public function getLongitude()
    {
        return $this->longitude;
    }

    /**
     * Set checksumOfLastCoordsCheck
     *
     * @param string $checksumOfLastCoordsCheck
     * @return OfferAdditionalAddress
     */
    public function setChecksumOfLastCoordsCheck($checksumOfLastCoordsCheck)
    {
        $this->checksumOfLastCoordsCheck = $checksumOfLastCoordsCheck;
    
        return $this;
    }

    /**
     * Get checksumOfLastCoordsCheck
     *
     * @return string 
     */
    public function getChecksumOfLastCoordsCheck()
    {
        return $this->checksumOfLastCoordsCheck;
    }

    /**
     * Set phone
     *
     * @param string $phone
     * @return OfferAdditionalAddress
     */
    public function setPhone($phone)
    {
        $this->phone = $phone;
    
        return $this;
    }

    /**
     * Get phone
     *
     * @return string 
     */
    public function getPhone()
    {
        return $this->phone;
    }

    /**
     * Set email
     *
     * @param string $email
     * @return OfferAdditionalAddress
     */
    public function setEmail($email)
    {
        $this->email = $email;
    
        return $this;
    }

    /**
     * Get email
     *
     * @return string 
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * Set created
     *
     * @param \DateTime $created
     * @return OfferAdditionalAddress
     */
    public function setCreated($created)
    {
        $this->created = $created;
    
        return $this;
    }

    /**
     * Get created
     *
     * @return \DateTime 
     */
    public function getCreated()
    {
        return $this->created;
    }

    /**
     * Set updated
     *
     * @param \DateTime $updated
     * @return OfferAdditionalAddress
     */
    public function setUpdated($updated)
    {
        $this->updated = $updated;
    
        return $this;
    }

    /**
     * Get updated
     *
     * @return \DateTime 
     */
    public function getUpdated()
    {
        return $this->updated;
    }

    /**
     * Set offer
     *
     * @param \Lm\CmsBundle\Entity\Offer $offer
     * @return OfferAdditionalAddress
     */
    public function setOffer(\Lm\CmsBundle\Entity\Offer $offer = null)
    {
        $this->offer = $offer;
    
        return $this;
    }

    /**
     * Get offer
     *
     * @return \Lm\CmsBundle\Entity\Offer 
     */
    public function getOffer()
    {
        return $this->offer;
    }
}