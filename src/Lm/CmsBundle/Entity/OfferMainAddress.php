<?php

namespace Lm\CmsBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use Lm\CmsBundle\Util\Province;

/**
 * @ORM\Entity()
 * @ORM\Table(name="offer_main_address")
 * @ORM\HasLifecycleCallbacks()
 */
class OfferMainAddress extends OfferAddress
{
    /**
     * @ORM\Column(type="smallint", nullable=true)
     */
    private $province;
    
    /**
     * @ORM\Column(type="string", length=20, nullable=true)
     */
    private $phone2;
    
    /**
     * @ORM\Column(type="string", length=254, nullable=true)
     * @Assert\Email()
     */
    private $email2;

    public function getProvinceName() 
    {
        return Province::getName($this->province);
    }
    
    /**
     * Set province
     *
     * @param integer $province
     * @return OfferMainAddress
     */
    public function setProvince($province)
    {
        $this->province = $province;
    
        return $this;
    }

    /**
     * Get province
     *
     * @return integer 
     */
    public function getProvince()
    {
        return $this->province;
    }
    
    /**
     * Set phone2
     *
     * @param string $phone2
     * @return OfferMainAddress
     */
    public function setPhone2($phone2)
    {
        $this->phone2 = $phone2;
    
        return $this;
    }

    /**
     * Get phone2
     *
     * @return string 
     */
    public function getPhone2()
    {
        return $this->phone2;
    }

    /**
     * Set email2
     *
     * @param string $email2
     * @return OfferMainAddress
     */
    public function setEmail2($email2)
    {
        $this->email2 = $email2;
    
        return $this;
    }

    /**
     * Get email2
     *
     * @return string 
     */
    public function getEmail2()
    {
        return $this->email2;
    }

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set city
     *
     * @param string $city
     * @return OfferMainAddress
     */
    public function setCity($city)
    {
        $this->city = $city;
    
        return $this;
    }

    /**
     * Get city
     *
     * @return string 
     */
    public function getCity()
    {
        return $this->city;
    }

    /**
     * Set street
     *
     * @param string $street
     * @return OfferMainAddress
     */
    public function setStreet($street)
    {
        $this->street = $street;
    
        return $this;
    }

    /**
     * Get street
     *
     * @return string 
     */
    public function getStreet()
    {
        return $this->street;
    }

    /**
     * Set postCode
     *
     * @param string $postCode
     * @return OfferMainAddress
     */
    public function setPostCode($postCode)
    {
        $this->postCode = $postCode;
    
        return $this;
    }

    /**
     * Get postCode
     *
     * @return string 
     */
    public function getPostCode()
    {
        return $this->postCode;
    }

    /**
     * Set latitude
     *
     * @param float $latitude
     * @return OfferMainAddress
     */
    public function setLatitude($latitude)
    {
        $this->latitude = $latitude;
    
        return $this;
    }

    /**
     * Get latitude
     *
     * @return float 
     */
    public function getLatitude()
    {
        return $this->latitude;
    }

    /**
     * Set longitude
     *
     * @param float $longitude
     * @return OfferMainAddress
     */
    public function setLongitude($longitude)
    {
        $this->longitude = $longitude;
    
        return $this;
    }

    /**
     * Get longitude
     *
     * @return float 
     */
    public function getLongitude()
    {
        return $this->longitude;
    }

    /**
     * Set checksumOfLastCoordsCheck
     *
     * @param string $checksumOfLastCoordsCheck
     * @return OfferMainAddress
     */
    public function setChecksumOfLastCoordsCheck($checksumOfLastCoordsCheck)
    {
        $this->checksumOfLastCoordsCheck = $checksumOfLastCoordsCheck;
    
        return $this;
    }

    /**
     * Get checksumOfLastCoordsCheck
     *
     * @return string 
     */
    public function getChecksumOfLastCoordsCheck()
    {
        return $this->checksumOfLastCoordsCheck;
    }

    /**
     * Set phone
     *
     * @param string $phone
     * @return OfferMainAddress
     */
    public function setPhone($phone)
    {
        $this->phone = $phone;
    
        return $this;
    }

    /**
     * Get phone
     *
     * @return string 
     */
    public function getPhone()
    {
        return $this->phone;
    }

    /**
     * Set email
     *
     * @param string $email
     * @return OfferMainAddress
     */
    public function setEmail($email)
    {
        $this->email = $email;
    
        return $this;
    }

    /**
     * Get email
     *
     * @return string 
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * Set created
     *
     * @param \DateTime $created
     * @return OfferMainAddress
     */
    public function setCreated($created)
    {
        $this->created = $created;
    
        return $this;
    }

    /**
     * Get created
     *
     * @return \DateTime 
     */
    public function getCreated()
    {
        return $this->created;
    }

    /**
     * Set updated
     *
     * @param \DateTime $updated
     * @return OfferMainAddress
     */
    public function setUpdated($updated)
    {
        $this->updated = $updated;
    
        return $this;
    }

    /**
     * Get updated
     *
     * @return \DateTime 
     */
    public function getUpdated()
    {
        return $this->updated;
    }
}