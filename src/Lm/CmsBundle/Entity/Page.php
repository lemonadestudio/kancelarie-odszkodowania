<?php
namespace Lm\CmsBundle\Entity;


use Doctrine\ORM\Mapping\OrderBy;
use Doctrine\Common\Collections\ArrayCollection;
use Lm\CmsBundle\Helper\Cms;
use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;
use Symfony\Component\Validator\Constraints as Assert;
/**
 * @ORM\Entity(repositoryClass="Lm\CmsBundle\Repository\PageRepository")
 * @ORM\Table(name="page")
 * @ORM\HasLifecycleCallbacks()
 */
class Page
{


	/**
	 * @ORM\Id
	 * @ORM\Column(type="integer")
	 * @ORM\GeneratedValue
	 */
	private $id;

	/**
	 * @var string $title
	 *
	 * @ORM\Column(type="string", length=255);
	 * @Assert\NotNull();
	 * @Assert\MaxLength(255);
	 */
	private $title;


	/**
	 * @ORM\Column(type="boolean")
	 */
	private $published = true;


	/**
	 *
	 * @var \DateTime  $publishDate;
	 * @ORM\Column(type="datetime", nullable=true);
	 */
	private $publishDate;

	/**
	 * @var text $content
	 * @ORM\Column(type="text", nullable=true )
	 */
	private $content;


	/**
	 * @var string $description
	 *
	 * @ORM\Column(type="string", length=2048, nullable=true)
	 * @Assert\MaxLength(2048)
	 */
	private $description;

	/**
	 * @var string $keywords
	 *
	 * @ORM\Column(type="string", length=1024, nullable=true)
	 * @Assert\MaxLength(1024)
	 */
	private $keywords;

	/**
	 * @Gedmo\Slug(fields={"title"})
	 * @ORM\Column(type="string", length=255, unique=false);
	 */
	private $slug;

	/**
	 *
	 * @var unknown_type
	 * @ORM\Column(type="boolean", nullable=true );
	 */
	private $automaticSeo;

	/**
	 * @var datetime $created
	 *
	 * @Gedmo\Timestampable(on="create")
	 * @ORM\Column(type="datetime", nullable=true)
	 */
	private $created;

	/**
	 * @var datetime $updated
	 *
	 * @Gedmo\Timestampable(on="update")
	 * @ORM\Column(type="datetime", nullable=true)
	 */
	private $updated;


	/** @Assert\File(maxSize="6000000") */
	public $_file_image;

	/**
	 * @var string
	 *
	 * @ORM\Column(name="image", type="string", length=255, nullable=true)
	 */
	private $image;



	/**
	 * @ORM\Column(type="string", length=30, nullable=true);
	 *
	 * @Assert\Choice(choices={"artykuly", "wydarzenia"})
	 */
	private $type;


	/**
	 * @ORM\ManyToOne(targetEntity="Galeria")
	 * @Orm\JoinColumn(name="galeria_id", referencedColumnName="id", onDelete="SET NULL")
	 */
	protected $galeria;


	public function __construct() {

		$this->published = true;
		$this->automaticSeo  = true;
		$this->publishDate = new \DateTime();
		$this->locale = 'pl';

	}

	public function __toString() {

		return ''.$this->getTitle();

	}



	/**
	 * @ORM\PrePersist();
	 * @ORM\PreUpdate();
	 */
	public function setCreatedValue()
	{



		if($this->getAutomaticSeo()) {


			// urla
			$cms_helper = new Cms();
			// $this->setSlug($cms_helper->make_url($this->getTitle()));

			// description
			$description = strip_tags($this->getContent());
			$description = mb_substr(html_entity_decode($description), 0, 1048);

			// usunięcie nowych linii
			$description = preg_replace('@\v@', ' ', $description);
			// podwójnych białych znaków
			$description = preg_replace('@\h{2,}@', ' ', $description);

			// usunięcie ostatniego, niedokończonego zdania
			$description = preg_replace('@(.*)\..*@', '\1.', $description);

			// trim
			$description = trim($description);






			// keywords
			$keywords_arr = explode(' ', $this->getTitle().' '.$this->getDescription());

			$keywords = array();
			if(is_array($keywords_arr)) {
				foreach($keywords_arr as $kw) {
					$kw = trim($kw);
					$kw = preg_replace('@\.,;\'\"@', '', $kw);
					if(strlen($kw) >= 4 && !in_array($kw, $keywords)) {
						$keywords[] = $kw;
					}
					if(count($keywords) >= 10) {
						break;
					}
				}
			}

			$this->setDescription($description);
			$this->setKeywords(implode(', ', $keywords));
			$this->setAutomaticSeo(false);

		}
	}



	/**
	 * @ORM\PrePersist()
	 * @ORM\PreUpdate()
	 */
	public function prepareUploadedNames()
	{


		foreach( array('image') as $_fi) {
			$_file = '_file_'.$_fi;

			// 			var_dump($_file, $this->{$_file});

			if (null !== $this->{$_file}) {

				// do whatever you want to generate a unique name
				$this->{'old'.$_file} = $this->getAbsolutePath($_fi);
				$this->{$_fi} = $_fi.'-'.uniqid().'.'.$this->{$_file}->guessExtension();

			}

		}
		// 		die;
	}

	/**
	 *
	 * @ORM\PostPersist()
	 * @ORM\PostUpdate()
	 */
	public function storeUploadedFiles()
	{

		foreach( array('image') as $_fi) {

			$_file = '_file_'.$_fi;

			if (null === $this->{$_file}) {
				continue;
			}

			$this->{$_file}->move($this->getUploadRootDir($_fi), $this->{$_fi});

			if($this->{'old'.$_file}) {
				@unlink($this->{'old'.$_file});
			}

			unset($this->{$_file});


		}

	}

	/**
	 * @ORM\PostRemove()
	 */
	public function removeUploadedFiles()
	{
		if ($file = $this->getAbsolutePath('image')) {
			@unlink($file);
		}

	}


	public function getAbsolutePath($_fil)
	{
		$file = null;
		switch($_fil){

			case 'image':
			default:
				$file = $this->getImage();
		}
		return !$file ? null : $this->getUploadRootDir().'/'.$file;
	}

	public function getWebPath($_fil = 'image')
	{
		$file = null;
		switch($_fil){

			case 'image':
			default:
				$file = $this->getImage();
		}
		return !$file ? null : $this->getUploadDir().'/'.$file;
	}

	protected function getUploadRootDir()
	{
		// the absolute directory path where uploaded documents should be saved
		return __DIR__.'/../../../../web/'.$this->getUploadDir();
	}

	protected function getUploadDir()
	{
		return 'uploads/pages/'.$this->getId();
	}


    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }


    /**
     * Set title
     *
     * @param string $title
     * @return Page
     */
    public function setTitle($title)
    {
        $this->title = $title;

        return $this;
    }

    /**
     * Get title
     *
     * @return string
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * Set published
     *
     * @param boolean $published
     * @return Page
     */
    public function setPublished($published)
    {
        $this->published = $published;

        return $this;
    }

    /**
     * Get published
     *
     * @return boolean
     */
    public function getPublished()
    {
        return $this->published;
    }

    /**
     * Set publishDate
     *
     * @param \DateTime $publishDate
     * @return Page
     */
    public function setPublishDate($publishDate)
    {
        $this->publishDate = $publishDate;

        return $this;
    }

    /**
     * Get publishDate
     *
     * @return \DateTime
     */
    public function getPublishDate()
    {
        return $this->publishDate;
    }

    /**
     * Set content
     *
     * @param string $content
     * @return Page
     */
    public function setContent($content)
    {
        $this->content = $content;

        return $this;
    }

    /**
     * Get content
     *
     * @return string
     */
    public function getContent()
    {
        return $this->content;
    }

    /**
     * Set description
     *
     * @param string $description
     * @return Page
     */
    public function setDescription($description)
    {
        $this->description = $description;

        return $this;
    }

    /**
     * Get description
     *
     * @return string
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * Set keywords
     *
     * @param string $keywords
     * @return Page
     */
    public function setKeywords($keywords)
    {
        $this->keywords = $keywords;

        return $this;
    }

    /**
     * Get keywords
     *
     * @return string
     */
    public function getKeywords()
    {
        return $this->keywords;
    }

    /**
     * Set slug
     *
     * @param string $slug
     * @return Page
     */
    public function setSlug($slug)
    {
        $this->slug = $slug;

        return $this;
    }

    /**
     * Get slug
     *
     * @return string
     */
    public function getSlug()
    {
        return $this->slug;
    }

    /**
     * Set automaticSeo
     *
     * @param boolean $automaticSeo
     * @return Page
     */
    public function setAutomaticSeo($automaticSeo)
    {
        $this->automaticSeo = $automaticSeo;

        return $this;
    }

    /**
     * Get automaticSeo
     *
     * @return boolean
     */
    public function getAutomaticSeo()
    {
        return $this->automaticSeo;
    }

    /**
     * Set created
     *
     * @param \DateTime $created
     * @return Page
     */
    public function setCreated($created)
    {
        $this->created = $created;

        return $this;
    }

    /**
     * Get created
     *
     * @return \DateTime
     */
    public function getCreated()
    {
        return $this->created;
    }

    /**
     * Set updated
     *
     * @param \DateTime $updated
     * @return Page
     */
    public function setUpdated($updated)
    {
        $this->updated = $updated;

        return $this;
    }

    /**
     * Get updated
     *
     * @return \DateTime
     */
    public function getUpdated()
    {
        return $this->updated;
    }

    /**
     * Set image
     *
     * @param string $image
     * @return Page
     */
    public function setImage($image)
    {
        $this->image = $image;

        return $this;
    }

    /**
     * Get image
     *
     * @return string
     */
    public function getImage()
    {
        return $this->image;
    }




    /**
     * Set type
     *
     * @param string $type
     * @return Page
     */
    public function setType($type)
    {
        $this->type = $type;
    
        return $this;
    }

    /**
     * Get type
     *
     * @return string 
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * Set galeria
     *
     * @param \Lm\CmsBundle\Entity\Galeria $galeria
     * @return Page
     */
    public function setGaleria(\Lm\CmsBundle\Entity\Galeria $galeria = null)
    {
        $this->galeria = $galeria;
    
        return $this;
    }

    /**
     * Get galeria
     *
     * @return \Lm\CmsBundle\Entity\Galeria 
     */
    public function getGaleria()
    {
        return $this->galeria;
    }
}