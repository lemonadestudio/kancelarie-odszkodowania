<?php

namespace Lm\CmsBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use Gedmo\Mapping\Annotation as Gedmo;

/**
 * @ORM\Entity()
 * @ORM\Table(name="offer_comment")
 * @ORM\HasLifecycleCallbacks()
 */
class OfferComment {

    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue
     */
    private $id;

    /**
     * @var string $title
     *
     * @ORM\Column(type="string", length=1024, nullable=true)
     * @Assert\Length(max=1000)
     * @Assert\NotBlank()
     */
    private $comment;
    
    /**
     * @var string
     *
     * @ORM\Column(type="string", length=255, nullable=true)
     * @Assert\Length(max=100)
     */
    private $buy_date;
    
    /**
     * @var string
     *
     * @ORM\Column(type="string", length=255, nullable=true);
     * @Assert\Length(max=100);
     */
    private $buy_object;
    
    

    /**
     * @ORM\Column(type="integer", nullable=true );
     */
    private $positive;

    /**
     * @ORM\Column(type="boolean", nullable=true );
     */
    private $approved;

    /**
     * @ORM\ManyToOne(targetEntity="Offer", inversedBy="comments")
     */
    private $offer;

    /**
     * @var datetime $created
     *
     * @Gedmo\Timestampable(on="create")
     * @ORM\Column(type="datetime", nullable=true)
     */
    private $created;

    /**
     * @var datetime $updated
     *
     * @Gedmo\Timestampable(on="update")
     * @ORM\Column(type="datetime", nullable=true)
     */
    private $updated;

    /**
     * @var string
     * @ORM\Column(type="string", length=64, nullable=true);
     */
    private $client_ip;
    
    /**
     * @var string
     */
    private $action;

    public function __construct()
    {
        $this->approved = 1;
    }
    
    public function __toString()
    {
        
        return mb_substr($this->comment, 0, 60);
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set comment
     *
     * @param string $comment
     * @return OfferComment
     */
    public function setComment($comment)
    {
        $this->comment = $comment;

        return $this;
    }

    /**
     * Get comment
     *
     * @return string
     */
    public function getComment()
    {
        return $this->comment;
    }

    /**
     * Set positive
     *
     * @param integer $positive
     * @return OfferComment
     */
    public function setPositive($positive)
    {
        $this->positive = $positive;

        return $this;
    }

    /**
     * Get positive
     *
     * @return integer
     */
    public function getPositive()
    {
        return $this->positive;
    }

    /**
     * Set approved
     *
     * @param boolean $approved
     * @return OfferComment
     */
    public function setApproved($approved)
    {
        $this->approved = $approved;

        return $this;
    }

    /**
     * Get approved
     *
     * @return boolean
     */
    public function getApproved()
    {
        return $this->approved;
    }

    /**
     * Set offer
     *
     * @param \Lm\CmsBundle\Entity\Offer $offer
     * @return OfferComment
     */
    public function setOffer(\Lm\CmsBundle\Entity\Offer $offer = null)
    {
        $this->offer = $offer;

        return $this;
    }

    /**
     * Get offer
     *
     * @return \Lm\CmsBundle\Entity\Offer
     */
    public function getOffer()
    {
        return $this->offer;
    }


    /**
     * Set created
     *
     * @param \DateTime $created
     * @return OfferComment
     */
    public function setCreated($created)
    {
        $this->created = $created;
    
        return $this;
    }

    /**
     * Get created
     *
     * @return \DateTime 
     */
    public function getCreated()
    {
        return $this->created;
    }

    /**
     * Set updated
     *
     * @param \DateTime $updated
     * @return OfferComment
     */
    public function setUpdated($updated)
    {
        $this->updated = $updated;
    
        return $this;
    }

    /**
     * Get updated
     *
     * @return \DateTime 
     */
    public function getUpdated()
    {
        return $this->updated;
    }

    /**
     * Set client_ip
     *
     * @param string $clientIp
     * @return OfferComment
     */
    public function setClientIp($clientIp)
    {
        $this->client_ip = $clientIp;
    
        return $this;
    }

    /**
     * Get client_ip
     *
     * @return string 
     */
    public function getClientIp()
    {
        return $this->client_ip;
    }

    /**
     * Set buy_date
     *
     * @param string $buyDate
     * @return OfferComment
     */
    public function setBuyDate($buyDate)
    {
        $this->buy_date = $buyDate;
    
        return $this;
    }

    /**
     * Get buy_date
     *
     * @return string 
     */
    public function getBuyDate()
    {
        return $this->buy_date;
    }

    /**
     * Set buy_object
     *
     * @param string $buyObject
     * @return OfferComment
     */
    public function setBuyObject($buyObject)
    {
        $this->buy_object = $buyObject;
    
        return $this;
    }

    /**
     * Get buy_object
     *
     * @return string 
     */
    public function getBuyObject()
    {
        return $this->buy_object;
    }
    
    public function getAction()
    {
        return $this->action;
    }

    public function setAction($action)
    {
        $this->action = $action;
        return $this;
    }


    
}