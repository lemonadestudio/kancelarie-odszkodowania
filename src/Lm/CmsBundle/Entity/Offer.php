<?php

namespace Lm\CmsBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Lm\CmsBundle\Helper\Cms;
use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Entity(repositoryClass="Lm\CmsBundle\Repository\OfferRepository")
 * @ORM\Table(name="offer", indexes={
 * @ORM\Index(name="offer_title_index", columns={"title"}),
 * @ORM\Index(name="offer_slug_index", columns={"slug"}),
 * @ORM\Index(name="offer_active_index", columns={"active"})
 * })
 * @ORM\HasLifecycleCallbacks()
 */
class Offer 
{
    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue
     */
    private $id;

    /**
     * @var string $title
     *
     * @ORM\Column(type="string", length=255)
     * @Assert\MaxLength(100)
     * @Assert\NotBlank( message = "Nazwa firmy: Uzupełnij to pole" )
     */
    private $title;

    /**
     * @var string $www
     *
     * @ORM\Column(type="string", length=255, nullable=true );
     * @Assert\MaxLength(200);
     */
    private $www;
    
    // w formularzu własności nie powiązane z bazą
    // po co?!! wtf?!!
    public $home;
    public $local;

    /**
     * @var string $promoted
     * @ORM\Column(type="date", nullable=true)
     */
    private $promoted;

    /**
     * @var text $content
     * @ORM\Column(type="text", nullable=true )
     * @Assert\Length(max=20000)
     */
    private $content;

    /**
     * @var text $content
     * @ORM\Column(type="text", nullable=true )
     * @Assert\Length(max=20000)
     */
    private $content2;

    /**
     * @var string $description
     *
     * @ORM\Column(type="string", length=255, nullable=true);
     * @Assert\MaxLength(255);
     */
    private $seoTitle;
    
    /**
     * @var string $description
     *
     * @ORM\Column(type="string", length=2048, nullable=true);
     * @Assert\MaxLength(2048);
     */
    private $description;

    /**
     * @var string $keywords
     *
     * @ORM\Column(type="string", length=1024, nullable=true)
     * @Assert\MaxLength(1000)
     */
    private $keywords;

    /**
     * @Gedmo\Slug(fields={"title"})
     * @ORM\Column(type="string", length=255, unique=false);
     */
    private $slug;

    /**
     *
     * @var unknown_type
     * @ORM\Column(type="boolean", nullable=true );
     */
    private $automaticSeo;

    /**
     * @var datetime $created
     *
     * @Gedmo\Timestampable(on="create")
     * @ORM\Column(type="datetime", nullable=true)
     */
    private $created;

    /**
     * @var datetime $updated
     *
     * @ORM\Column(type="datetime", nullable=true)
     */
    private $updated;

    /** @Assert\File(maxSize="1048576", maxSizeMessage="Plik jest za duży. Maksymalna wielkość to 1MB.") */
    public $_file_image;
    public $_delete_file_image;

    /**
     * @var string
     *
     * @ORM\Column(name="image", type="string", length=255, nullable=true)
     */
    private $image;

    /**
     * @ORM\Column(type="boolean", nullable=false);
     */
    private $createdUsingImport = false;
    
    /**
     * @ORM\Column(type="integer", nullable=true)
     * @var unknown
     */
    private $importedFileNumber;

    /**
     * @ORM\Column(type="integer", nullable=true)
     * @var unknown
     */
    private $view;

    /**
     * @ORM\Column(type="boolean", nullable=false)
     * @var unknown
     */
    private $active;

    /**
     * @ORM\Column(type="string", length=1024, nullable=true)
     * @Assert\MaxLength(1000)
     *
     * */
    private $confirmation_token;

    /**
     * @ORM\Column(type="string", length=1024, nullable=true)
     * @Assert\MaxLength(1000);
     *
     * */
    private $edit_token;

    /**
     * @ORM\Column(type="datetime", nullable=true);
     */
    private $edit_link_last_request;

    /**
     * @ORM\Column(type="string", length=1024, nullable=true)
     * @Assert\MaxLength(1000);
     *
     * */
    private $remove_token;

    /**
     * @ORM\Column(type="datetime", nullable=true);
     */
    private $remove_link_last_request;

    /**
     * @ORM\OneToMany(targetEntity="OfferComment", mappedBy="offer", cascade={"persist", "remove"} )
     * @ORM\OrderBy({"created" = "DESC"})
     */
    private $comments;
    
    /**
     * @ORM\ManyToMany(targetEntity="OfferCategory", inversedBy="offers")
     * @ORM\JoinTable(name="offer_to_offer_category")
     **/
    private $categories;
    
    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $facebookUrl;
    
    /**
     * @ORM\ManyToMany(targetEntity="OfferProperty", inversedBy="offers")
     * @ORM\JoinTable(name="offer_visible_property")
     **/
    private $visibleProperties;
    
    /**
     * @ORM\Column(type="datetime", nullable=true)
     */
    private $expirationOfPromotionForVisibleProperties;
    
    /**
     * @ORM\OneToMany(targetEntity="OfferExistenceNotification", mappedBy="offer", cascade={"persist", "remove"} )
     */
    private $existenceNotifications;
    
    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    private $yearsInSystemForLastExistenceNotification;
    
    /**
     * @ORM\OneToOne(targetEntity="OfferMainAddress", cascade={"persist", "remove"})
     */
    private $mainAddress;
    
    /**
     * @ORM\OneToMany(targetEntity="OfferAdditionalAddress", mappedBy="offer", orphanRemoval=true, cascade={"all"})
     */
    private $additionalAddresses;
    
    /**
     * @ORM\Column(type="datetime", nullable=true)
     */
    private $expirationOfPromotionForAdditionalAddresses;
    
    /**
     * @ORM\OneToMany(targetEntity="OfferPromotionExpirationNotification", mappedBy="offer", cascade={"persist", "remove"} )
     */
    private $promotionExpirationNotifications;
    
    public function __construct() {
        $this->automaticSeo = true;
        $this->view = 0;
        $this->active = 0;
        
        $this->categories = new ArrayCollection();
        $this->comments = new ArrayCollection();
        $this->visibleProperties = new ArrayCollection();
        $this->existenceNotifications = new ArrayCollection();
        $this->additionalAddresses = new ArrayCollection();
        $this->promotionExpirationNotifications = new ArrayCollection();
    }

    public function __toString() 
    {
        return '' . $this->getTitle();
    }

    /**
     * @ORM\PrePersist();
     * @ORM\PreUpdate();
     */
    public function setCreatedValue() {
        if ($this->getAutomaticSeo()) {
            // urla
            $cms_helper = new Cms();
            // $this->setSlug($cms_helper->make_url($this->getTitle()));
            // description
            $description = strip_tags($this->getContent()) . '.' . strip_tags($this->getContent2());
            $description = mb_substr(html_entity_decode($description), 0, 1048);

            // usunięcie nowych linii
            $description = preg_replace('@\v@', ' ', $description);
            // podwójnych białych znaków
            $description = preg_replace('@\h{2,}@', ' ', $description);

            // usunięcie ostatniego, niedokończonego zdania
            $description = preg_replace('@(.*)\..*@', '\1.', $description);

            // trim
            $description = trim($description);
            
            // keywords
            if (empty($this->keywords)) {
                $keywords_arr = explode(' ', $this->getTitle() . ' ' . $this->getDescription());
                $keywords = array();
                
                if (is_array($keywords_arr)) {
                    foreach ($keywords_arr as $kw) {
                        $kw = trim($kw);
                        $kw = preg_replace('@\.,;\'\"@', '', $kw);
                        if (strlen($kw) >= 4 && !in_array($kw, $keywords)) {
                            $keywords[] = $kw;
                        }
                        if (count($keywords) >= 10) {
                            break;
                        }
                    }
                }
                
                $this->setKeywords(implode(', ', $keywords));
            }

            $this->setDescription($description);
            $this->setAutomaticSeo(false);
        }
    }

    /**
     * @ORM\PrePersist()
     * @ORM\PreUpdate()
     */
    public function prepareUploadedNames() {
        foreach (array('image') as $_fi) {
            $_file = '_file_' . $_fi;

            if (null !== $this->{$_file} && $this->{$_file}->isValid()) {
                // do whatever you want to generate a unique name
                $this->{'old' . $_file} = $this->getAbsolutePath($_fi);
                $this->{$_fi} = $_fi . '-' . uniqid() . '.' . $this->{$_file}->guessExtension();
            } elseif (true == $this->{'_delete' . $_file}) {
                @unlink($this->getAbsolutePath($_fi));
                $this->{$_fi} = null;
            }
        }
    }

    /**
     *
     * @ORM\PostPersist()
     * @ORM\PostUpdate()
     */
    public function storeUploadedFiles() {
        foreach (array('image') as $_fi) {
            $_file = '_file_' . $_fi;

            if (null === $this->{$_file} || !$this->{$_file}->isValid()) {
                continue;
            }

            $this->{$_file}->move($this->getUploadRootDir($_fi), $this->{$_fi});

            if ($this->{'old' . $_file}) {
                @unlink($this->{'old' . $_file});
            }

            unset($this->{$_file});
        }
    }

    /**
     * @ORM\PostRemove()
     */
    public function removeUploadedFiles() {
        if ($file = $this->getAbsolutePath('image')) {
            @unlink($file);
        }
    }

    public function getAbsolutePath($_fil) {
        $file = null;
        switch ($_fil) {

            case 'image':
            default:
                $file = $this->getImage();
        }
        return !$file ? null : $this->getUploadRootDir() . '/' . $file;
    }

    public function getWebPath($_fil = 'image') {
        $file = null;
        switch ($_fil) {

            case 'image':
            default:
                $file = $this->getImage();
        }
        return !$file ? null : $this->getUploadDir() . '/' . $file;
    }

    protected function getUploadRootDir() {
        // the absolute directory path where uploaded documents should be saved
        return __DIR__ . '/../../../../web/' . $this->getUploadDir();
    }

    protected function getUploadDir() {
        return 'uploads/wizyt/' . $this->getId();
    }
    
    public function canGenerateNewEditToken() {
        $lastRequest = $this->getEditLinkLastRequest();
        $now = new \DateTime();
        
        if (!($lastRequest instanceof \DateTime)) {
            return true;
        }
        
        // token mozna wygenerowac raz na 15 min
        $nowTime = strtotime($now->format('Y-m-d H:i:s'));
        $lastReqTime = strtotime($lastRequest->format('Y-m-d H:i:s'));
        
        return ($nowTime - $lastReqTime) >= (15 * 60);
    }
    
    public function canGenerateNewDeleteToken() {
        $lastRequest = $this->getRemoveLinkLastRequest();
        $now = new \DateTime();
        
        if (!($lastRequest instanceof \DateTime)) {
            return true;
        }
        
        // token mozna wygenerowac raz na 15 min
        $nowTime = strtotime($now->format('Y-m-d H:i:s'));
        $lastReqTime = strtotime($lastRequest->format('Y-m-d H:i:s'));
        
        return ($nowTime - $lastReqTime) >= (15 * 60);
    }
    
    public function isPromoted() {
        $promoted = $this->getPromoted();

        if (!$promoted) {
            return false;
        }

        $prom = $promoted->format('y-m-d');
        $prom_time = strtotime($prom);
        $date = strtotime("-7 day");

        return $prom_time >= $date;
    }

    public function getCategoriesAsString() {
        $categories = '';
        
        if (!$this->categories) {
            return $categories;
        }
        
        foreach ($this->categories as $category) {
            if (!empty($categories)) {
                $categories .= ', ';
            }
            
            $categories .= $category;
        }
        
        return $categories;
    }

    public function setCategories(\Doctrine\Common\Collections\ArrayCollection $categories)
    {
        $this->categories = $categories;
    
        return $this;
    }
    
    public function getWwwWithHtmlPrefix() 
    {
        if (empty($this->www))
            return '';
        
        if (strpos($this->www, 'http://') === false)
            return 'http://' . $this->www;
        
        return $this->www;
    }
    
    public function isImportedFromSecondFile()
    {
        return $this->importedFileNumber === 2;
    }
    
    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set title
     *
     * @param string $title
     * @return Offer
     */
    public function setTitle($title)
    {
        $this->title = $title;
    
        return $this;
    }

    /**
     * Get title
     *
     * @return string 
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * Set www
     *
     * @param string $www
     * @return Offer
     */
    public function setWww($www)
    {
        $this->www = $www;
    
        return $this;
    }

    /**
     * Get www
     *
     * @return string 
     */
    public function getWww()
    {
        return $this->www;
    }

    /**
     * Set promoted
     *
     * @param \DateTime $promoted
     * @return Offer
     */
    public function setPromoted($promoted)
    {
        $this->promoted = $promoted;
    
        return $this;
    }

    /**
     * Get promoted
     *
     * @return \DateTime 
     */
    public function getPromoted()
    {
        return $this->promoted;
    }

    /**
     * Set content
     *
     * @param string $content
     * @return Offer
     */
    public function setContent($content)
    {
        $this->content = $content;
    
        return $this;
    }

    /**
     * Get content
     *
     * @return string 
     */
    public function getContent()
    {
        return $this->content;
    }

    /**
     * Set content2
     *
     * @param string $content2
     * @return Offer
     */
    public function setContent2($content2)
    {
        $this->content2 = $content2;
    
        return $this;
    }

    /**
     * Get content2
     *
     * @return string 
     */
    public function getContent2()
    {
        return $this->content2;
    }

    /**
     * Set description
     *
     * @param string $description
     * @return Offer
     */
    public function setDescription($description)
    {
        $this->description = $description;
    
        return $this;
    }

    /**
     * Get description
     *
     * @return string 
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * Set keywords
     *
     * @param string $keywords
     * @return Offer
     */
    public function setKeywords($keywords)
    {
        $this->keywords = $keywords;
    
        return $this;
    }

    /**
     * Get keywords
     *
     * @return string 
     */
    public function getKeywords()
    {
        return $this->keywords;
    }

    /**
     * Set slug
     *
     * @param string $slug
     * @return Offer
     */
    public function setSlug($slug)
    {
        $this->slug = $slug;
    
        return $this;
    }

    /**
     * Get slug
     *
     * @return string 
     */
    public function getSlug()
    {
        return $this->slug;
    }

    /**
     * Set automaticSeo
     *
     * @param boolean $automaticSeo
     * @return Offer
     */
    public function setAutomaticSeo($automaticSeo)
    {
        $this->automaticSeo = $automaticSeo;
    
        return $this;
    }

    /**
     * Get automaticSeo
     *
     * @return boolean 
     */
    public function getAutomaticSeo()
    {
        return $this->automaticSeo;
    }

    /**
     * Set created
     *
     * @param \DateTime $created
     * @return Offer
     */
    public function setCreated($created)
    {
        $this->created = $created;
    
        return $this;
    }

    /**
     * Get created
     *
     * @return \DateTime 
     */
    public function getCreated()
    {
        return $this->created;
    }

    /**
     * Set updated
     *
     * @param \DateTime $updated
     * @return Offer
     */
    public function setUpdated($updated)
    {
        $this->updated = $updated;
    
        return $this;
    }

    /**
     * Get updated
     *
     * @return \DateTime 
     */
    public function getUpdated()
    {
        return $this->updated;
    }

    /**
     * Set image
     *
     * @param string $image
     * @return Offer
     */
    public function setImage($image)
    {
        $this->image = $image;
    
        return $this;
    }

    /**
     * Get image
     *
     * @return string 
     */
    public function getImage()
    {
        return $this->image;
    }

    /**
     * Set view
     *
     * @param integer $view
     * @return Offer
     */
    public function setView($view)
    {
        $this->view = $view;
    
        return $this;
    }

    /**
     * Get view
     *
     * @return integer 
     */
    public function getView()
    {
        return $this->view;
    }

    /**
     * Set active
     *
     * @param boolean $active
     * @return Offer
     */
    public function setActive($active)
    {
        $this->active = $active;
    
        return $this;
    }

    /**
     * Get active
     *
     * @return boolean 
     */
    public function getActive()
    {
        return $this->active;
    }

    /**
     * Set confirmation_token
     *
     * @param string $confirmationToken
     * @return Offer
     */
    public function setConfirmationToken($confirmationToken)
    {
        $this->confirmation_token = $confirmationToken;
    
        return $this;
    }

    /**
     * Get confirmation_token
     *
     * @return string 
     */
    public function getConfirmationToken()
    {
        return $this->confirmation_token;
    }

    /**
     * Set edit_token
     *
     * @param string $editToken
     * @return Offer
     */
    public function setEditToken($editToken)
    {
        $this->edit_token = $editToken;
    
        return $this;
    }

    /**
     * Get edit_token
     *
     * @return string 
     */
    public function getEditToken()
    {
        return $this->edit_token;
    }

    /**
     * Set edit_link_last_request
     *
     * @param \DateTime $editLinkLastRequest
     * @return Offer
     */
    public function setEditLinkLastRequest($editLinkLastRequest)
    {
        $this->edit_link_last_request = $editLinkLastRequest;
    
        return $this;
    }

    /**
     * Get edit_link_last_request
     *
     * @return \DateTime 
     */
    public function getEditLinkLastRequest()
    {
        return $this->edit_link_last_request;
    }

    /**
     * Set remove_token
     *
     * @param string $removeToken
     * @return Offer
     */
    public function setRemoveToken($removeToken)
    {
        $this->remove_token = $removeToken;
    
        return $this;
    }

    /**
     * Get remove_token
     *
     * @return string 
     */
    public function getRemoveToken()
    {
        return $this->remove_token;
    }

    /**
     * Set remove_link_last_request
     *
     * @param \DateTime $removeLinkLastRequest
     * @return Offer
     */
    public function setRemoveLinkLastRequest($removeLinkLastRequest)
    {
        $this->remove_link_last_request = $removeLinkLastRequest;
    
        return $this;
    }

    /**
     * Get remove_link_last_request
     *
     * @return \DateTime 
     */
    public function getRemoveLinkLastRequest()
    {
        return $this->remove_link_last_request;
    }

    /**
     * Add comments
     *
     * @param \Lm\CmsBundle\Entity\OfferComment $comments
     * @return Offer
     */
    public function addComment(\Lm\CmsBundle\Entity\OfferComment $comments)
    {
        $this->comments[] = $comments;
    
        return $this;
    }

    /**
     * Remove comments
     *
     * @param \Lm\CmsBundle\Entity\OfferComment $comments
     */
    public function removeComment(\Lm\CmsBundle\Entity\OfferComment $comments)
    {
        $this->comments->removeElement($comments);
    }

    /**
     * Get comments
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getComments()
    {
        return $this->comments;
    }

    /**
     * Add categories
     *
     * @param \Lm\CmsBundle\Entity\OfferCategory $categories
     * @return Offer
     */
    public function addCategorie(\Lm\CmsBundle\Entity\OfferCategory $categories)
    {
        $this->categories[] = $categories;
    
        return $this;
    }

    /**
     * Remove categories
     *
     * @param \Lm\CmsBundle\Entity\OfferCategory $categories
     */
    public function removeCategorie(\Lm\CmsBundle\Entity\OfferCategory $categories)
    {
        $this->categories->removeElement($categories);
    }

    /**
     * Get categories
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getCategories()
    {
        return $this->categories;
    }

    /**
     * Set facebookUrl
     *
     * @param string $facebookUrl
     * @return Offer
     */
    public function setFacebookUrl($facebookUrl)
    {
        $this->facebookUrl = $facebookUrl;
    
        return $this;
    }

    /**
     * Get facebookUrl
     *
     * @return string 
     */
    public function getFacebookUrl()
    {
        return $this->facebookUrl;
    }

    /**
     * Add visibleProperties
     *
     * @param \Lm\CmsBundle\Entity\OfferProperty $visibleProperties
     * @return Offer
     */
    public function addVisiblePropertie(\Lm\CmsBundle\Entity\OfferProperty $visibleProperties)
    {
        $this->visibleProperties[] = $visibleProperties;
    
        return $this;
    }

    /**
     * Remove visibleProperties
     *
     * @param \Lm\CmsBundle\Entity\OfferProperty $visibleProperties
     */
    public function removeVisiblePropertie(\Lm\CmsBundle\Entity\OfferProperty $visibleProperties)
    {
        $this->visibleProperties->removeElement($visibleProperties);
    }

    /**
     * Get visibleProperties
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getVisibleProperties()
    {
        return $this->visibleProperties;
    }

    /**
     * Set yearsInSystemForLastExistenceNotification
     *
     * @param integer $yearsInSystemForLastExistenceNotification
     * @return Offer
     */
    public function setYearsInSystemForLastExistenceNotification($yearsInSystemForLastExistenceNotification)
    {
        $this->yearsInSystemForLastExistenceNotification = $yearsInSystemForLastExistenceNotification;
    
        return $this;
    }

    /**
     * Get yearsInSystemForLastExistenceNotification
     *
     * @return integer 
     */
    public function getYearsInSystemForLastExistenceNotification()
    {
        return $this->yearsInSystemForLastExistenceNotification;
    }

    /**
     * Add existenceNotifications
     *
     * @param \Lm\CmsBundle\Entity\OfferExistenceNotification $existenceNotifications
     * @return Offer
     */
    public function addExistenceNotification(\Lm\CmsBundle\Entity\OfferExistenceNotification $existenceNotifications)
    {
        $this->existenceNotifications[] = $existenceNotifications;
    
        return $this;
    }

    /**
     * Remove existenceNotifications
     *
     * @param \Lm\CmsBundle\Entity\OfferExistenceNotification $existenceNotifications
     */
    public function removeExistenceNotification(\Lm\CmsBundle\Entity\OfferExistenceNotification $existenceNotifications)
    {
        $this->existenceNotifications->removeElement($existenceNotifications);
    }

    /**
     * Get existenceNotifications
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getExistenceNotifications()
    {
        return $this->existenceNotifications;
    }

    /**
     * Set mainAddress
     *
     * @param \Lm\CmsBundle\Entity\OfferMainAddress $mainAddress
     * @return Offer
     */
    public function setMainAddress(\Lm\CmsBundle\Entity\OfferMainAddress $mainAddress = null)
    {
        $this->mainAddress = $mainAddress;
    
        return $this;
    }

    /**
     * Get mainAddress
     *
     * @return \Lm\CmsBundle\Entity\OfferMainAddress 
     */
    public function getMainAddress()
    {
        return $this->mainAddress;
    }

    /**
     * Add additionalAddresses
     *
     * @param \Lm\CmsBundle\Entity\OfferAdditionalAddress $additionalAddresses
     * @return Offer
     */
    public function addAdditionalAddresse(\Lm\CmsBundle\Entity\OfferAdditionalAddress $additionalAddresses)
    {
        $additionalAddresses->setOffer($this);
        $this->additionalAddresses[] = $additionalAddresses;
    
        return $this;
    }

    /**
     * Remove additionalAddresses
     *
     * @param \Lm\CmsBundle\Entity\OfferAdditionalAddress $additionalAddresses
     */
    public function removeAdditionalAddresse(\Lm\CmsBundle\Entity\OfferAdditionalAddress $additionalAddresses)
    {
        $this->additionalAddresses->removeElement($additionalAddresses);
    }

    /**
     * Get additionalAddresses
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getAdditionalAddresses()
    {
        return $this->additionalAddresses;
    }

    /**
     * Set createdUsingImport
     *
     * @param boolean $createdUsingImport
     * @return Offer
     */
    public function setCreatedUsingImport($createdUsingImport)
    {
        $this->createdUsingImport = $createdUsingImport;
    
        return $this;
    }

    /**
     * Get createdUsingImport
     *
     * @return boolean 
     */
    public function getCreatedUsingImport()
    {
        return $this->createdUsingImport;
    }

    /**
     * Set expirationOfPromotionForVisibleProperties
     *
     * @param \DateTime $expirationOfPromotionForVisibleProperties
     * @return Offer
     */
    public function setExpirationOfPromotionForVisibleProperties($expirationOfPromotionForVisibleProperties)
    {
        $this->expirationOfPromotionForVisibleProperties = $expirationOfPromotionForVisibleProperties;
    
        return $this;
    }

    /**
     * Get expirationOfPromotionForVisibleProperties
     *
     * @return \DateTime 
     */
    public function getExpirationOfPromotionForVisibleProperties()
    {
        return $this->expirationOfPromotionForVisibleProperties;
    }

    /**
     * Set expirationOfPromotionForAdditionalAddresses
     *
     * @param \DateTime $expirationOfPromotionForAdditionalAddresses
     * @return Offer
     */
    public function setExpirationOfPromotionForAdditionalAddresses($expirationOfPromotionForAdditionalAddresses)
    {
        $this->expirationOfPromotionForAdditionalAddresses = $expirationOfPromotionForAdditionalAddresses;
    
        return $this;
    }

    /**
     * Get expirationOfPromotionForAdditionalAddresses
     *
     * @return \DateTime 
     */
    public function getExpirationOfPromotionForAdditionalAddresses()
    {
        return $this->expirationOfPromotionForAdditionalAddresses;
    }

    /**
     * Add promotionExpirationNotifications
     *
     * @param \Lm\CmsBundle\Entity\OfferPromotionExpirationNotification $promotionExpirationNotifications
     * @return Offer
     */
    public function addPromotionExpirationNotification(\Lm\CmsBundle\Entity\OfferPromotionExpirationNotification $promotionExpirationNotifications)
    {
        $this->promotionExpirationNotifications[] = $promotionExpirationNotifications;
    
        return $this;
    }

    /**
     * Remove promotionExpirationNotifications
     *
     * @param \Lm\CmsBundle\Entity\OfferPromotionExpirationNotification $promotionExpirationNotifications
     */
    public function removePromotionExpirationNotification(\Lm\CmsBundle\Entity\OfferPromotionExpirationNotification $promotionExpirationNotifications)
    {
        $this->promotionExpirationNotifications->removeElement($promotionExpirationNotifications);
    }

    /**
     * Get promotionExpirationNotifications
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getPromotionExpirationNotifications()
    {
        return $this->promotionExpirationNotifications;
    }

    /**
     * Set seoTitle
     *
     * @param string $seoTitle
     * @return Offer
     */
    public function setSeoTitle($seoTitle)
    {
        $this->seoTitle = $seoTitle;
    
        return $this;
    }

    /**
     * Get seoTitle
     *
     * @return string 
     */
    public function getSeoTitle()
    {
        return $this->seoTitle;
    }

    /**
     * Set importedFileNumber
     *
     * @param integer $importedFileNumber
     * @return Offer
     */
    public function setImportedFileNumber($importedFileNumber)
    {
        $this->importedFileNumber = $importedFileNumber;
    
        return $this;
    }

    /**
     * Get importedFileNumber
     *
     * @return integer 
     */
    public function getImportedFileNumber()
    {
        return $this->importedFileNumber;
    }
}