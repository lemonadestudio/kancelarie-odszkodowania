<?php
namespace Lm\CmsBundle\Entity;


use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Entity
 * @ORM\Table(name="box")
 */
class Box
{
    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue
     */
    private $id;


    /**
     * @var string $title
     *
     * @ORM\Column(type="string", length=255)
     * @Assert\NotNull()
     * @Assert\MaxLength(255)
     */
    private $title;

    /**
    * @Gedmo\Slug(fields={"title"}, updatable=false)
    * @ORM\Column(type="string", length=255, unique=true)
    */
    private $slug;

    /**
     * @ORM\Column(type="boolean")
     */
    private $published = true;


    /**
     *
     * @var \DateTime  $publishDate;
     * @ORM\Column(type="datetime", nullable=true)
     */
    private $publishDate;


    /**
     * @var text $content
     * @ORM\Column(type="text", nullable=true)
     */
    private $content;


    public function __construct() {

            $this->published = true;
            $this->publishDate = new \DateTime();

    }

    public function __toString() {

            return ''.$this->title;

    }




    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }




    /**
     * Set title
     *
     * @param string $title
     */
    public function setTitle($title)
    {
        $this->title = $title;
    }

    /**
     * Get title
     *
     * @return string
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * Set slug
     *
     * @param string $slug
     */
    public function setSlug($slug)
    {
        $this->slug = $slug;
    }

    /**
     * Get slug
     *
     * @return string
     */
    public function getSlug()
    {
        return $this->slug;
    }

    /**
     * Set published
     *
     * @param boolean $published
     */
    public function setPublished($published)
    {
        $this->published = $published;
    }

    /**
     * Get published
     *
     * @return boolean
     */
    public function getPublished()
    {
        return $this->published;
    }



    /**
     * Set publishDate
     *
     * @param datetime $publishDate
     */
    public function setPublishDate($publishDate)
    {
        $this->publishDate = $publishDate;
    }

    /**
     * Get publishDate
     *
     * @return datetime
     */
    public function getPublishDate()
    {
        return $this->publishDate;
    }

    /**
     * Set content
     *
     * @param text $content
     */
    public function setContent($content)
    {
        $this->content = $content;
    }

    /**
     * Get content
     *
     * @return text
     */
    public function getContent()
    {
        return $this->content;
    }


}