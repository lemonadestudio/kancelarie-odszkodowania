<?php

namespace Lm\CmsBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Lm\CmsBundle\Helper\Cms;
use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Entity()
 * @ORM\Table(name="offer_promotion_expiration_notification")
 * @ORM\HasLifecycleCallbacks()
 */
class OfferPromotionExpirationNotification 
{
    const FOR_ADDITIONAL_ADDRESSES = 1;
    const FOR_VISIBLE_PROPERTIES = 2;
    
    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue
     */
    private $id;
    
    /**
     * @ORM\ManyToOne(targetEntity="Offer", inversedBy="promotionExpirationNotifications")
     */
    private $offer;
    
    /**
     * @ORM\Column(type="integer", nullable=false)
     */
    private $type;
    
    /**
     * @ORM\Column(type="datetime", nullable=false)
     */
    private $forExpirationDate;
    
    /**
     * @Gedmo\Timestampable(on="create")
     * @ORM\Column(type="datetime", nullable=false)
     */
    private $created;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     */
    private $sentDate;

    public function __construct(\Lm\CmsBundle\Entity\Offer $offer, $type, $expirationDate)
    {
        $this->offer = $offer;
        $this->type = $type;
        $this->forExpirationDate = $expirationDate;
    }
    
    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set type
     *
     * @param integer $type
     * @return OfferPromotionExpirationNotification
     */
    public function setType($type)
    {
        $this->type = $type;
    
        return $this;
    }

    /**
     * Get type
     *
     * @return integer 
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * Set forExpirationDate
     *
     * @param \DateTime $forExpirationDate
     * @return OfferPromotionExpirationNotification
     */
    public function setForExpirationDate($forExpirationDate)
    {
        $this->forExpirationDate = $forExpirationDate;
    
        return $this;
    }

    /**
     * Get forExpirationDate
     *
     * @return \DateTime 
     */
    public function getForExpirationDate()
    {
        return $this->forExpirationDate;
    }

    /**
     * Set created
     *
     * @param \DateTime $created
     * @return OfferPromotionExpirationNotification
     */
    public function setCreated($created)
    {
        $this->created = $created;
    
        return $this;
    }

    /**
     * Get created
     *
     * @return \DateTime 
     */
    public function getCreated()
    {
        return $this->created;
    }

    /**
     * Set sentDate
     *
     * @param \DateTime $sentDate
     * @return OfferPromotionExpirationNotification
     */
    public function setSentDate($sentDate)
    {
        $this->sentDate = $sentDate;
    
        return $this;
    }

    /**
     * Get sentDate
     *
     * @return \DateTime 
     */
    public function getSentDate()
    {
        return $this->sentDate;
    }

    /**
     * Set offer
     *
     * @param \Lm\CmsBundle\Entity\Offer $offer
     * @return OfferPromotionExpirationNotification
     */
    public function setOffer(\Lm\CmsBundle\Entity\Offer $offer = null)
    {
        $this->offer = $offer;
    
        return $this;
    }

    /**
     * Get offer
     *
     * @return \Lm\CmsBundle\Entity\Offer 
     */
    public function getOffer()
    {
        return $this->offer;
    }
}