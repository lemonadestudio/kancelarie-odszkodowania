<?php

namespace Lm\CmsBundle\Entity;

use Doctrine\ORM\EntityManager;
use Doctrine\ORM\Mapping\OrderBy;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;
use Symfony\Component\Validator\Constraints as Assert;

/**
 *
 * @ORM\Table(name="newsletter_message")
 * @ORM\Entity(repositoryClass="Lm\CmsBundle\Repository\NewsletterMessageRepository")
 * @ORM\HasLifecycleCallbacks();
 */
class NewsletterMessage
{
    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;
    
    /**
     * @ORM\Column(type="string", length=255, nullable=false)
     * @Assert\NotNull();
     */
    private $title;
    
    /**
     * @ORM\Column(type="text", nullable=false)
     * @Assert\NotBlank()
     */
    private $message;
    
    /**
     * @ORM\Column(type="datetime", nullable=true)
     */
    private $startSendDate;

    /**
     * @ORM\Column(type="datetime")
     */
    private $createdAt;
    
    /**
     * @ORM\Column(type="datetime", nullable=true)
     */
    private $updatedAt;
    
    /**
     * @ORM\OneToMany(
     *   targetEntity="NewsletterMessageRecipientAsSubscriber",
     *   mappedBy="newsletterMessage",
     *   cascade={"persist", "remove"}, fetch="EXTRA_LAZY"
     * )
     * @var \Doctrine\Common\Collections\Collection
     */
    private $recipientsAsSubscribers;
    
    /**
     * @ORM\OneToMany(
     *   targetEntity="NewsletterMessageRecipientAsEmail",
     *   mappedBy="newsletterMessage",
     *   cascade={"persist", "remove"}, fetch="EXTRA_LAZY"
     * )
     * @var \Doctrine\Common\Collections\Collection
     */
    private $recipientsAsEmails;
    
    // kontener na liste "id" klasy NewsletterSubscriber
    // potrzebne tylko do walidacji, bo sonata nie pozwala na walidacje
    // wlasciwosci formularza, ktore nie sa zmapowane do obiektu docelowego
    private $recipientIds;
    
    /** 
     * 1MB
     * @Assert\File(maxSize="1048576", mimeTypes={"text/plain", "text/csv"})
     */
    private $fileWithEmails;

    public function __construct()
    {
        $this->createdAt = new \DateTime();
    }
    
    public function __toString()
    {
        return '' . $this->title;
    }
    
    public function getShortMessage()
    {
        return substr($this->message, 0, 100);
    }
    
    public function getShortTitle()
    {
        return substr($this->title, 0, 100);
    }
    
    /**
     * Zapytania powoduja n+1. Mimo tego jest na liscie w adminie, bo trzeba wyswietlic jakies info. 
     * Niestety sonata nie pozwala na pobranie tych danych w jednym zapytaniu.
     */
    public function getRecipientsCount()
    {
        return $this->getRecipientsAsSubscribers()->count() + $this->getRecipientsAsEmails()->count();
    }
    
    public function getRecipientIds()
    {
        return $this->recipientIds;
    }
    
    public function setRecipientIds(array $recipientIds)
    {
        $this->recipientIds = $recipientIds;
    }
    
    public function getFileWithEmails()
    {
        return $this->fileWithEmails;
    }
    
    public function setFileWithEmails($fileWithEmails)
    {
        $this->fileWithEmails = $fileWithEmails;
    }
    
    public function setRecipientsAsEmailsUsingUploadedFile() {
        if (!isset($this->fileWithEmails)) {
            return;
        }
        
        $fileHandle = fopen($this->fileWithEmails->getPathname(), 'r');
        
        if ($fileHandle === false) {
            return;
        }
        
        while (($fileData = fgetcsv($fileHandle, 1000, ",")) !== false) {
            $columnsNumber = count($fileData);

            for ($i = 0; $i < $columnsNumber; $i++) {
                $email = trim($fileData[$i], " \t\n\r\0\x0B\xC2\xA0");
                
                if (empty($email) || !filter_var($email, FILTER_VALIDATE_EMAIL)) {
                    continue;
                }
                
                $recipient = new NewsletterMessageRecipientAsEmail();
                $recipient->setEmail($email);
                $recipient->setNewsletterMessage($this);
                
                $this->recipientsAsEmails[] = $recipient;
            }
        }

        fclose($fileHandle);
    }
    
    public function setRecipientsAsSubscribersUsingRecipientIds($getSubscriberReference) {
        foreach ($this->getRecipientIds() as $recipientId) {
            $recipient = new NewsletterMessageRecipientAsSubscriber();
            $recipient->setNewsletterSubscriber($getSubscriberReference($recipientId));
            $this->addRecipientsAsSubscriber($recipient);
            $recipient->setNewsletterMessage($this);
        }
    }
    
    /**
     * nie uzywac w listowaniu bo wyjda tragiczne zapytania (n+1)
     * dodatkowo powinno to byc listowanie ze stronicowaniem, ale problem stanowi 
     * sonata
     */
    public function getRecipientsAsString()
    {
        $recipients = '';
        
        foreach ($this->getRecipientsAsSubscribers() as $recipient)
        {
            if (strlen($recipients) > 0) {
                $recipients .= ', ';
            }
            
            $recipients .= $recipient->getNewsletterSubscriber()->getEmail();
        }
        
        foreach ($this->getRecipientsAsEmails() as $recipient)
        {
            if (strlen($recipients) > 0) {
                $recipients .= ', ';
            }
            
            $recipients .= $recipient->getEmail();
        }
        
        return $recipients;
    }
    
    /**
     * nie uzywac w listowaniu bo wyjda tragiczne zapytania (n+1)
     */
    public function getStatus()
    {
        $sentCount = 0;
        $recipientsCount = $this->getRecipientsCount();
        $lastSentDate = null;
        $allRecipients = array_merge(
                $this->getRecipientsAsSubscribers()->toArray(), 
                $this->getRecipientsAsEmails()->toArray());
        
        foreach ($allRecipients as $recipient)
        {
            $sentDate = $recipient->getSentDate();
            
            if ($sentDate !== null) {
                $sentCount++;
            }
            
            if ($sentDate > $lastSentDate) {
                $lastSentDate = $sentDate;
            }
        }
        
        if ($sentCount === 0) {
            return 'Jeszcze nie rozpoczęto wysyłania.';
        }
        
        if ($recipientsCount != $sentCount) {
            return 'Wiadomość została wysłana do ' . $sentCount . ' z ' . $recipientsCount . ' odbiorców.';
        }
        
        return 'Wiadomość została wysłana do wszystkich odbiorców. Data zakończenia: ' . $lastSentDate->format('Y-m-d H:i:s') . '.';
    }
    
    public function getStartSendDateText() 
    {
        if (!$this->getStartSendDate()) {
            return 'Natychmiast.';
        }
        
        return $this->getStartSendDate()->format('Y-m-d H:i:s');
    }
    

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set title
     *
     * @param string $title
     * @return NewsletterMessage
     */
    public function setTitle($title)
    {
        $this->title = $title;
    
        return $this;
    }

    /**
     * Get title
     *
     * @return string 
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * Set message
     *
     * @param string $message
     * @return NewsletterMessage
     */
    public function setMessage($message)
    {
        $this->message = $message;
    
        return $this;
    }

    /**
     * Get message
     *
     * @return string 
     */
    public function getMessage()
    {
        return $this->message;
    }

    /**
     * Set startSendDate
     *
     * @param \DateTime $startSendDate
     * @return NewsletterMessage
     */
    public function setStartSendDate($startSendDate)
    {
        $this->startSendDate = $startSendDate;
    
        return $this;
    }

    /**
     * Get startSendDate
     *
     * @return \DateTime 
     */
    public function getStartSendDate()
    {
        return $this->startSendDate;
    }

    /**
     * Set createdAt
     *
     * @param \DateTime $createdAt
     * @return NewsletterMessage
     */
    public function setCreatedAt($createdAt)
    {
        $this->createdAt = $createdAt;
    
        return $this;
    }

    /**
     * Get createdAt
     *
     * @return \DateTime 
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    /**
     * Set updatedAt
     *
     * @param \DateTime $updatedAt
     * @return NewsletterMessage
     */
    public function setUpdatedAt($updatedAt)
    {
        $this->updatedAt = $updatedAt;
    
        return $this;
    }

    /**
     * Get updatedAt
     *
     * @return \DateTime 
     */
    public function getUpdatedAt()
    {
        return $this->updatedAt;
    }

    /**
     * Add recipientsAsSubscribers
     *
     * @param \Lm\CmsBundle\Entity\NewsletterMessageRecipientAsSubscriber $recipientsAsSubscribers
     * @return NewsletterMessage
     */
    public function addRecipientsAsSubscriber(\Lm\CmsBundle\Entity\NewsletterMessageRecipientAsSubscriber $recipientsAsSubscribers)
    {
        $this->recipientsAsSubscribers[] = $recipientsAsSubscribers;
    
        return $this;
    }

    /**
     * Remove recipientsAsSubscribers
     *
     * @param \Lm\CmsBundle\Entity\NewsletterMessageRecipientAsSubscriber $recipientsAsSubscribers
     */
    public function removeRecipientsAsSubscriber(\Lm\CmsBundle\Entity\NewsletterMessageRecipientAsSubscriber $recipientsAsSubscribers)
    {
        $this->recipientsAsSubscribers->removeElement($recipientsAsSubscribers);
    }

    /**
     * Get recipientsAsSubscribers
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getRecipientsAsSubscribers()
    {
        return $this->recipientsAsSubscribers;
    }

    /**
     * Add recipientsAsEmails
     *
     * @param \Lm\CmsBundle\Entity\NewsletterMessageRecipientAsEmail $recipientsAsEmails
     * @return NewsletterMessage
     */
    public function addRecipientsAsEmail(\Lm\CmsBundle\Entity\NewsletterMessageRecipientAsEmail $recipientsAsEmails)
    {
        $this->recipientsAsEmails[] = $recipientsAsEmails;
    
        return $this;
    }

    /**
     * Remove recipientsAsEmails
     *
     * @param \Lm\CmsBundle\Entity\NewsletterMessageRecipientAsEmail $recipientsAsEmails
     */
    public function removeRecipientsAsEmail(\Lm\CmsBundle\Entity\NewsletterMessageRecipientAsEmail $recipientsAsEmails)
    {
        $this->recipientsAsEmails->removeElement($recipientsAsEmails);
    }

    /**
     * Get recipientsAsEmails
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getRecipientsAsEmails()
    {
        return $this->recipientsAsEmails;
    }
}