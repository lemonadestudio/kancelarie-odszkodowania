<?php

namespace Lm\CmsBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;
use Symfony\Component\Validator\Constraints as Assert;

abstract class OfferAddress
{
    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue
     */
    protected $id;

    /**
     * @ORM\Column(type="string", length=200, nullable=false)
     */
    protected $city;
    
    /**
     * @ORM\Column(type="string", length=200, nullable=true)
     */
    protected $street;
    
    /**
     * @ORM\Column(type="string", length=6, nullable=true)
     */
    protected $postCode;
    
    /**
     * @ORM\Column(type="float", nullable=true)
     */
    protected $latitude;
    
    /**
     * @ORM\Column(type="float", nullable=true)
     */
    protected $longitude;
    
    /**
     * @ORM\Column(type="string", length=32, nullable=true)
     */
    protected $checksumOfLastCoordsCheck;
    
    /**
     * @ORM\Column(type="string", length=20, nullable=true)
     */
    protected $phone;
    
    /**
     * @ORM\Column(type="string", length=254, nullable=true)
     * @Assert\Email()
     */
    protected $email;

    /**
     * @Gedmo\Timestampable(on="create")
     * @ORM\Column(type="datetime", nullable=true)
     */
    protected $created;

    /**
     * @Gedmo\Timestampable(on="update")
     * @ORM\Column(type="datetime", nullable=true)
     */
    protected $updated;
    
    public function hasCoordinates()
    {
        return 
            !empty($this->latitude) && !empty($this->longitude) && 
            $this->latitude > 0 && $this->longitude > 0;
    }
}