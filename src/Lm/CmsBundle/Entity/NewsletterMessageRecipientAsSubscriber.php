<?php

namespace Lm\CmsBundle\Entity;

use Doctrine\ORM\Mapping\OrderBy;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Wspolne pola dla NewsletterMessageRecipientAsSubscriber i NewsletterMessageRecipientAsEmail
 * nie sa zdefiniowane w jakiejs klasie bazowej i dziedziczone przez ta klase, bo 
 * po uzyciu doctrine:generate:entities i tak te pola sa kopiowane ...
 * 
 * @ORM\Table(name="newsletter_message_recipient_as_subscriber")
 * @ORM\Entity(repositoryClass="Lm\CmsBundle\Repository\NewsletterMessageRecipientAsSubscriberRepository")
 * @ORM\HasLifecycleCallbacks();
 */
class NewsletterMessageRecipientAsSubscriber
{
    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;
    
    /**
     * @ORM\Column(type="datetime", nullable=true)
     */
    protected $sentDate;

    /**
     * @ORM\Column(type="datetime")
     */
    protected $createdAt;
    
    /**
     * @ORM\Column(type="datetime", nullable=true)
     */
    protected $updatedAt;
    
    /**
     * @ORM\ManyToOne(targetEntity="NewsletterMessage", inversedBy="recipientsAsSubscribers")
     * @ORM\JoinColumn(name="newsletterMessageId", referencedColumnName="id")
     */
    private $newsletterMessage;
    
    /**
     * @ORM\ManyToOne(targetEntity="NewsletterSubscriber", inversedBy="messageRecipients")
     * @ORM\JoinColumn(name="newsletterSubscriberId", referencedColumnName="id")
     */
    private $newsletterSubscriber;

    public function __construct()
    {
        $this->createdAt = new \DateTime();
    }
    

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set sentDate
     *
     * @param \DateTime $sentDate
     * @return NewsletterMessageRecipientAsSubscriber
     */
    public function setSentDate($sentDate)
    {
        $this->sentDate = $sentDate;
    
        return $this;
    }

    /**
     * Get sentDate
     *
     * @return \DateTime 
     */
    public function getSentDate()
    {
        return $this->sentDate;
    }

    /**
     * Set createdAt
     *
     * @param \DateTime $createdAt
     * @return NewsletterMessageRecipientAsSubscriber
     */
    public function setCreatedAt($createdAt)
    {
        $this->createdAt = $createdAt;
    
        return $this;
    }

    /**
     * Get createdAt
     *
     * @return \DateTime 
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    /**
     * Set updatedAt
     *
     * @param \DateTime $updatedAt
     * @return NewsletterMessageRecipientAsSubscriber
     */
    public function setUpdatedAt($updatedAt)
    {
        $this->updatedAt = $updatedAt;
    
        return $this;
    }

    /**
     * Get updatedAt
     *
     * @return \DateTime 
     */
    public function getUpdatedAt()
    {
        return $this->updatedAt;
    }

    /**
     * Set newsletterMessage
     *
     * @param \Lm\CmsBundle\Entity\NewsletterMessage $newsletterMessage
     * @return NewsletterMessageRecipientAsSubscriber
     */
    public function setNewsletterMessage(\Lm\CmsBundle\Entity\NewsletterMessage $newsletterMessage = null)
    {
        $this->newsletterMessage = $newsletterMessage;
    
        return $this;
    }

    /**
     * Get newsletterMessage
     *
     * @return \Lm\CmsBundle\Entity\NewsletterMessage 
     */
    public function getNewsletterMessage()
    {
        return $this->newsletterMessage;
    }

    /**
     * Set newsletterSubscriber
     *
     * @param \Lm\CmsBundle\Entity\NewsletterSubscriber $newsletterSubscriber
     * @return NewsletterMessageRecipientAsSubscriber
     */
    public function setNewsletterSubscriber(\Lm\CmsBundle\Entity\NewsletterSubscriber $newsletterSubscriber = null)
    {
        $this->newsletterSubscriber = $newsletterSubscriber;
    
        return $this;
    }

    /**
     * Get newsletterSubscriber
     *
     * @return \Lm\CmsBundle\Entity\NewsletterSubscriber 
     */
    public function getNewsletterSubscriber()
    {
        return $this->newsletterSubscriber;
    }
}