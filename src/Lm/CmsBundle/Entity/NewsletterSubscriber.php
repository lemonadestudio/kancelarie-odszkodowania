<?php

namespace Lm\CmsBundle\Entity;

use Doctrine\ORM\Mapping\OrderBy;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use Lm\CmsBundle\Repository\NewsletterSubscriberRepository;

/**
 *
 * @ORM\Table(name="newsletter_subscriber")
 * @ORM\Entity(repositoryClass="Lm\CmsBundle\Repository\NewsletterSubscriberRepository")
 * @ORM\HasLifecycleCallbacks();
 * @UniqueEntity("email")
 */
class NewsletterSubscriber
{
    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;
    
    /**
     * @ORM\Column(type="string", length=255, unique=true)
     * @Assert\Email()
     * @Assert\NotBlank()
     */
    private $email;
    
    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $name;
    
    /**
     * @ORM\Column(type="string", length=40)
     */
    private $token;
    
    /**
     * @ORM\Column(type="boolean", nullable=true)
     * 
     * Określa czy konto zostało aktywowane przez użytkownika
     */
    private $isActive;

    /**
     * @ORM\Column(type="datetime")
     */
    private $createdAt;
    
    /**
     * @ORM\Column(type="datetime", nullable=true)
     */
    private $updatedAt;
    
    /**
     * @ORM\OneToMany(
     *   targetEntity="NewsletterMessageRecipientAsSubscriber",
     *   mappedBy="newsletterSubscriber",
     *   cascade={"persist", "remove"}
     * )
     */
    private $messageRecipientsAsSubscribers;

    public function __construct()
    {
        $this->createdAt = new \DateTime();
    }
    
    public function __toString()
    {
        return '' . $this->email;
    }
    
    public function generateToken($prefix)
    {
        return sha1(uniqid($prefix, true));
    }
    
    /**
     * @ORM\PrePersist()
     */
    public function onPrePersist()
    {
        $this->token = $this->generateToken($this->email);
    }
    
    /**
     * @ORM\PreUpdate()
     */
    public function onPreUpdate()
    {
        $this->updatedAt = new \DateTime();
    }
    
    


    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set email
     *
     * @param string $email
     * @return NewsletterSubscriber
     */
    public function setEmail($email)
    {
        $this->email = $email;
    
        return $this;
    }

    /**
     * Get email
     *
     * @return string 
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * Set name
     *
     * @param string $name
     * @return NewsletterSubscriber
     */
    public function setName($name)
    {
        $this->name = $name;
    
        return $this;
    }

    /**
     * Get name
     *
     * @return string 
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set token
     *
     * @param string $token
     * @return NewsletterSubscriber
     */
    public function setToken($token)
    {
        $this->token = $token;
    
        return $this;
    }

    /**
     * Get token
     *
     * @return string 
     */
    public function getToken()
    {
        return $this->token;
    }

    /**
     * Set isActive
     *
     * @param boolean $isActive
     * @return NewsletterSubscriber
     */
    public function setIsActive($isActive)
    {
        $this->isActive = $isActive;
    
        return $this;
    }

    /**
     * Get isActive
     *
     * @return boolean 
     */
    public function getIsActive()
    {
        return $this->isActive;
    }

    /**
     * Set createdAt
     *
     * @param \DateTime $createdAt
     * @return NewsletterSubscriber
     */
    public function setCreatedAt($createdAt)
    {
        $this->createdAt = $createdAt;
    
        return $this;
    }

    /**
     * Get createdAt
     *
     * @return \DateTime 
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    /**
     * Set updatedAt
     *
     * @param \DateTime $updatedAt
     * @return NewsletterSubscriber
     */
    public function setUpdatedAt($updatedAt)
    {
        $this->updatedAt = $updatedAt;
    
        return $this;
    }

    /**
     * Get updatedAt
     *
     * @return \DateTime 
     */
    public function getUpdatedAt()
    {
        return $this->updatedAt;
    }

    /**
     * Add messageRecipientsAsSubscribers
     *
     * @param \Lm\CmsBundle\Entity\NewsletterMessageRecipientAsSubscriber $messageRecipientsAsSubscribers
     * @return NewsletterSubscriber
     */
    public function addMessageRecipientsAsSubscriber(\Lm\CmsBundle\Entity\NewsletterMessageRecipientAsSubscriber $messageRecipientsAsSubscribers)
    {
        $this->messageRecipientsAsSubscribers[] = $messageRecipientsAsSubscribers;
    
        return $this;
    }

    /**
     * Remove messageRecipientsAsSubscribers
     *
     * @param \Lm\CmsBundle\Entity\NewsletterMessageRecipientAsSubscriber $messageRecipientsAsSubscribers
     */
    public function removeMessageRecipientsAsSubscriber(\Lm\CmsBundle\Entity\NewsletterMessageRecipientAsSubscriber $messageRecipientsAsSubscribers)
    {
        $this->messageRecipientsAsSubscribers->removeElement($messageRecipientsAsSubscribers);
    }

    /**
     * Get messageRecipientsAsSubscribers
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getMessageRecipientsAsSubscribers()
    {
        return $this->messageRecipientsAsSubscribers;
    }
}