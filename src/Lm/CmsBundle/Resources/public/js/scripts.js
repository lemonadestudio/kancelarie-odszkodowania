$(function() {
    $('.gallery_photo').colorbox({
        rel: '.gallery_photo',
        maxWidth: '90%',
        maxHeight: '90%'
    });
    showInfoPopup();
    hideInfoPopup();
    
    $('#PopupFormType select.select-dropdown').click(function() {
        $('#PopupFormType div:nth-child(4)').toggleClass('rotated')
    });
    
    $('.radio-choice input').on('change', function () {
        var placeholder = $('.radio-choice input:checked').val();    
        $('.email-field').attr('placeholder', placeholder);
    });
    
    var $offersMap = $('#offers_map');
    
    if ($offersMap.length) {
        $offersMap.offersMap();
    }
    
    var $offersMain = $('.offer-main');
    
    if ($offersMain.length) {
        if ($offersMain.find('.realisationsGallery .realisationItem').length) {
            $.scrollTo('.offer-main .header', 100);
        }
    }
    
    //$('.search-box').tabs();
    
    var $searchBox = $('.search-box');
    
    if ($searchBox.length) {
        $searchBox.find('.category select').select2({
            containerCssClass: 'normal-container',
            dropdownCssClass: 'normal-dropdown',
            allowClear: true,
            placeholder: 'wybierz'
        });
        
        var $radiusSelectElement = $searchBox.find('.radius select');
        $radiusSelectElement.select2({
            containerCssClass: 'medium-container',
            dropdownCssClass: 'medium-dropdown',
            allowClear: true,
            placeholder: 'wybierz'
        });
                
        $radiusSelectElement.select2('disable', true);
        
        $searchBox.find('.city input[type="text"]').on('keyup', function(){
            if ($(this).val().length) {
                $radiusSelectElement.select2('enable', true);
            } else {
                $radiusSelectElement.select2('disable', true);
                $radiusSelectElement.select2('val', null);
            }
        });
    }
    
    var $offerDetails = $('.offer-details');
    
    if ($offerDetails.length) {
        $('.formShower').on('click', function() {
            $('.formContent').animate({'height': '191px'}, 500);
            $(this).hide();
        });
        
        $offerDetails.find('.map').offersMap();
        
        $offerDetails.find('.company.left .button').on('click', function(e){
            e.preventDefault();
            var $this = $(this);
            
            if ($this.attr('data-sending') === 1)
                return;

            $.ajax({
               type: 'GET',
               url: $this.attr('href'),
               beforeSend: function() {
                   $this.attr('data-sending', '1');
                   $this.addClass('saving');
               }
            }).done(function(response) {
                showOfferRequestLinkSuccessPopup(response.message);
            }).always(function() {
                $this.attr('data-sending', '0');
                $this.removeClass('saving');
            });
        });
        
        $offerDetails.on('submit', '#offer_comment_form', function(e){
            e.preventDefault();
            $(this).hide();
            $('#offer_comment_loading').show();

            $.ajax({
               type: 'POST',
               data: $(this).serialize(),
               url:  $(this).attr('action')
            }).done(function(response) {
                $('#form_content').html(response);
            });
        });
        
        $offerDetails.on('submit', '.referral-form form', function(e){
            e.preventDefault();
            
            var $this = $(this);
            
            if ($this.attr('data-sending') === 1)
                return;

            $.ajax({
               type: 'POST',
               data: $this.serialize(),
               url:  $this.attr('action'),
               beforeSend: function() {
                   $this.attr('data-sending', '1');
                   $this.find('input[type="submit"]').val('Wysyłanie...');
               }
            }).done(function(response) {
                var $parent = $this.parent('.referral-form');
                $this.remove();
                $parent.prepend(response);
                
                if (!$('.referral-form form li').length) {
                    $('.referral-form form input[type="text"]').val('');
                    showOfferShareThanksPopup();
                }
            });
        });
        
    }
    
    $('.print').on('click', function(e){
        e.preventDefault();
        window.print();
    });
    
    var $offerAdd = $('.offer-add');
    
    if ($offerAdd.length) {
        $("#offer_add_keywords").select2({
            tags: [],
            tokenSeparators: [","],
            formatNoMatches: false,
            containerCssClass: 'full-width-container',
            dropdownCssClass: 'full-width-dropdown'
        });

        $("#offer_add_categories").select2({
            containerCssClass: 'full-width-container',
            dropdownCssClass: 'full-width-dropdown'
        });
        
        $('#offer_add_mainAddress_province').select2({
            containerCssClass: 'normal-container',
            dropdownCssClass: 'normal-dropdown',
            allowClear: true,
            placeholder: 'wybierz'
        });
        
        $('.categoriesForm').on('click', function(e){
            e.preventDefault();
            
            $('#offer_add_categories').select2('open');
        });
        
        $('.keywordForm').on('click', function(e){
            e.preventDefault();
            
            $('#offer_add_keywords').select2('open');
        });

        $('.addAnother').on('click', function() {
            $(this).hide().next('div').next('label').show().next('input').show();
        });
    }
    
    $('.newsletter-subscribe-button').on('click', function(e){
        e.preventDefault();
        openNewsletterPopup(this);
    });
    
    autoOpenNewsletterPopupOnFirstVisit();
    
    var informationPopupText = $('body').attr('data-informationPopup');
    
    if (informationPopupText && informationPopupText.length) {
        showInformationPopup(informationPopupText);
    }
});

function showWelcomeInfo() {
    var cookieName = 'showedWelcomeInfo',
        cookie = $.cookie(cookieName);

    if (!cookie) {
        $.colorbox({
            html: '<div class="welcomePopup"></div>'
        });

        $.cookie(cookieName, 1, { expires: 365 });
    }
}

function showOfferShareThanksPopup() {
    var html = '<div class="thanksPopup forOfferShare">' +
                   '<div class="text-1">Dziękujemy za zainteresowanie firmą.</div>' +
                   '<div class="text-2">Wizytówka zostanie wysłana na podany adres e-mail.</div>' +
               '</div>';
       
    $.colorbox({
        html: html
    });
}

function showOfferRequestLinkSuccessPopup(message) {
    var html = '<div class="thanksPopup offerLinkRequest">' +
                   '<div class="text-1">' + message + '</div>' +
               '</div>';
       
    $.colorbox({
        html: html
    });
}

function openNewsletterPopup(anchorElement) {
    $.colorbox({
        href: $(anchorElement).attr('href'),
        className: 'newsletter-popup-wrapper',
        closeButton: false,
        onComplete: function(){
            var submitInProgress = false;

            $('#cboxContent .newsletter-popup').on('submit', 'form', function (e) {
                e.preventDefault();

                var $form = $(this);

                if (submitInProgress) {
                    return;
                }

                $.ajax({
                    url: $form.attr('action'),
                    type: 'POST',
                    data: $form.serialize(),
                    beforeSend: function() {
                        submitInProgress = true;
                        $form.find('.preloader').show();
                        $.colorbox.resize();
                    },
                    success: function (response) {
                        $form.replaceWith(response);
                        $.colorbox.resize();
                        submitInProgress = false;
                        $form.find('.preloader').hide();
                    },
                    error: function () {
                        submitInProgress = false;
                        $form.find('.preloader').hide();
                        alert('Wystąpił błąd. Proszę spróbować później.');
                    }
                });
            });
        }
    });
}

function autoOpenNewsletterPopupOnFirstVisit() {
    var cookieName = 'openedNewsletterPopup',
        cookie = $.cookie(cookieName);

    if (!cookie) {
        $('.newsletter-subscribe-button').click();
        $.cookie(cookieName, 1, { expires: 365 });
    }
}

function showInformationPopup(text) {
    $.colorbox({
        html: '<div class="information-popup">' + text + '</div>',
        className: 'information-popup-wrapper',
        closeButton: false
    });
}

/**
 * Triggers showin popup.
 */
function showInfoPopup() {
    $('.popup-form-trigger a').on('click', function(e) {
        e.preventDefault();
        $(".popup-form-container").fadeIn();
    });
}

/**
 * ides popup on clicking.
 */
function hideInfoPopup() {
    $('.popup-form .close-btn, .popup-clickable').on('click', function(e) {
       e.preventDefault(); 
       $('.popup-form-container').fadeOut();
    });
}

$(document).ready(function() {
    var currentLocation = window.location.pathname;
    var timeout = $('#timeout-setting').data('popup-timeout');
    var katalog = 'katalog';
    var checkIfInCatalog = currentLocation.indexOf(katalog);
    
    console.log(timeout);
    console.log(showTimeout);
    console.log(checkIfInCatalog);
    console.log($.cookie('noShowWelcomeShow'));
    console.log($.cookie('noShowWelcomeList'));
    
    if (checkIfInCatalog == -1) {
        $(".popup-form-container").hide();
    }
});