﻿$(function () {
    //show and hide mask on hover
    $("aside.gallery a.photo").hover(function () {
        $(this).find('.mask').show();
    }, function () {
        $(this).find('.mask').hide();
    });

    //fold item navigation
    var counter = 1;
    var interval;

    $(".fold a.nav").on('click', function (e) {
        e.preventDefault();
        $itemToMove = $(".fold .whatWeDoItems");
        $items = $(".fold .whatWeDoItem");
        
        if ($(this).hasClass('next')) {
            if (counter < 4) {
                $itemToMove.animate({
                    left: '-=250'
                }, 500, 'easeInOutExpo');
                counter++;
            } else {
                $itemToMove.animate({
                    left: '0'
                }, 500, 'easeInOutExpo');
                counter = 1;
            }
        } else if ($(this).hasClass('prev')) {
            if (counter > 1) {
                $itemToMove.animate({
                    left: '+=250'
                }, 500, 'easeInOutExpo');
                counter--;
            } else {
                $itemToMove.animate({
                    left: parseInt($items.length - 1) * -250
                }, 500, 'easeInOutExpo');
                counter = $items.length;
            }
        }
    });

    var clickOnNext = function () {
        $('.fold a.nav.next').click();
    }

    interval = setInterval(clickOnNext, 4000);

    $(".fold .whatWeDoBox").hover(function () {
        clearInterval(interval);
    }, function () {
        interval = setInterval(clickOnNext, 4000);
    });

    //chosing language
    $(".langOptions").hover(function () {
        var option = $(this).find('.option');
        var optionHeight = option.height();
        $(this).height(option.length * optionHeight);
    }, function () {
        $(this).height('23px');
    });
	
	//gray scale
    var newImg = $('.rightSide section.photo img');
    console.log(newImg.get(0));
    newImg.attr('src', desaturate(newImg.get(0), 3));
    newImg.parent().append(newImg);
});

var desaturate = function (img, opacity) {

    var canvas = document.createElement('canvas'),
        canvasContext = canvas.getContext('2d'),
        imgW = img.width,
        imgH = img.height,
        opacity = opacity || 3,
        imgPixels,
        i, avg;

    canvas.width = imgW;
    canvas.height = imgH;
    canvasContext.drawImage(img, 0, 0);

    imgPixels = canvasContext.getImageData(0, 0, imgW, imgH);

    for (var y = 0; y < imgPixels.height; y++) {
        for (var x = 0; x < imgPixels.width; x++) {
            i = (y * 4) * imgPixels.width + x * 4,
            avg = (imgPixels.data[i] + imgPixels.data[i + 1] + imgPixels.data[i + 2]) / opacity;
            imgPixels.data[i] = avg;
            imgPixels.data[i + 1] = avg;
            imgPixels.data[i + 2] = avg;
        }
    }

    canvasContext.putImageData(imgPixels, 0, 0, 0, 0, imgPixels.width, imgPixels.height);

    return canvas.toDataURL();
}