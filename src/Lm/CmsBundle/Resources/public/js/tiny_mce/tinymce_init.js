tinyMCE.init({
        mode : "textareas",
        editor_selector : "mceEditor",
        theme : "advanced",
        plugins : "emotions,spellchecker,advhr,insertdatetime,preview", 
        width: 340,
        height: 210,
        // Theme options - button# indicated the row# only
        theme_advanced_buttons1 : "bold,italic,underline,|,bullist",    
        theme_advanced_toolbar_location : "bottom",
        theme_advanced_toolbar_align : "left",
        theme_advanced_statusbar_location : false,
        theme_advanced_resizing : true,
        content_css: "test.css"
});