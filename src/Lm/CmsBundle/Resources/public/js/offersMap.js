; (function ($, window, document, google, undefined) {
    
    // Create the defaults once
    var pluginName = 'offersMap',
        defaults = {
            scrollOnMarkerClick: false,
            radius: 0,
            offers: [],
            city: ''
        },
        publicMethods = {
            init: function (options) {
                return this.each(function () {
                    if (!$.data(this, pluginName)) {
                        $.data(this, pluginName, new Plugin(this, options));
                    }
                });
            }
        };

    // The actual plugin constructor
    function Plugin(element, options) {
        this._defaults = defaults;
        this._name = pluginName;
        
        this.element = element;
        this.options = defaults;
        var $element = $(element);
        
        var dataOffers = JSON.parse($element.attr('data-offers')),
            dataScrollOnMarkerClick = $element.attr('data-scrollOnMarkerClick'),
            radius = $element.attr('data-radius'),
            city = $element.attr('data-city');
        
        if (dataOffers) {
            this.options.offers = dataOffers;
        }
        
        if (dataScrollOnMarkerClick) {
            this.options.scrollOnMarkerClick = dataScrollOnMarkerClick;
        }
        
        if (radius) {
            this.options.radius = parseInt(radius, 10);
        }
        
        if (city) {
            this.options.city = city;
        }
        
        this.options = $.extend({}, this.options, options);
        
        this.offers = this.options.offers || [];
        this.offersLength = this.offers.length;
        this.geocoder = new google.maps.Geocoder();
        this.addressToCentre = this.options.city || this.getCityWithMostOffers() || 'Warszawa';
        this.markers = [];
        this.map = new google.maps.Map(element, {
            zoom: 11,
            mapTypeId: google.maps.MapTypeId.ROADMAP
        });
        
        this.init();
    }

    Plugin.prototype.init = function () {
        var self = this;
        
        self.geocoder.geocode({ 
            address: self.addressToCentre
        }, function(results, status) {
            if (status !== google.maps.GeocoderStatus.OK)
                return;

            var locationData = results[0].geometry.location,
                directionsDisplay = new google.maps.DirectionsRenderer();
                
            self.map.setCenter(locationData);
            directionsDisplay.setMap(self.map);
            self.showOffersOnMap();
            
            if (self.options.radius > 0) {
                self.showRadius(self.options.radius, locationData);
            }
        });
    };

    Plugin.prototype.getCityWithMostOffers = function () {
        var citiesOffersAmount = [],
            cityWithMostOffers = '';
        
        for (var i = 0; i < this.offersLength; i++) {
            var offer = this.offers[i];
            
            if (!offer.addresses || !offer.addresses.length) {
                continue;
            }
            
            var addressesCount = offer.addresses.length;
            for (var j = 0; j < addressesCount; j++) {
                var address = offer.addresses[j];
                
                if (!address.city) {
                    continue;
                }
                
                if (!citiesOffersAmount[address.city]) {
                    citiesOffersAmount[address.city] = 1;
                } else {
                    citiesOffersAmount[address.city]++;
                }
                
                if (!cityWithMostOffers) {
                    cityWithMostOffers = address.city;
                } else if (citiesOffersAmount[address.city] > citiesOffersAmount[cityWithMostOffers]) {
                    cityWithMostOffers = address.city;
                }
            }
        }
        
        return cityWithMostOffers;
    };
    
    Plugin.prototype.showOffersOnMap = function() {
        var self = this,
            offersCount = self.offersLength;
        
        for (var i = 0; i < offersCount; i++) {
            var offer = self.offers[i];
            
            if (!offer.addresses || !offer.addresses.length) {
                continue;
            }
            
            var addressesCount = offer.addresses.length;
            for (var j = 0; j < addressesCount; j++) {
                var address = offer.addresses[j];
                
                if (address.coords) {
                    var location = new google.maps.LatLng(address.coords.latitude, address.coords.longitude);
                    self.showOfferOnMap(offer, location, i + 1, offersCount);
                } else {
                    self.showOfferOnMapWithoutCords(offer, address, i + 1, offersCount);
                }
            }
        }
    };
    
    Plugin.prototype.showOfferOnMap = function(offer, position, orderNum, offersCount) {
        var self = this;
        
        self.markers[offer.id] = new google.maps.Marker({
            position: position,
            map: self.map,
            title: offer.title,
            icon: 'http://chart.apis.google.com/chart?chst=d_map_pin_letter&chld=' + orderNum + '|FE6256|000000',
            zIndex: offersCount - orderNum
        });
        
        if (self.options.scrollOnMarkerClick) {
            google.maps.event.addListener(self.markers[offer.id], 'click', function() {
                var scrollElementId = '#offer-' + offer.id;

                $.scrollTo(scrollElementId, 500, {
                    onAfter: function(elem) {
                        var $overlay = $(elem).find('.selectedFromMapOverlay');
                        $overlay.addClass('visible');

                        setTimeout(function(){
                            $overlay.addClass('start');
                        }, 500);

                        setTimeout(function(){
                            $overlay.removeClass('visible')
                                    .removeClass('start');
                        }, 2000);
                    }
                });
            });
        }
    };
    
    Plugin.prototype.showOfferOnMapWithoutCords = function(offer, address, orderNum, offersCount) {
        var self = this;
        
        self.geocoder.geocode({
            address: address.city + ', ' + address.postCode + ', ' + address.street
        }, function(results, status) {
            if (status !== google.maps.GeocoderStatus.OK)
                return;

            self.showOfferOnMap(offer, results[0].geometry.location, orderNum, offersCount);
        });
    };
    
    Plugin.prototype.showRadius = function(radius, centerPosition) {
        var self = this;
        
        new google.maps.Circle({
            strokeColor: '#a3b19b',
            strokeOpacity: 0.7,
            strokeWeight: 2,
            fillColor: '#a5d6bf',
            fillOpacity: 0.35,
            map: self.map,
            center: centerPosition,
            radius: radius * 1000
        });
    };

    // A really lightweight plugin wrapper around the constructor, 
    // preventing against multiple instantiations
    $.fn[pluginName] = function (options) {
        if (publicMethods[options])
            return publicMethods[options].apply(this, Array.prototype.slice.call(arguments, 1));
        else if (typeof options === 'object' || !options)
            return publicMethods.init.apply(this, arguments);
        else
            $.error('Method ' +  options + ' does not exist on jQuery.' + pluginName);
    }

})(jQuery, window, document, google);
