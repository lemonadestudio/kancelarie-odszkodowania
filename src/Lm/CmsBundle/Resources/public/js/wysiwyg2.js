$(function () {
    fckconfig_common = {
        skin: 'BootstrapCK-Skin',
        contentsCss: [edytor_css2, edytor_css],
        bodyClass: 'editor',
        //EditorAreaCSS: edytor_css,
        stylesSet: [
            {name: 'Normalny', element: 'p', attributes: {'class': ''}},
            {name: 'Zwykły', element: 'span', attributes: {'class': ''}},
            {name: 'Wyróżnienie', element: 'p', attributes: {'class': 'gold'}}
        ],
        filebrowserBrowseUrl: edytor_browse_url
    };

    fckconfig = jQuery.extend(true, {}, fckconfig_common);

    fckconfig_basic = jQuery.extend(true, {
        toolbar:
        [
            ['Bold', 'Italic', '-', 'NumberedList', 'BulletedList', '-', 'Link', 'Unlink'],
            ['UIColor']
        ]
    }, fckconfig_common);

    fckconfig_tiny = jQuery.extend(true, {
        toolbar:
        [
            ['Source', '-', 'Bold', 'Italic', 'Strike', '-', 'TextColor'], ['FontSize']
        ]
    }, fckconfig_common);

    fckconfig_basic_with_img = jQuery.extend(true, {
        toolbar:
        [
            ['Source', '-', 'Bold', 'Italic', 'Strike', '-', 'NumberedList', 'BulletedList', '-', 'Link', 'Unlink', 'UIColor', 'Image', '-', 'JustifyLeft', 'JustifyCenter', 'JustifyRight', 'JustifyBlock']
        ]
    }, fckconfig_common);

    $('.wysiwyg').ckeditor(fckconfig);
    $('.wysiwyg-basic').ckeditor(fckconfig_basic);
    $('.wysiwyg-tiny').ckeditor(fckconfig_tiny);
    $('.wysiwyg-basic-with-img').ckeditor(fckconfig_basic_with_img);
    
    CKEDITOR.config.forcePasteAsPlainText = true;
});
