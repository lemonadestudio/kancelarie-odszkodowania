<?php

namespace Lm\CmsBundle\Form\Type;

use Doctrine\ORM\EntityRepository;
use Lm\CmsBundle\Repository\OfferRepository;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilder;
use Symfony\Component\Validator\Constraints as Constraints;

class PopupFormType extends AbstractType 
{

    /**
     * @param FormBuilder $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options) 
    {
        $builder
            ->add('name', 'text', array(
                'attr' => array(
                    'placeholder' => 'Imię i nazwisko'
                ),
                'label' => false,
            ))
            ->add('contact', 'text', array(
                'attr' => array(
                    'placeholder' => 'Wpisz swój adres e-mail lub numer telefonu',
                ),
                'label' => false
            ))
            ->add('category', 'entity', array(
                'class' => 'LmCmsBundle:OfferCategory',
                'mapped' => false,
                'property' => 'name',
                'required' => false,
                'query_builder' => function(EntityRepository $er) {
                    return $er->createQueryBuilder('u')
                        ->orderBy('u.id', 'ASC');
                },
                'empty_value' => 'Rodzaj szkody',
                'label' => false,
                'attr' => array(
                    'class' => 'select-dropdown',
                ),
            ))
            ->add('message', 'textarea', array(
                'attr' => array(
                    'placeholder' => 'Opis szkody'
                ),
                'label' => false
            ));
                    
    }

    public function getName() 
    {
        return 'PopupFormType';
    }

}
