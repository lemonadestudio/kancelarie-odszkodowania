<?php

namespace Lm\CmsBundle\Form\Type;

use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\DependencyInjection\Container;
use Symfony\Component\Validator\Constraints\Length;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilder;


class OfferCommentType extends AbstractType {


	public function __construct() {
		
	}

	/**
	 * @param FormBuilder $builder
	 * @param array $options
	 */
	public function buildForm(FormBuilderInterface $builder, array $options) {
		$builder
			->add('comment', 'textarea', array('required' => true, 'label' => 'Treść opinii'))
            ->add('buy_date', 'text', array('required' => false, 'label' => 'Rok skorzystania z usługi'))
            ->add('buy_object', 'text', array('required' => false, 'label' => 'Rodzaj szkody'))
            ->add('positive', 'choice', array('expanded' => true, 'data' => 1, 'required' => true, 'choices' => array(0 => 'Nie', 1 => 'Tak' )))
            ->add('action', 'hidden', array( 'data' => 'offer_comment'))
		;
	}

	/**
	 * @param array $options
	 * @return multitype:
	 */
	public function getDefaultOptions(array $options) {

		return array(
		      'data_class' => 'Lm\CmsBundle\Entity\OfferComment'
		);
	}

	public function getName() {

		return 'offer_comment';

	}
}
