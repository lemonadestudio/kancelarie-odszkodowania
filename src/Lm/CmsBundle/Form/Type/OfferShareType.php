<?php

namespace Lm\CmsBundle\Form\Type;

use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\DependencyInjection\Container;
use Symfony\Component\Validator\Constraints\Email;
use Symfony\Component\Validator\Constraints\Collection;
use Symfony\Component\Validator\Constraints\NotBlank;
use Symfony\Component\Validator\Constraints\MaxLength;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilder;

class OfferShareType extends AbstractType {

    /**
     *
     * @var ContainerAware
     */
    private $container;

    public function __construct(Container $container) {
        $this->container = $container;
    }

    /**
     * @param FormBuilder $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options) {
        $builder
                ->add('email', 'text', array('required' => true))
        ;
    }

    /**
     * @param array $options
     * @return multitype:
     */
    public function getDefaultOptions(array $options) {
        $collectionConstraint = new Collection(array(
            'email' => array(
                new NotBlank(array('message' => 'Email: Uzupełnij to pole')), 
                new Email(array('message' => 'Nieprawidłowy adres e-mail'))),
        ));

        return array('validation_constraint' => $collectionConstraint);
    }

    public function getName() {
        return '';
    }

}
