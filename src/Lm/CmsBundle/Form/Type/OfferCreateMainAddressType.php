<?php

namespace Lm\CmsBundle\Form\Type;

use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\DependencyInjection\Container;
use Symfony\Component\Validator\Constraints\Email;
use Symfony\Component\Validator\Constraints\NotBlank;
use Symfony\Component\Validator\Constraints\MaxLength;
use Symfony\Component\Validator\Constraints\Count;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilder;
use Symfony\Component\Validator\Constraints\Collection;
use Lm\CmsBundle\Util\Province;

class OfferCreateMainAddressType extends AbstractType 
{
    /**
     *
     * @var ContainerAware
     */
    private $container;
    private $provinces;

    public function __construct(Container $container) {
        $this->container = $container;
        $this->provinces = Province::getProvinces();
    }

    /**
     * @param FormBuilder $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options) {
        $builder
                ->add('province', 'choice', array(
                    'label' => 'Województwo',
                    'choices' => $this->provinces,
                    'required' => true,
                    'empty_value' => '',
                    'constraints' => array(
                        new NotBlank(array('message' => 'Województwo: Uzupełnij to pole'))
                    ),
                ))
                ->add('city', 'text', array(
                    'label' => 'Miejscowość',
                    'required' => true,
                    'constraints' => array(
                        new NotBlank(array('message' => 'Miejscowość: Uzupełnij to pole')),
                        new MaxLength(255)
                    ),
                ))
                ->add('postCode', 'text', array(
                    'label' => 'Kod pocztowy',
                    'required' => true,
                    'constraints' => array(
                        new NotBlank(array('message' => 'Kod pocztowy: Uzupełnij to pole')),
                        new MaxLength(20)
                    ),
                ))
                ->add('street', 'text', array(
                    'label' => 'Ulica',
                    'required' => true,
                    'constraints' => array(
                        new NotBlank(array('message' => 'Ulica: Uzupełnij to pole')),
                        new MaxLength(255)
                    ),
                ))
                ->add('email', 'text', array(
                    'label' => 'Adres email',
                    'required' => true,
                    'constraints' => array(
                        new NotBlank(array('message' => 'Email: Uzupełnij to pole')),
                        new MaxLength(254),
                        new Email()
                    )
                ))
                ->add('email2', 'text', array(
                    'label' => '',
                    'required' => false,
                    'constraints' => array(
                        new MaxLength(254),
                        new Email()
                    )
                ))
                ->add('phone', 'text', array(
                    'label' => 'Numer telefonu',
                    'required' => false,
                    'constraints' => array(
                        new MaxLength(20)
                    )
                ))
                ->add('phone2', 'text', array(
                    'label' => '',
                    'required' => false,
                    'constraints' => array(
                        new MaxLength(20)
                    )
                ))
        ;
    }

    /**
     * @param array $options
     * @return multitype:
     */
    public function getDefaultOptions(array $options) {
        return array(
            'data_class' => 'Lm\CmsBundle\Entity\OfferMainAddress'
        );
    }

    public function getName() {
        return 'offer_add_main_address';
    }
}
