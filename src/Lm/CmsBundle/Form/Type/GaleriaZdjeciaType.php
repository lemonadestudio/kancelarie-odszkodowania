<?php


namespace Lm\CmsBundle\Form\Type;

use Symfony\Component\Form\FormBuilderInterface;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilder;

class GaleriaZdjeciaType extends AbstractType
{
	public function buildForm(FormBuilderInterface $builder, array $options)
	{


		$builder->add('zdjecia', 'collection', array(
				'type' => new GaleriaZdjecieType(),
				'allow_add' => true,
				'by_reference' => false,
				))
		;

	}

	public function getDefaultOptions(array $options)
	{
		return array(
			'data_class' => 'Lm\CmsBundle\Entity\Galeria',
		);
	}

	public function getName()
	{
		return 'galeria_zdjecia';
	}
}