<?php

namespace Lm\CmsBundle\Form\Type;

use Symfony\Component\Form\FormBuilderInterface;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilder;

class GaleriaZdjecieType extends AbstractType {


	/**
	 * @param FormBuilder $builder
	 * @param array $options
	 */
	public function buildForm(FormBuilderInterface $builder, array $options) {



		$builder
				->add('_file', 'file', array('required' => false))
				->add('podpis')
				->add('kolejnosc')
				->add('_delete', 'checkbox', array('required' => false, 'property_path' => false))
				->add('updated_at', null, array( 'with_seconds' => 'true',  'attr'=>array('style'=>'display:none;')));
		;

	}

	/**
	 * @param array $options
	 * @return multitype:
	 */
	public function getDefaultOptions(array $options) {

		return array(
				'data_class' => 'Lm\CmsBundle\Entity\GaleriaZdjecie'
		);

	}

	public function getName() {
		return 'galeria_zdjecie';
	}



}