<?php

namespace Lm\CmsBundle\Form\Type;

use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;
use Symfony\Component\DependencyInjection\Container;
use Symfony\Component\Validator\Constraints\Email;
use Symfony\Component\Validator\Constraints\NotBlank;
use Symfony\Component\Validator\Constraints\MaxLength;
use Symfony\Component\Validator\Constraints\Count;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilder;
use Symfony\Component\Validator\Constraints\Collection;
use Lm\CmsBundle\Util\Province;

class OfferCreateType extends AbstractType {

    /**
     *
     * @var ContainerAware
     */
    private $container;
    private $addDeletePhotoField;

    public function __construct(Container $container, $addDeletePhotoField = false) {
        $this->container = $container;
        $this->addDeletePhotoField = $addDeletePhotoField;
    }

    /**
     * @param FormBuilder $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options) {
        $builder
                ->add('content2', 'textarea', array(
                    'required' => false
                ))
                ->add('content', 'textarea', array(
                    'required' => false
                ))
                ->add('title', 'text', array(
                    'label' => 'Nazwa firmy'
                ))
                ->add('home', 'text', array(
                    //'mapped' => false,
                    'label' => 'Numer domu',
                    'constraints' => array(
                        new NotBlank(array('message' => 'Nr domu: Uzupełnij to pole')),
                        new MaxLength(10)
                    ),
                ))
                ->add('local', 'text', array(
                    //'mapped' => false,
                    'label' => 'Nr lokalu',
                    'required' => false,
                    'constraints' => array(
                        new MaxLength(10)
                    ),
                ))
                ->add('www', 'text', array(
                    'label' => 'Adres www',
                    'required' => false,
                ))
                ->add('facebookUrl', 'text', array(
                    'label' => 'Facebook',
                    'required' => false,
                ))
                ->add('categories', 'entity', array(
                    'required' => true,
                    'multiple' => true,
                    'expanded' => false,
                    'property' => 'name',
                    'group_by' => 'typeName',
                    'class'    => 'Lm\CmsBundle\Entity\OfferCategory',
                    'constraints' => array(
                        new Count(array(
                            'min' => 1,
                            'minMessage' => 'Wybranie branż jest wymagane.'
                        ))
                    )
                ))
                ->add('keywords', 'text', array(
                    'required' => false,
                ))
                ->add('rules', 'checkbox', array(
                    'mapped' => false,
                    'constraints' => array(
                        new NotBlank(array('message' => 'Wymagana akceptacja'))
                    ),
                ))
                ->add('_file_image', 'file', array('required' => false))
                ->add('mainAddress', new OfferCreateMainAddressType($this->container))
        ;
        
        if ($this->addDeletePhotoField) {
            $builder->add('_delete_file_image', 'checkbox', array('required'=> false, 'label' => 'Usuń logo'));
        }
    }

    /**
     * @param array $options
     * @return multitype:
     */
    public function getDefaultOptions(array $options) {
        return array(
            'data_class' => 'Lm\CmsBundle\Entity\Offer'
        );
    }

    public function getName() {
        return 'offer_add';
    }
}
