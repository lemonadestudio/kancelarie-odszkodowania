<?php

namespace Lm\CmsBundle\Form\Type;

use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilder;
use Symfony\Component\Validator\Constraints as Constraints;

class NewsletterSubscriberType extends AbstractType 
{

    /**
     * @param FormBuilder $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options) 
    {
        $builder
                ->add('email', 'text', array(
                    'required' => true,
                    'constraints' => array(new Constraints\Email(), new Constraints\NotBlank())
        ));
    }

    public function getName() 
    {
        return 'NewsletterSubscriber';
    }

}
