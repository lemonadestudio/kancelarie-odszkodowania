<?php

namespace Lm\CmsBundle\Form\Type;

use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\DependencyInjection\Container;
use Symfony\Component\Validator\Constraints\Email;
use Symfony\Component\Validator\Constraints\Collection;
use Symfony\Component\Validator\Constraints\NotBlank;
use Symfony\Component\Validator\Constraints\MaxLength;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilder;

class ContactType extends AbstractType {

    /**
     *
     * @var ContainerAware
     */
    private $container;

    public function __construct(Container $container) {
        $this->container = $container;
    }

    /**
     * @param FormBuilder $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options) {
        $builder
                ->add('title', 'text', array('label' => 'Tytuł zapytania'))
                ->add('email', 'text', array('required' => false, 'label' => 'Twój adres e-mail'))
                ->add('content', 'textarea', array('label' => 'Treśc zapytania'))
                ->add('captcha', 'captcha', array('label' => 'Przepisz kod z obrazka'))
        ;
    }

    /**
     * @param array $options
     * @return multitype:
     */
    public function getDefaultOptions(array $options) {
        $collectionConstraint = new Collection(array(
            'title' => array(new NotBlank(array('message' => 'Tytuł zapytania: Uzupełnij to pole')), new MaxLength(100)),
            'email' => array(new NotBlank(array('message' => 'Twój adres e-mail: Uzupełnij to pole')), new Email(array('message' => 'Twój adres e-mail: Nieprawidłowy adres e-mail'))),
            'content' => array(new NotBlank(array('message' => 'Treść: Uzupełnij to pole')), new MaxLength(100))
        ));

        return array('validation_constraint' => $collectionConstraint);
    }

    public function getName() {
        return 'contact';
    }

}
