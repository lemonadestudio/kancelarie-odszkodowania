<?php

namespace Lm\CmsBundle\Form\Type;

use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\DependencyInjection\Container;
use Symfony\Component\Validator\Constraints\Email;
use Symfony\Component\Validator\Constraints\Collection;
use Symfony\Component\Validator\Constraints\NotBlank;
use Symfony\Component\Validator\Constraints\MaxLength;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilder;

class OfferSearchType extends AbstractType {

    /**
     *
     * @var ContainerAware
     */
    private $container;
    private $categories;
    private $radiusList;

    public function __construct(Container $container, $categories) {
        $this->container = $container;
        $this->categories = $categories;
        $this->radiusList = array(5, 10, 15, 30, 50);
    }

    /**
     * @param FormBuilder $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options) {
        $builder
                ->add('city', 'text', array('label' => 'Miasto'))
                ->add('keyword', 'text', array('label' => 'Usługi, nazwy, słowa kluczowe'))
                ->add('category', 'choice', array(
                    'label' => 'Rodzaj szkody',
                    'choices' => $this->getCategoryChoices(),
                    'empty_value' => '',
                ))
                ->add('radius', 'choice', array(
                    'label' => 'Promień wyszukiwania',
                    'choices' => $this->getRadiusChoices(),
                    'empty_value' => ''
                ))
                ->add('onlyPromoted', 'checkbox', array(
                    'label' => 'Tylko oferty promowane',
                    'required' => false,
                ))
        ;
    }

    /**
     * @param array $options
     * @return multitype:
     */
    public function getDefaultOptions(array $options) {
        $collectionConstraint = new Collection(array(
            'city' => array(),
            'keyword' => array(),
            'category' => array(),
            'radius' => array(),
            'onlyPromoted' => array()
        ));

        return array(
            'validation_constraint' => $collectionConstraint,
            'csrf_protection' => false
        );
    }

    public function getCategoryChoices() {
        $choices = array();

        foreach ($this->categories as $category) {
            $choices[$category['id']] = $category['name'];
        }

        return $choices;
    }

    public function getCategoryValidatorChoices() {
        $validator = array();

        foreach ($this->categories as $category) {
            $validator[] = $category['id'];
        }

        return $validator;
    }

    public function getRadiusChoices() {
        $choices = array();

        foreach ($this->radiusList as $radius) {
            $choices[$radius] = $radius;
        }

        return $choices;
    }

    public function getName() {
        return '';
    }

}
