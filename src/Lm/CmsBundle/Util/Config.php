<?php

namespace Lm\CmsBundle\Util;

use Lm\CmsBundle\Entity\Setting;
use Doctrine\ORM\EntityManager;
use Doctrine\ORM\EntityRepository;


class Config {

	/**
	 * @var EntityManager
	 */
	protected $em;

	/**
	 * @var EntityRepository
	 */
	protected $repo;

	protected $settings = array();

	protected $is_loaded = false;

	protected $default_settings = array(


	        'seo_title' => array(
	                'value' => 'Tytuł',
	                'description' => 'Domyślny tytuł strony - strona główna - wartość meta tagu title'),

	        'seo_description' => array(
	                'value' => 'Opis',
	                'description' => 'Opis strony - strona główna - wartość meta tagu "description"'),

	        'seo_keywords' => array(
	                'value' => 'Słowa, kluczowe',
	                'description' => 'Domyślne słowa kluczowe - strona główna - wartość meta tagu "keywords"'),


	        'ust_mailer_wiadomosc_od' => array(
	                'value' => 'Mailer Katalog',
	                'description' => 'Nazwa pojawiająca się w polu "OD" w mailach wysyłanych przez system'
	         ),

	        'ust_nowy_wpis_powiadomienie_email' => array(
	                'value' => '',
	                'description' => 'Oddzielone przecinkami Adresy e-mail na które ma przyjść powiadomienie o nowym wpisie do katalogu'
	        ),

	        'ust_nowy_wpis_aktywacja_kopia_email' => array(
	                'value' => '',
	                'description' => 'Oddzielone przecinkami Adresy e-mail na które ma przyjść ukryta kopia maila z aktywacją wpisu wysłaną do klienta',
	        ),

	        /*
	        'ust_nowy_wpis_podziekowanie_html' => array(
	                'value' => '
	                <div style="text-align: center">
	                    <p style="color:red">
	                         Aby wpis został dodany do katalogu prosimy o sprawdzenie skrzynki e-mail, którą podali Państwo w formularzu i kliknięcie w znajdujący się w otrzymanym od nas mailu link.
	                    </p>
	                    <p>
	                        W razie problemów i nie otrzymania maila zapraszamy do <a href="/kontakt">kontaktu.</a>
	                    </p>
	                </div>',
	                'description' => 'Tekst powiadomienia po dokonaniu wpisu'
	        ),
            */
	        'ust_nowy_wpis_info_1' => array(
	                 'value' =>      'Wpis został aktywowany',
	                'description' => 'Tekst informacji po dokonaniu aktywacji wpisu'
	        ),

	        'ust_nowy_wpis_info_2' => array(
	                'value' =>      'Wpis był wcześniej aktywowany',
	                'description' => 'Tekst informacji po dokonaniu aktywacji wpisu, który był wcześniej już aktywowany'
	        ),

	        'ust_nowy_wpis_info_3' => array(
	                'value' =>      'Nieprawidłowy kod aktywacji, spróbuj ponownie lub skontaktuj się z nami',
	                'description' => 'Tekst informacji po próbie dokonania aktywacji wpisu - lecz kod aktywacji w linku jest nieprawidłowy'
	        ),





	        'formularz_wysylka_temat' => array(
	                'value' => 'Nowa wiadomość z formularza kontaktowego',
	                'description' => 'Domyślny temat wiadomości przesyłanej przez formularz kontaktowy'
	        ),

	        'formularz_wysylka_od' => array(
	                'value' => 'Formularz kontaktowy',
	                'description' => 'Pole nadawca dla wiadomości przesyłanej przez formularz kontaktowy'
	        ),

	        'txt_formularz_podziekowanie' => array(
	                'value'=> 'Wiadomość została wysłana. Dziękujemy za skorzystanie z formularza.',
	                'description' => 'Podziękowanie za skorzystanie z formularza kontaktowego'
	        ),

	        'ust_wpis_edycja_link_kopia_email' => array(
	                'value' => '',
	                'description' => 'Oddzielone przecinkami Adresy e-mail na które ma przyjść ukryta kopia maila wysłanego do klienta po użyciu opcji modyfikacji danych wizytówki',
	        ),

	        'ust_wpis_edycja_info_0' => array(
	                'value' => 'Użycie tej funkcji spowoduje wysłanie odnośnika do edycji danych na adres e-mail firmy.',
	                'description' =>      'Komunikat informujący o funkcji modyfikacji danych',
	        ),

	        'ust_wpis_edycja_info_1' => array(
	                'value' =>      'Link do edycji został przesłany na adres firmy. Sprawdź E-mail.',
	                'description' => 'Tekst informacji po próbie dokonania edycji wpisu - o wysłaniu maila z linkiem do edycji.'
	        ),

	        'ust_wpis_edycja_info_2' => array(
	                'value' =>      'Link do edycji danych został już wygenerowany. Poczekaj do 15 minut do kolejnej próby.',
	                'description' => 'Tekst informacji po próbie dokonania edycji wpisu - jeżeli czas pomiędzy żądaniami zmiany był zbyt krótki'
	        ),
	        'ust_wpis_edycja_info_3' => array(
	                'value' =>      'Link do edycji jest nieprawidłowy. Spróbuj ponownie lub skontaktuj się z nami.',
	                'description' => 'Tekst informacji po próbie dokonania edycji wpisu - jeżeli link edycji jest nieprawidłowy'
	        ),

	        'ust_wpis_edycja_info_4' => array(
	                'value' =>      'Pomyślnie zaktualizowano wpis.',
	                'description' => 'Tekst informacji po pomyślnym wyedytowaniu wpisu.'
	        ),



	        'ust_wpis_usun_info_0' => array(
	                'value' => 'Użycie tej funkcji spowoduje wysłanie potwierdzenia usunięcia danych na adres e-mail firmy.',
	                'description' =>      'Komunikat informujący o funkcji usuwania danych',
	        ),

	        'ust_wpis_usun_info_1' => array(
	                'value' =>      'Link do usunięcia danych został przesłany na adres firmy. Sprawdź E-mail.',
	                'description' => 'Tekst informacji po próbie usunięcia wpisu - o wysłaniu maila z linkiem do usunięcia.'
	        ),

	        'ust_wpis_usun_info_2' => array(
	                'value' =>      'Link do usunięcia danych został już wygenerowany. Poczekaj do 15 minut do kolejnej próby.',
	                'description' => 'Tekst informacji po próbie usunięcia wpisu - jeżeli czas pomiędzy żądaniami zmiany był zbyt krótki'
	        ),
	        'ust_wpis_usun_info_3' => array(
	                'value' =>      'Link do usunięcia danych jest nieprawidłowy. Spróbuj ponownie lub skontaktuj się z nami.',
	                'description' => 'Tekst informacji po próbie usunięcia wpisu - jeżeli link usuwania jest nieprawidłowy'
	        ),

	        'ust_wpis_usun_info_4' => array(
	                'value' =>      'Pomyślnie usunięto dane wpisu.',
	                'description' => 'Tekst informacji po pomyślnym usunięciu (ukryciu) wpisu.'
	        ),

	        'ust_wpis_usun_link_kopia_email' => array(
	                'value' => '',
	                'description' => 'Oddzielone przecinkami Adresy e-mail na które ma przyjść ukryta kopia maila wysłanego do klienta po użyciu opcji usunięcia danych wizytówki',
	        ),

	        'ust_wpis_usun_powiadomienie_email' => array(
	                'value' => '',
	                'description' => 'Oddzielone przecinkami Adresy e-mail na które ma przyjść powiadomienie o usunięciu (ukryciu) danych przez klienta'
	        ),
        
            'ust_komentarz_informacja_o_moderacji' => array(
                    'value' => 'Dziękujemy za opinię. Po akceptacji przez administrację będzie wyświetlana.',
                    'description' => 'Informacja po dodaniu komenatarza do oferty.',
            ),
            'ust_komentarz_informacja_o_dodanym_wpisie' => array(
                    'value' => 'Już skomentowałeś ten wpis, jeżeli nie widać Twojej opini, może oznaczać że jeszcze nie został zatwierdzony przez administrację.',
                    'description' => 'Informacja dla użytkownika o tym, że już skomentował ten wpis',
            ),
        
            'ust_komentarz_powiadomienie_email' => array(
	                'value' => '',
	                'description' => 'Oddzielone przecinkami Adresy e-mail na które ma przyjść powiadomienie o nowym komentarzu'
	        ),



    );



	public function __construct(EntityManager $em) {

		$this->em = $em;

	}


	/**
	 * @param string $name Name of the setting.
	 * @return string|null Value of the setting.
	 * @throws \RuntimeException If setting is not defined.
	 */
	public function get($name, $default = '') {

	    $this->all();

		if(array_key_exists($name, $this->settings)) {
			return $this->settings[$name];
		}

		$sett = new Setting();
		$sett->setName($name);
		$sett->setValue($default);
		$this->em->persist($sett);
		$this->em->flush();

		$this->settings[$name] = $default;

		return $default;

	}

	/**
	 * @return string[] with key => value
	 */
	public function all() {

		$settings = array();

		if($this->is_loaded) {
			return $this->settings;
		}

		foreach ($this->getRepo()->findAll() as $setting) {
			$settings[$setting->getName()] = $setting->getValue();
		}


		foreach($this->default_settings as $_name => $_def_setting) {
            if(!array_key_exists($_name, $settings)) {
                $sett = new Setting();


                $sett->setName($_name);
                $sett->setValue($_def_setting['value']);
                $sett->setDescription($_def_setting['description']);
                $this->em->persist($sett);
                $this->em->flush();
                $settings[$_name] = $_def_setting['value'];
            }
		}

		$this->settings = $settings;
		$this->is_loaded = true;;

		return $settings;
	}

	/**
	 * @return EntityRepository
	 */
	protected function getRepo() {
		if ($this->repo === null) {
			$this->repo = $this->em->getRepository(get_class(new Setting()));
		}

		return $this->repo;
	}

}
