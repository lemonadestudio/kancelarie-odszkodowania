<?php

namespace Lm\CmsBundle\Util;

class GeolocationFinder
{
    private $apiKey;
    
    public function __construct($apikey)
    {
        $this->apiKey = $apikey;
    }
    
    public function find($address) 
    {
        $url = $this->getApiUrl($address);
        $rawCoordinatesFromGoogle = $this->queryGooglePlacesApi($url);
        
        return new GeolocationResult($rawCoordinatesFromGoogle);
    }
      
    private function queryGooglePlacesApi($url) 
    {
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
        curl_setopt($ch, CURLOPT_HEADER, 0);
        curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 0);
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_TIMEOUT, 5);
        $result = curl_exec($ch);
        curl_close($ch);
        
        return $result;
    }
    
    private function getApiUrl($address)
    {
        $address = str_replace(' ', '+', $address);
        
        if (empty($this->apiKey)) {
            return 'http://maps.googleapis.com/maps/api/geocode/json?sensor=false&address=' . $address;
        }
        
        return 'https://maps.googleapis.com/maps/api/place/textsearch/json?sensor=false&query=' . $address . '&key=' . $this->apiKey;
    }
}

class GeolocationResult
{
    private $latitude = null;
    private $longitude = null;
    private $status;
    
    public function __construct($rawCoordinatesFromGoogle)
    {
        if (!$rawCoordinatesFromGoogle) {
            $this->status = GeolocationResultStatus::ERROR;
            return;
        }
        
        $coordinatesFromGoogle = json_decode($rawCoordinatesFromGoogle);
        $googleApiStatus = strtolower($coordinatesFromGoogle->status);
        
        if ($googleApiStatus === 'ok') {
            $this->status = GeolocationResultStatus::FOUND;
            $this->latitude = (float)$coordinatesFromGoogle->results[0]->geometry->location->lat;
            $this->longitude = (float)$coordinatesFromGoogle->results[0]->geometry->location->lng;
        } else if ($googleApiStatus === 'zero_results') {
            $this->status = GeolocationResultStatus::NOT_FOUND;
        } else {
            $this->status = GeolocationResultStatus::ERROR;
        }
    }
    
    public function getLatitude()
    {
        return $this->latitude;
    }
    
    public function getLongitude()
    {
        return $this->longitude;
    }
    
    public function hasStatus($status)
    {
        return $this->status === $status;
    }
}

class GeolocationResultStatus
{
    const FOUND = 1;
    const NOT_FOUND = 2;
    const ERROR = 3;
}