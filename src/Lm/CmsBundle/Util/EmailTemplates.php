<?php

namespace Lm\CmsBundle\Util;

use Lm\CmsBundle\Entity\EmailTemplate;
use Doctrine\ORM\EntityManager;
use Doctrine\ORM\EntityRepository;

class EmailTemplates {

    /**
     * @var EntityManager
     */
    protected $em;

    /**
     * @var EntityRepository
     */
    protected $repo;
    protected $email_templates = array();
    protected $is_loaded = false;
    protected $default_email_templates = array(
        'nowy_wpis_powiadomienie_admin' => array(
            'description' => '
    	                <p>Wysyłane do administracji z informacją o nowym wpisie</p>
    	                <p>Dostępne zmienne:</p>
    	                <ul>
    	                    <li>{EDYCJA_URL} - Link do edycji wpisu</li>
	                        <li>{FIRMA} - Nazwa firmy podana w formularzu</li>

	                        <li>{WOJEWODZTWO} - Województwo wybrane w formularzu</li>
	                        <li>{MIASTO} - Miasto podane w formularzu</li>
	                        <li>{KOD_POCZTOWY} - Kod pocztowy podany w formularzu</li>
	                        <li>{ADRES} - Adres (ulica, nr domu, lokalu)</li>

	                        <li>{EMAIL} - Email podany w formularzu</li>
	                        <li>{EMAIL2} - Email 2 podany w formularzu</li>
	                        <li>{WWW} - Adres www< podany w formularzu/li>

	                        <li>{TELEFON} - Telefon podany w formularzu</li>
	                        <li>{TELEFON2} - Telefon2 podany w formularzu</li>

	                        <li>{SLOWA_KLUCZOWE} - Słowa kluczowe</li>
	                        <li>{BRANZE} - Rodzaje szkód</li>
	                        <li>{OPIS} - Opis</li>
	                        <li>{OFERTA} - Branże</li>
	                        <li>{LOGO} - Obrazek (wraz tagiem img, jeżeli nie istnieje - tekst "brak logo"</li>

    	                </ul>
	                ',
            'topic' => 'Dodano nowy wpis do katalogu firm',
            'content' => '
	                    <p>Dodano nowy wpis do katalogu firm</p>
	                    <p>Link do edycji: <a href="{EDYCJA_URL}">{EDYCJA_URL}</a></p>
	                    <p>Dane wpisu:</p>
	                    <ul>
	                        <li>Nazwa firmy: {FIRMA}</li>

	                        <li>Województwo: {WOJEWODZTWO}</li>
	                        <li>Miasto: {MIASTO}</li>
	                        <li>Kod pocztowy: {KOD_POCZTOWY}</li>
	                        <li>Adres: {ADRES}</li>

	                        <li>Email: {EMAIL}</li>
	                        <li>Email2: {EMAIL2}</li>
	                        <li>WWW: {WWW}</li>
	                        <li>Telefon: {TELEFON}</li>
	                        <li>Telefon2: {TELEFON2}</li>
	                        <li>Słowa kluczowe: {SLOWA_KLUCZOWE}</li>

	                        <li>Rodzaje szkód: {BRANZE}</li>
	                    </ul>

	                    <p>Logo:<p>
	                    <div>{LOGO}</div>

	                     <p>Opis:</p>
	                     <div>{OPIS}</div>

	                     <p>Oferta:</p>
	                     <div>{OFERTA}</div>
	                    ',
        ),
        'nowy_wpis_aktywacja' => array(
            'description' => '
    	                <p>Wysyłane do użytkownika w celu aktywacji wpisu</p>
    	                <p>Dostępne zmienne:</p>
    	                <ul>
    	                    <li>{AKTYWACJA_URL} - Link do aktywacji wpisu</li>
	                        <li>{FIRMA} - Nazwa firmy podana w formularzu</li>

	                        <li>{WOJEWODZTWO} - Województwo wybrane w formularzu</li>
	                        <li>{MIASTO} - Miasto podane w formularzu</li>
	                        <li>{KOD_POCZTOWY} - Kod pocztowy podany w formularzu</li>
	                        <li>{ADRES} - Adres (ulica, nr domu, lokalu)</li>

	                        <li>{EMAIL} - Email podany w formularzu</li>
	                        <li>{EMAIL2} - Email 2 podany w formularzu</li>
	                        <li>{WWW} - Adres www< podany w formularzu/li>

	                        <li>{TELEFON} - Telefon podany w formularzu</li>
	                        <li>{TELEFON2} - Telefon2 podany w formularzu</li>

	                        <li>{SLOWA_KLUCZOWE} - Słowa kluczowe</li>
	                        <li>{BRANZE} - Rodzaje szkód</li>
	                        <li>{OPIS} - Opis</li>
	                        <li>{OFERTA} - Branże</li>
	                        <li>{LOGO} - Obrazek (wraz tagiem img, jeżeli nie istnieje - tekst "brak logo"</li>

    	                </ul>
	                ',
            'topic' => 'Dodano nową firmę do katalogu',
            'content' => '
	                    <p>Dodano nowy wpis do katalogu firm</p>
	                    <p>Aby aktywować wpis należy kliknąć link: <a href="{AKTYWACJA_URL}">{AKTYWACJA_URL}</a></p>
	                    <p>Dane wpisu:</p>
	                    <ul>
	                        <li>Nazwa firmy: {FIRMA}</li>

	                        <li>Województwo: {WOJEWODZTWO}</li>
	                        <li>Miasto: {MIASTO}</li>
	                        <li>Kod pocztowy: {KOD_POCZTOWY}</li>
	                        <li>Adres: {ADRES}</li>

	                        <li>Email: {EMAIL}</li>
	                        <li>Email2: {EMAIL2}</li>
	                        <li>WWW: {WWW}</li>
	                        <li>Telefon: {TELEFON}</li>
	                        <li>Telefon2: {TELEFON2}</li>
	                        <li>Słowa kluczowe: {SLOWA_KLUCZOWE}</li>

	                        <li>Rodzaje szkód: {BRANZE}</li>
	                    </ul>

	                    <p>Logo:<p>
	                    <div>{LOGO}</div>

	                     <p>Opis:</p>
	                     <div>{OPIS}</div>

	                     <p>Oferta:</p>
	                     <div>{OFERTA}</div>
	                    ',
        ),
        'wpis_edycja_link' => array(
            'description' => '
    	                <p>Wysyłane do użytkownika po wybraniu opcji edycji danych</p>
	                    <p>Zawiera link do edycji</p>
    	                <p>Dostępne zmienne:</p>
    	                <ul>
    	                    <li>{POKAZ_URL} - Link do wizytówki firmy</li>
	                        <li>{EDYCJA_URL} - Link do edycji</li>
	                        <li>{FIRMA} - Nazwa firmy </li>
	                        <li>{EMAIL} - Email firmy </li>
	                        <li>{IP} - Adres IP z którego dokonano żądania linku</li>

    	                </ul>
	                ',
            'topic' => 'Link do edycji wizytówki',
            'content' => '
	                    <p>Ktoś, najprawdopodobnie Ty chce zmienić dane firmy {FIRMA} w katalogu firm</p>
	                    <p>Jeżeli nie użyłeś opcji modyfikacji danych na stronie, zignoruj tego maila.</p>
	                    <p></p>
	                    <p>Aby edytować wpis należy kliknąć link: <a href="{EDYCJA_URL}">{EDYCJA_URL}</a></p>
	                    <p></p>
	                    <p>Żądania zmiany dokonano z adresu IP: {IP}<p>
	                    ',
        ),
        'wpis_usun_link' => array(
            'description' => '
    	                <p>Wysyłane do użytkownika po wybraniu opcji usunięcia </p>
	                    <p>Zawiera link do usunięcia (ukrycia) danych</p>
    	                <p>Dostępne zmienne:</p>
    	                <ul>
    	                    <li>{POKAZ_URL} - Link do wizytówki firmy</li>
	                        <li>{USUN_URL} - Link do usunięcia</li>
	                        <li>{FIRMA} - Nazwa firmy </li>
	                        <li>{EMAIL} - Email firmy </li>
	                        <li>{IP} - Adres IP z którego dokonano żądania linku</li>

    	                </ul>
	                ',
            'topic' => 'Link do usunięcia wizytówki firmy',
            'content' => '
	                    <p>Ktoś, najprawdopodobnie Ty chce usunąć dane firmy {FIRMA} w katalogu firm</p>
	                    <p>Jeżeli nie użyłeś opcji usuwania danych na stronie, zignoruj tego maila.</p>
	                    <p></p>
	                    <p>Aby usunąć wpis należy kliknąć link: <a href="{USUN_URL}">{USUN_URL}</a></p>
	                    <p></p>
	                    <p>Żądania usuwania danych dokonano z adresu IP: {IP}<p>
	                    ',
        ),
        'usun_wpis_powiadomienie_admin' => array(
            'description' => '
    	                <p>Wysyłane do administracji z informacją o usuniętym (ukrytym) wpisie przez klienta</p>
    	                <p>Dostępne zmienne:</p>
    	                <ul>
    	                    <li>{EDYCJA_URL} - Link do edycji wpisu</li>
	                        <li>{FIRMA} - Nazwa firmy </li>
	                        <li>{IP} - Adres IP z którego dokonano kliknięcia w link usuwający dane</li>

    	                </ul>
	                ',
            'topic' => 'Usunięto (ukryto) wpis z katalogu firm. ',
            'content' => '
	                    <p>Usunięto (ukryto) wpis z katalogu firm</p>
	                    <p>Link do edycji: <a href="{EDYCJA_URL}">{EDYCJA_URL}</a></p>
	                    <p>Dane:</p>
	                    <ul>
	                        <li>Nazwa firmy: {FIRMA}</li>
	                        <li>Adres IP z którego kliknięto w link: {IP}</li>
	                    </ul>

	                    ',
        ),
        'nowy_komentarz_powiadomienie' => array(
            'description' => '
    	                <p>Wysyłane do administracji z informacją o nowym komentarzu</p>
    	                <p>Dostępne zmienne:</p>
                        
    	                <ul>
                            <li>{FIRMA_URL} - Link do wizytowki firmy</li>
                            <li>{FIRMA} - Nazwa firmy którą skomentowano</li>
    	                    <li>{EDYCJA_URL} - Link do edycji komentarza</li>
	                        <li>{KOMENTARZ} - Komentarz </li>
                            <li>{DATA_ZAKUPU} - Data zakupu usługi</li>
                            <li>{PRZEDMIOT_ZAKUPU} - Przedmiot zakupu</li>
	                        <li>{IP} - Adres IP z którego dodano komentarz</li>
                            <li>{POZYTYWNY} - Czy komentarz pozytywny - "Tak" lub "Nie" </li>
                            <li>{DATA_DODANIA} - Data dodania</li>
    	                </ul>
	                ',
            'topic' => 'Dodano nowy komentarz do wizytowki firmy {FIRMA}',
            'content' => '
	                    <p>Dodano nowy komentarz</p>
	                    <p>Link do edycji: <a href="{EDYCJA_URL}">{EDYCJA_URL}</a></p>
	                    <p>Dane:</p>
	                    <ul>
                            <li>Firma: <a href="{FIRMA_URL}">{FIRMA}</a> -</li>
	                        <li>Komentarz: {KOMENTARZ}</li>
                            <li>Data zakupu: {DATA_ZAKUPU}</li>
                            <li>Przedmiot zakupu: {PRZEDMIOT_ZAKUPU}</li>
                            <li>Pozytywny: {POZYTYWNY}</li>
                            <li>Adres IP z którego dodano: {IP}</li>
                            <li>Data dodania: {DATA_DODANIA}</li>
                            
	                    </ul>

	                    ',
        ),
        'nowy_komentarz_powiadomienie_klient' => array(
            'description' => '
    	                <p>Wysyłane do klienta (właściciela wizytówki) powiadomienia z informacją o nowym komentarzu. 
                        Pod warunkiem że firma ma adres email. 
                        Jeżeli szablon nie będzie zawierał treści powiadomienie nie zostanie wysłane</p>
    	                <p>Dostępne zmienne:</p>
                        
    	                <ul>
                            <li>{FIRMA_URL} - Link do wizytowki firmy</li>
                            <li>{FIRMA} - Nazwa firmy którą skomentowano</li>
	                        <li>{KOMENTARZ} - Komentarz </li>
                            <li>{DATA_ZAKUPU} - Data zakupu usługi</li>
                            <li>{PRZEDMIOT_ZAKUPU} - Przedmiot zakupu</li>
	                        <li>{IP} - Adres IP z którego dodano komentarz</li>
                            <li>{POZYTYWNY} - Czy komentarz pozytywny - "Tak" lub "Nie" </li>
                            <li>{DATA_DODANIA} - Data dodania</li>
    	                </ul>
	                ',
            'topic' => 'Dodano nowy komentarz do wizytowki Państwa firmy',
            'content' => '
	                    <p>Dodano nowy komentarz do wizytówki państwa firmy. Komentarz czeka na akceptację moderatora.</p>
	                    <p>Dane:</p>
	                    <ul>
                            <li>Firma: <a href="{FIRMA_URL}">{FIRMA}</a> -</li>
	                        <li>Komentarz: {KOMENTARZ}</li>
                            <li>Data zakupu: {DATA_ZAKUPU}</li>
                            <li>Przedmiot zakupu: {PRZEDMIOT_ZAKUPU}</li>
                            <li>Pozytywny: {POZYTYWNY}</li>
                            <li>Adres IP z którego dodano: {IP}</li>
                            <li>Data dodania: {DATA_DODANIA}</li>
	                    </ul>

	                    ',
        ),
        'offer_share' => array(
            'description' => '
    	                <p>Wysyłane na podanego e-maila. Link do wizytówki firmy.</p>
    	                <p>Dostępne zmienne:</p>
                        
    	                <ul>
                            <li>{COMPANY_URL} - Link do wizytowki firmy</li>
                            <li>{COMPANY_NAME} - Nazwa firmy którą skomentowano</li>
                            <li>{COMPANY_CITY} - Miejscowość firmy</li>
    	                </ul>
	                ',
            'topic' => 'Polecenie firmy ze strony FirmyMazowieckie.eu',
            'content' => '
	                    <p>Witaj,</p>
	                    <p>Serwis FirmyMazowieckie.eu poleca firmę {COMPANY_NAME} z miasta {COMPANY_CITY}.</p>
                            <p>Kliknij w poniższy link, aby przejść do wizytówki:<br>
                            <a href="{COMPANY_URL}">{COMPANY_NAME}</a></p>
	                    ',
        ),
        'offer_existence_notification' => array(
            'description' => '
                {EXISTENCE_INFO} - wiadomość mówiąca o tym za ile czasu minie określona ilość czasu odkąd firma istnieje w systemie, 
                                   np. "Za miesiąc mija rok", "Za 20 dni mija 2 lata", "Właśnie minęło 6 lat"
                {COMPANY_NAME} - nazwa firmy
            ',
            'topic' => 'Powiadomienie do administratora o tym, że dana firma istnieje już "x" czasu w systemie.',
            'content' => '<p>Witaj,</p>
                          <br />
                          <p>{EXISTENCE_INFO} od kiedy firma "{COMPANY_NAME}" dołączyła do katalogu.</p>
            '
        ),
        'offer_promotion_expiration_notification_for_additional_addresses' => array(
            'description' => '
                {EXPIRATION_INFO} - wiadomość mówiąca o tym za ile czasu upłynie promocja dotycząca dodatkowych adresów"
                {COMPANY_NAME} - nazwa firmy
            ',
            'topic' => 'Powiadomienie do administratora o tym, że danej firmie kończy się promocja dla dodatkowych adresów.',
            'content' => '<p>Witaj,</p>
                          <br />
                          <p>{EXPIRATION_INFO} koniec promocji związanej z dodatkowymi adresami dla firmy "{COMPANY_NAME}".</p>
            '
        ),
        'offer_promotion_expiration_notification_for_visible_properties' => array(
            'description' => '
                {EXPIRATION_INFO} - wiadomość mówiąca o tym za ile czasu upłynie promocja dotycząca dodatkowych pól"
                {COMPANY_NAME} - nazwa firmy
            ',
            'topic' => 'Powiadomienie do administratora o tym, że danej firmie kończy się promocja dla dodatkowych pól.',
            'content' => '<p>Witaj,</p>
                          <br />
                          <p>{EXPIRATION_INFO} koniec promocji związanej z dodatkowymi polami dla firmy "{COMPANY_NAME}".</p>
            '
        )
        
    );

    public function __construct(EntityManager $em) {

        $this->em = $em;
    }

    /**
     * @param string $name Name of the email_template.
     * @return string|null Value of the email_template.
     * @throws \RuntimeException If email_template is not defined.
     */
    public function get($name, $default_content = '', $default_topic = '') {

        $this->all();

        if (array_key_exists($name, $this->email_templates)) {
            return $this->email_templates[$name];
        }

        $_templ = new EmailTemplate();
        $_templ->setName($name);
        $_templ->setContent($default_content);
        $_templ->setTopic($default_topic);
        $this->em->persist($_templ);
        $this->em->flush();

        $this->email_templates[$name] = $_templ;

        return $_templ;
    }

    public function getParsed($name, $values = array()) {

        $template = $this->get($name);

        $parsed_template = new EmailTemplate();

        $topic = $template->getTopic();
        $content = $template->getContent();
        $content_txt = $template->getContentTxt();

        foreach ($values as $key => $val) {

            $topic = str_replace('{' . $key . '}', $val, $topic);
            $content = str_replace('{' . $key . '}', $val, $content);
            $content_txt = str_replace('{' . $key . '}', $val, $content_txt);
        }

        $parsed_template->setTopic($topic);
        $parsed_template->setContent($content);
        $parsed_template->setContentTxt($content_txt);

        return $parsed_template;
    }

    /**
     * @return string[] with key => value
     */
    public function all() {

        $email_templates = array();

        if ($this->is_loaded) {
            return $this->email_templates;
        }

        foreach ($this->getRepo()->findAll() as $email_template) {
            $email_templates[$email_template->getName()] = $email_template;
        }


        foreach ($this->default_email_templates as $_name => $_def_email_template) {
            if (!array_key_exists($_name, $email_templates)) {
                $_templ = new EmailTemplate();
                $_templ->setName($_name);
                $_templ->setDescription($_def_email_template['description']);
                $_templ->setContent($_def_email_template['content']);
                $_templ->setTopic($_def_email_template['topic']);
                $this->em->persist($_templ);
                $this->em->flush();
                $email_templates[$_name] = $_templ;
            }
        }

        $this->email_templates = $email_templates;
        $this->is_loaded = true;
        ;

        return $email_templates;
    }

    /**
     * @return EntityRepository
     */
    protected function getRepo() {
        if ($this->repo === null) {
            $this->repo = $this->em->getRepository(get_class(new EmailTemplate()));
        }

        return $this->repo;
    }

}
