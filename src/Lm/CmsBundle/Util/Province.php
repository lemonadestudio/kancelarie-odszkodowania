<?php

namespace Lm\CmsBundle\Util;

class Province {
    
    public static function getProvinces() {
        return array(
            1 => 'dolnośląskie',
            2 => 'kujawsko-pomorskie',
            3 => 'lubelskie',
            4 => 'lubuskie',
            5 => 'łódzkie',
            6 => 'małopolskie',
            7 => 'mazowieckie',
            8 => 'opolskie',
            9 => 'podkarpackie',
            10 => 'podlaskie',
            11 => 'pomorskie',
            12 => 'śląskie',
            13 => 'świętokrzyskie',
            14 => 'warmińsko-mazurskie',
            15 => 'wielkopolskie',
            16 => 'zachodniopomorskie'
        );
    }
    
    public static function exists($idOrName) {
        $provinces = $this::getProvinces();
        
        if (array_key_exists($idOrName, $provinces)) {
            return true;
        }
        
        if (in_array($idOrName, $provinces)) {
            return true;
        }
        
        return false;
    }
    
    public static function getName($id) {
        $provinces = $this::getProvinces();
        
        if (array_key_exists($id, $provinces)) {
            return $provinces[$id];
        }
        
        return null;
    }
}
