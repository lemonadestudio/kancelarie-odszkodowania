<?php

namespace Lm\CmsBundle\Util;

class DateIntervalUtils 
{
    public static function AreEqual(\DateInterval $d1, \DateInterval $d2)
    {
        return $d1->y === $d2->y &&
               $d1->m === $d2->m &&
               $d1->d === $d2->d &&
               $d1->h === $d2->h &&
               $d1->i === $d2->i &&
               $d1->s === $d2->s &&
               $d1->invert === $d2->invert;
    }
}
