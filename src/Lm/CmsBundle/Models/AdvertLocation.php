<?php

namespace Lm\CmsBundle\Models;

class AdvertLocation
{
    const LEFT_SIDE_BAR = 1;
    const HOMEPAGE_TOP_UNDER_BREADCRUMBS = 2;
}
