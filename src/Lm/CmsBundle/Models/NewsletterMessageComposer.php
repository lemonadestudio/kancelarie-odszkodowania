<?php

namespace Lm\CmsBundle\Models;

use Symfony\Component\DomCrawler\Crawler;

abstract class NewsletterMessageComposer 
{
    protected function modifyImageElements($htmlMessage, $cmsHost) 
    {
        $document = new \DOMDocument();
        $document->loadHtml($htmlMessage);
        $images = $document->getElementsByTagName('img');

        foreach ($images as $domElement) {
            $this->changeImagesUrlToAbsolute($domElement, $cmsHost);
        }

        return $this->getDomDocumentHtml($document);
    }
    
    protected function changeImagesUrlToAbsolute($domElement, $cmsHost) 
    {
        if (strstr($domElement->getAttribute('src'), 'http') === false) {
            $domElement->setAttribute('src', 'http://' . $cmsHost . $domElement->getAttribute('src'));
        }
    }
    
    protected function getDomDocumentHtml($document)
    {
        $crawler = new Crawler();
        $crawler->addDocument($document);
        
        $html = '';

        foreach ($crawler as $domElement) {
            $html .= $domElement->ownerDocument->saveHTML($domElement);
        }

        return $html;
    }
    
    public abstract function compose($recipient);
}
