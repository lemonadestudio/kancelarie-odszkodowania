<?php

namespace Lm\CmsBundle\Models;

class NewsletterMessageComposerForEmailRecipient extends NewsletterMessageComposer
{
    private $templateEngine;
    private $cmsHost;
    
    public function __construct($templateEngine, $cmsHost)
    {
        $this->templateEngine = $templateEngine;
        $this->cmsHost = $cmsHost;
    }
    
    public function compose($recipient)
    {
        $htmlMessage = $this->templateEngine->render('LmCmsBundle:NewsletterMessage:mail.html.twig', array(
            'content' => nl2br($recipient->getNewsletterMessage()->getMessage())
        ));
        
        return $this->modifyImageElements($htmlMessage, $this->cmsHost);
    }
}
