<?php

namespace Lm\CmsBundle\Models;

class OfferPropertyType 
{
    private function __construct() {}
    
    const PHONE = 1;
    const ADDITIONAL_PHONE = 2;
    const EMAIL = 3;
    const ADDITIONAL_EMAIL = 4;
    const WEBSITE_URL = 5;
    const FACEBOOK_URL = 6;
}
