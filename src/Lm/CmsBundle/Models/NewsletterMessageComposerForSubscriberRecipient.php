<?php

namespace Lm\CmsBundle\Models;

class NewsletterMessageComposerForSubscriberRecipient extends NewsletterMessageComposer
{
    private $router;
    private $templateEngine;
    private $cmsHost;
    
    public function __construct($router, $templateEngine, $cmsHost)
    {
        $this->router = $router;
        $this->templateEngine = $templateEngine;
        $this->cmsHost = $cmsHost;
    }
    
    public function compose($recipient)
    {
        $subscriptionDeactivationUrl = $this->router->generate('lm_cms_newsletter_unsubscribe', array(
            'token' => $recipient->getNewsletterSubscriber()->getToken()));
        
        $htmlMessage = $this->templateEngine->render('LmCmsBundle:NewsletterMessage:mail.html.twig', array(
            'content' => nl2br($recipient->getNewsletterMessage()->getMessage()),
            'subscriptionDeactivationUrl' => 'http://' . $this->cmsHost . $subscriptionDeactivationUrl
        ));
        
        return $this->modifyImageElements($htmlMessage, $this->cmsHost);
    }
}
