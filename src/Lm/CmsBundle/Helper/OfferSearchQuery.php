<?php

namespace Lm\CmsBundle\Helper;

use Lm\CmsBundle\Util\GeolocationFinder;
use Lm\CmsBundle\Util\GeolocationResultStatus;
use Lm\CmsBundle\Entity\Coordinates;

class OfferSearchQuery
{
    private $doctrine;
    private $queryBuilder;
    private $queryWhereExpressions;
    private $queryParameters;
    private $readableSearchParts;
    private $googlePlacesApiKey;
    private $category;
    private $knpPaginator;
    private $city = null;
    
    public function __construct($doctrine, $knpPaginator, $googlePlacesApiKey, $city)
    {
        $this->doctrine = $doctrine;
        $this->queryBuilder = $doctrine
                ->getRepository('LmCmsBundle:Offer')
                ->createQueryBuilder('o');
        $this->queryWhereExpressions = array();
        $this->queryWhereExpressions[] = $this->queryBuilder->expr()->andX('o.active = 1');
        $this->queryParameters = array();
        $this->readableSearchParts = array();
        $this->googlePlacesApiKey = $googlePlacesApiKey;
        $this->knpPaginator = $knpPaginator;
        $this->city = $city;
    }
    
    public function searchByKeyword($keyword)
    {
        if (empty($keyword)) {
            return $this;
        }
        
        $this->queryWhereExpressions[] = $this->queryBuilder->expr()->orX(
            $this->queryBuilder->expr()->like('o.title', ':keyword'),
            $this->queryBuilder->expr()->like('o.content', ':keyword'),
            $this->queryBuilder->expr()->like('o.content2', ':keyword'),
            $this->queryBuilder->expr()->like('o.keywords', ':keyword'),
            $this->queryBuilder->expr()->like('o.www', ':keyword'),
            $this->queryBuilder->expr()->like('c.name', ':keyword'),
            $this->queryBuilder->expr()->like('ma.province', ':keyword'),
            $this->queryBuilder->expr()->like('ma.street', ':keyword'),
            $this->queryBuilder->expr()->like('ma.postCode', ':keyword'),
            $this->queryBuilder->expr()->like('aa.street', ':keyword'),
            $this->queryBuilder->expr()->like('aa.postCode', ':keyword')
        );
        $this->queryParameters['keyword'] = '%' . $keyword . '%';
        $this->readableSearchParts[] = array(
            'label' => 'Słowo kluczowe',
            'value' => $keyword
        );
        
        return $this;
    }
    
    public function searchByCategory($categoryId) 
    {
        if (empty($categoryId)) {
            return $this;
        }
        
        $this->queryWhereExpressions[] = $this->queryBuilder
                ->expr()->in('c.id', array($categoryId));
        
        $this->category = $this->doctrine
                ->getRepository('LmCmsBundle:OfferCategory')
                ->findOneBy(array('id' => $categoryId));
            
        if ($this->category != null) {
            $this->readableSearchParts[] = array(
                'label' => 'Kategoria', 
                'value' => $this->category->getName()
            );
        }
        
        return $this;
    }
    
    public function searchByCityAndRadius($city, $radius) 
    {
        $radiusExists = !empty($radius);
        $cityExists = !empty($city);
        $coordinates = null;
        
        if ($radiusExists) {
            $this->readableSearchParts[] = array(
                'label' => 'Promień wyszukiwania',
                'value' => $radius . ' km'
            );
        }
        
        if ($cityExists) {
            $this->readableSearchParts[] = array(
                'label' => 'Miasto', 
                'value' => $city
            );
            $coordinates = $this->getCityCoordinates($city);
        }
        
        if ($cityExists && $radiusExists && $coordinates) {
            $szer_geo = $coordinates->getLatitude();
            $dlug_geo = $coordinates->getLongitude();
            $diff_szer = $this->km_stopnie_szerokosc($radius);
            $diff_dlug = $this->km_stopnie_dlugosc($radius);
            
            $this->queryWhereExpressions[] = $this->queryBuilder->expr()->orX(
                $this->queryBuilder->expr()->andX(
                    $this->queryBuilder->expr()->gt('ma.latitude', ':minLatitude'),
                    $this->queryBuilder->expr()->lt('ma.latitude', ':maxLatitude'),
                    $this->queryBuilder->expr()->gt('ma.longitude', ':minLongitude'),
                    $this->queryBuilder->expr()->lt('ma.longitude', ':maxLongitude')
                ),
                $this->queryBuilder->expr()->andX(
                    $this->queryBuilder->expr()->gt('aa.latitude', ':minLatitude'),
                    $this->queryBuilder->expr()->lt('aa.latitude', ':maxLatitude'),
                    $this->queryBuilder->expr()->gt('aa.longitude', ':minLongitude'),
                    $this->queryBuilder->expr()->lt('aa.longitude', ':maxLongitude')
                )   
            );
            
            $this->queryParameters['minLatitude'] = $szer_geo - $diff_szer;
            $this->queryParameters['maxLatitude'] = $szer_geo + $diff_szer;
            $this->queryParameters['minLongitude'] = $dlug_geo - $diff_dlug;
            $this->queryParameters['maxLongitude'] = $dlug_geo + $diff_dlug;
        } else if ($cityExists) {
            $this->queryWhereExpressions[] = $this->queryBuilder->expr()->orX(
                $this->queryBuilder->expr()->like('ma.city', ':city'),
                $this->queryBuilder->expr()->like('aa.city', ':city')
            );
            $this->queryParameters['city'] = "%{$city}%";
        }
        
        return $this;
    }
    
    public function searchByBeeingPromoted($onlyPromoted)
    {
        if (!isset($onlyPromoted) || empty($onlyPromoted) || !$onlyPromoted) {
            return $this;
        }
        
        $now = new \DateTime();
        $promotionMinDate = $now->add(\DateInterval::createFromDateString('-14 days'));
        $this->queryParameters['promotionMinDate'] = $promotionMinDate;
        $this->queryWhereExpressions[] = $this->queryBuilder->expr()->gte('o.promoted', ':promotionMinDate');

        $this->readableSearchParts[] = array(
            'label' => 'Promowane',
            'value' => 'Tak'
        );
        
        return $this;
    }
    
    public function getResults($pageNumber, $pageSize)
    {
        $offersQuery = $this->getOffersQuery();
        $offersPagination = $this->knpPaginator->paginate($offersQuery, $pageNumber, $pageSize);
        
        $offerIds = array();
        foreach ($offersPagination->getItems() as $offer) {
            $offerIds[] = $offer->getId();
        }
        
        if (count($offerIds)) {
            // wyniki ponizszych zapytan, czyli dodatkowe adresy i kategorie
            // beda automatycznie przypisane do wczesniej pobranych ofert
            // bo doctrine operuje na cache-u
            $offerRepo = $this->doctrine->getRepository('LmCmsBundle:Offer');
            $offerRepo->getOffersAdditionalAddresses($offerIds);
            $offerRepo->getOffersCategories($offerIds);
        }
        
        return array(
            'offers' => $offersPagination,
            'readableSearchParts' => $this->readableSearchParts,
            'category' => $this->category
        );
    }
    
    private function concatenateWhereExpressions() 
    {
        $queryWhereFullExpression = $this->queryBuilder->expr()->andX();
        
        foreach ($this->queryWhereExpressions as $expression) {
            $queryWhereFullExpression->add($expression);
        }
        
        return $queryWhereFullExpression;
    }
    
    private function getCityCoordinates($address) 
    {
        $address = trim($address);
        $coordinates = $this->doctrine
                ->getRepository('LmCmsBundle:Coordinates')
                ->createQueryBuilder('c')
                ->select('partial c.{id,latitude,longitude}')
	    	->where('c.address = :address')
                ->andWhere('c.latitude > 0')
                ->andWhere('c.longitude > 0')
                ->setParameter('address', $address)
                ->setMaxResults(1)
                ->getQuery()
                ->getOneOrNullResult();
      
        if($coordinates) {
            return $coordinates;
        }
        
        $geolocationFinder = new GeolocationFinder($this->googlePlacesApiKey);
        $geolocationResult = $geolocationFinder->find($address);
        
        if (!$geolocationResult->hasStatus(GeolocationResultStatus::FOUND)) {
            return null;
        }
        
        $newCoordinates = new Coordinates();
        $newCoordinates->setAddress($address);
        $newCoordinates->setLatitude($geolocationResult->getLatitude());
        $newCoordinates->setLongitude($geolocationResult->getLongitude());

        $em = $this->doctrine->getManager();
        $em->persist($newCoordinates);
        $em->flush();

        return $newCoordinates;
    }
    
    private function km_stopnie_dlugosc($km) 
    {
        // uproszczenie - dla szerokości geograficznej 52N (środek polski)
        // 1 st długości = 68 km
        $_1km = 1 / 68;
        
        return round($_1km * $km, 6);
    }
    
    private function km_stopnie_szerokosc($km) 
    {
        // dla każdej długości geograficznej
        // 1st szerokości = 111 km
        $_1km = 1 / 111;
        
        return round($_1km * $km, 6);
    }
    
    private function getOffersQuery()
    {
        $queryWhereFullExpression = $this->concatenateWhereExpressions();
        
        $offersCount = $this->doctrine
                ->getRepository('LmCmsBundle:Offer')
                ->createQueryBuilder('o')
                ->select('COUNT(DISTINCT o.id)')
                ->leftJoin('o.categories', 'c')
                ->leftJoin('o.mainAddress', 'ma')
                ->leftJoin('o.additionalAddresses', 'aa')
                ->add('where', $queryWhereFullExpression)
                ->setParameters($this->queryParameters)
                ->getQuery()
                ->getSingleScalarResult();
        
        return $this->doctrine
                ->getRepository('LmCmsBundle:Offer')
                ->createQueryBuilder('o')
                ->select('partial o.{id,title,promoted,slug,image}, partial ma.{id,province,city,street,postCode,latitude,longitude}')
                    ->addSelect('CASE
                         WHEN ma.city = :city THEN 0
                         ELSE 1
                     END as HIDDEN city')
                ->distinct(true)
                ->leftJoin('o.categories', 'c')
                ->leftJoin('o.mainAddress', 'ma')
                ->leftJoin('o.additionalAddresses', 'aa')
                ->add('where', $queryWhereFullExpression)
                ->setParameters($this->queryParameters)
                ->setParameter('city', $this->city)
                ->orderBy('city', 'ASC')
                    ->addOrderBy('o.title', 'ASC')
                    ->addOrderBy('o.id', 'ASC')
                    ->addOrderBy('o.promoted', 'DESC')
                ->getQuery()
                ->setHint('knp_paginator.count', $offersCount)
                ->setHint(\Doctrine\ORM\Query::HINT_FORCE_PARTIAL_LOAD, true);
    }
}
