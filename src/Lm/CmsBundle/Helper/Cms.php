<?php

namespace Lm\CmsBundle\Helper;

use Gedmo\Sluggable\Util\Urlizer;
use Lm\CmsBundle\Model\Domain;
use Symfony\Component\DependencyInjection\ContainerInterface;

class Cms {

    /**
     * @var ContainerInterface
     */
    private $container = null;

    public function __construct(ContainerInterface $container = null) {
        $this->container = $container;
    }

    public function make_url($slug, $strategy = 'default') {

        $options = array(
            'separator' => '-'
        );

        // kod zaczerpnięty z Gedmo Doctrine Extensions
        // build the slug
        // Step 1: transliteration, changing 北京 to 'Bei Jing'
        $slug = call_user_func_array(
                array('Gedmo\Sluggable\Util\Urlizer', 'transliterate'), array($slug, $options['separator'])
        );
        // Step 2: urlization (replace spaces by '-' etc...)

        $slug = Urlizer::urlize($slug, $options['separator']);


        // stylize the slug
        switch ($strategy) {
            case 'camel':
                $slug = preg_replace_callback('/^[a-z]|' . $options['separator'] . '[a-z]/smi', function ($m) {
                    return strtoupper($m[0]);
                }, $slug);
                break;

            case 'lower':
                if (function_exists('mb_strtolower')) {
                    $slug = mb_strtolower($slug);
                } else {
                    $slug = strtolower($slug);
                }
                break;

            case 'upper':
                if (function_exists('mb_strtoupper')) {
                    $slug = mb_strtoupper($slug);
                } else {
                    $slug = strtoupper($slug);
                }
                break;

            default:
                // leave it as is
                break;
        }

        return $slug;
    }

    public function _old_make_url($string, $strategy = 'default') {


        if (!in_array($strategy, array('default'))) {
            $strategy = 'default';
        }

        $clean = $string;

        switch ($strategy) {

            case 'default':

                $delimiter = '-';

                $string = str_replace(
                        array('Ą', 'Ć', 'Ę', 'Ł', 'Ń', 'Ó', 'Ś', 'Ź', 'Ż', 'ą', 'ć', 'ę', 'ł', 'ń', 'ó', 'ś', 'ź', 'ż'), array('a', 'c', 'e', 'l', 'n', 'o', 's', 'z', 'z', 'a', 'c', 'e', 'l', 'n', 'o', 's', 'z', 'z'), $string);
                $clean = iconv('UTF-8', 'ASCII//TRANSLIT', $string);
                $clean = preg_replace("/[^a-zA-Z0-9\/_|+ -]/", '', $clean);
                $clean = strtolower(trim($clean, '-'));
                $clean = preg_replace("/[\/_|+ -]+/", $delimiter, $clean);


                break;

            default: break;
        }

        return $clean;
    }

    /**
     * Zwraca domeny z konfiguracji cms
     */
    public function domains() {

        $domains = $this->container->parameters['lm_cms.domains'];

        return $domains;
    }

    /**
     * Zwraca informacjie o aktualnej domenie
     * @return Domain
     */
    public function domain() {

        $domains = $this->domains();
        $domain = $this->container->get('request')->attributes->get('_domain');
        return new Domain($domains[$domain]);
    }
    
    public static function generateNewRandomToken() {
        return sha1(time() . rand(1000000, 9000000));
    }

}
