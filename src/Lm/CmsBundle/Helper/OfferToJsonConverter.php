<?php

namespace Lm\CmsBundle\Helper;

class OfferToJsonConverter
{
    public function convertOffers(array $offers)
    {
        return json_encode($this->mapOffers($offers));
    }
    
    public function convertOffer(\Lm\CmsBundle\Entity\Offer $offer)
    {
        return json_encode($this->mapOffer($offer));
    }
    
    private function mapOffers(array $offers)
    {
        $convertedOffers = array();
        
        foreach ($offers as $offer) {
            $convertedOffers[] = $this->mapOffer($offer);
        }
        
        return $convertedOffers;
    }
    
    private function mapOffer(\Lm\CmsBundle\Entity\Offer $offer)
    {
        $convertedOffer = array(
            'id' => $offer->getId(),
            'title' => $offer->getTitle()
        );

        $addresses = array();

        if ($offer->getMainAddress()) {
            $addresses[] = $this->mapAddress($offer->getMainAddress());
        }

        if ($offer->getAdditionalAddresses()) {
            foreach ($offer->getAdditionalAddresses() as $additionalAddress) {
                $addresses[] = $this->mapAddress($additionalAddress);
            }
        }

        $convertedOffer['addresses'] = $addresses;
        
        return $convertedOffer;
    }
    
    private function mapAddress($address)
    {
        $convertedAddress = array(
            'city' => $address->getCity(),
            'street' => $address->getStreet(),
            'postCode' => $address->getPostCode(),
            'coords' => null
        );

        if ($address->hasCoordinates()) {
            $convertedAddress['coords'] = array(
                'latitude' => $address->getLatitude(),
                'longitude' => $address->getLongitude()
            );
        }
        
        return $convertedAddress;
    }
}
