<?php

namespace Lm\CmsBundle\Admin;

use Doctrine\ORM\EntityRepository;
use Sonata\AdminBundle\Admin\AdminInterface;
use Sonata\AdminBundle\Route\RouteCollection;
use Sonata\AdminBundle\Show\ShowMapper;
use Sonata\AdminBundle\Admin\Admin;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Validator\ErrorElement;
use Sonata\AdminBundle\Form\FormMapper;

class NewsletterSubscriberAdmin extends Admin 
{
    public function getTemplate($title) 
    {
        switch ($title) {
            case 'edit':
                return 'LmCmsBundle:Admin\NewsletterSubscriber:edit.html.twig';
            default:
                return parent::getTemplate($title);
                break;
        }
    }
    
    public function configureShowFields(ShowMapper $showMapper) 
    {
        $showMapper
                ->add('email')
                ->add('createdAt');
    }

    public function configureListFields(ListMapper $listMapper) 
    {
        $listMapper
                ->addIdentifier('name')
                ->addIdentifier('email')
                ->addIdentifier('isActive')
                ->add('createdAt')
                ->add('_action', 'actions', array(
                    'actions' => array(
                        'edit' => array(),
                        'delete' => array())
        ));
    }

    public function configureDatagridFilters(DatagridMapper $datagridMapper) 
    {
        $datagridMapper
                ->add('email')
                ->add('isActive')
                ->add('name');
    }

    public function configureFormFields(FormMapper $formMapper) 
    {
        $formMapper
                ->with('Główne dane')
                ->add('email', null, array('required' => true))
                ->add('name', null, array('required' => false))
                ->add('isActive', null, array('required' => false));
    }
}
