<?php

namespace Lm\CmsBundle\Admin;

use Sonata\AdminBundle\Route\RouteCollection;

use Sonata\AdminBundle\Admin\Admin;
use Sonata\AdminBundle\Form\FormMapper;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Show\ShowMapper;

class CoordinatesAdmin extends Admin {

	protected $translationDomain = 'LmCmsBundle';

	protected $datagridValues = array(
			'_page'       => 1,
			'_sort_order' => 'ASC', // sort direction
			'_sort_by' => 'address' // field name
	);

	protected function configureRoutes(RouteCollection $collection) {

//				$collection->remove('create');
//				$collection->remove('edit');


	}

    public function configureShowFields(ShowMapper $showMapper) {

        $showMapper
                ->add('id')
                ->add('address')
                ->add('latitude')
                ->add('longitude')
                ;
    }


    public function configureListFields(ListMapper $listMapper) {

        $listMapper->addIdentifier('id')
        ->add('address')
        ->add('latitude')
        ->add('longitude')
        
       ;
        
        $listMapper->add('_action', 'actions', array(
       		$this->trans('actions') => array(
       				'edit' => array(),
         			'delete' => array()
       		)
       ));

    }

    public function configureDatagridFilters(DatagridMapper $datagridMapper)
    {
         $datagridMapper
             ->add('address')

         ;
    }

    public function configureFormFields(FormMapper $formMapper)
    {
         $formMapper
            ->add('address')
            ->add('latitude')
            ->add('longitude')
         ;
    }



}