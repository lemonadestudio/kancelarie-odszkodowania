<?php

namespace Lm\CmsBundle\Admin;

use Doctrine\ORM\EntityRepository;

use Sonata\AdminBundle\Admin\AdminInterface;

use Sonata\AdminBundle\Route\RouteCollection;

use Sonata\AdminBundle\Show\ShowMapper;

use Sonata\AdminBundle\Admin\Admin;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Validator\ErrorElement;
use Sonata\AdminBundle\Form\FormMapper;

use Knp\Menu\FactoryInterface as MenuFactoryInterface;
use Knp\Menu\ItemInterface as MenuItemInterface;

class SliderPhotoAdmin extends Admin {

    protected $translationDomain = 'LmCmsBundle';

    protected $datagridValues = array(
    		'_page'       => 1,
    		'_per_page'   => 25,
    		'_sort_by' => 'publishDate',
    		'_sort_order' => 'DESC',

    );

    protected function configureSideMenu(MenuItemInterface $menu, $action, AdminInterface $childAdmin = null)
    {
		if (!$childAdmin && !in_array($action, array('edit'))) {
        	return;
    	}
		    $admin = $this->isChild() ? $this->getParent() : $this;
		    $id = $admin->getRequest()->get('id');
		    // $_object = $this->getObject($id);
		    $menu->addChild('Edytuj', array('uri' => $this->generateUrl('edit', array('id' => $id))));


    }

    protected function configureRoutes(RouteCollection $collection)
    {

//     	$collection->add('edit_rows', '{id}/edit_rows');


    }

	public function getTemplate($name)
	{
		switch ($name) {
			case 'edit':
				return 'LmCmsBundle:Admin\SliderPhoto:edit.html.twig';
				break;
// 			case 'show':
// 				return 'LmCmsBundle:Admin\Aktualnosci:show.html.twig';
// 				break;

			default:
				return parent::getTemplate($name);
				break;
		}
	}


    protected function configureFormFields(FormMapper $formMapper)
    {

    	$this->getSubject()->setUpdated( new \DateTime('now'));
        $formMapper

	        	->with('label.mainoptions')
	        		->add('title')
	        		->add('content')


					->add('_file_image', 'file', array('required' => false, 'label' => 'label.main_photo'))
					->add('kolejnosc')
					->add('link')




      	-> setHelps(array(

      					'lang'	=> $this->trans('help.lang')
        		))

        		->add('updated', null, array('label' => ' ', 'with_seconds' => 'true',  'attr'=>array('style'=>'display:none;')))
      ;

    }

    public function configureShowFields(ShowMapper $showMapper) {

    	$showMapper

	    	->add('id')
	    	->add('image')
	    	->add('kolejnosc')
	    	->add('link')
	    	->add('created')
	    	->add('updated')

    	;
    }

    protected function configureDatagridFilters(DatagridMapper $datagridMapper)
    {
        $datagridMapper



        ;
    }

    protected function configureListFields(ListMapper $listMapper)
    {



        $listMapper

            ->addIdentifier('id')
            ->add('photo', 'string', array('template' => 'LmCmsBundle:Admin\List:obrazek.html.twig'))
            ->add('kolejnosc')
            ->add('title')
            ->add('content')
            ->add('link')

            ->add('_action', 'actions', array(
            		'actions' => array(
            				// 'view' => array(),
            				'edit' => array(),
            				'delete' => array(),

            				)
            	))


        ;
    }




}