<?php

namespace Lm\CmsBundle\Admin;

use Lm\CmsBundle\Entity\NewsletterSubscriber;
use Lm\CmsBundle\Entity\NewsletterMessage;
use Doctrine\ORM\EntityRepository;
use Sonata\AdminBundle\Admin\AdminInterface;
use Sonata\AdminBundle\Route\RouteCollection;
use Sonata\AdminBundle\Show\ShowMapper;
use Sonata\AdminBundle\Admin\Admin;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Validator\ErrorElement;
use Sonata\AdminBundle\Form\FormMapper;
use Symfony\Component\Validator\Constraints\Choice;

class NewsletterMessageAdmin extends Admin {

    protected function configureRoutes(RouteCollection $collection) {
        $collection->remove('edit');
    }

    public function configureShowFields(ShowMapper $showMapper) {
        $showMapper
                ->add('title')
                ->add('message', null, array('safe' => true))
                ->add('getRecipientsAsString')
                ->add('getRecipientsCount')
                ->add('getStartSendDateText')
                ->add('getStatus')
                ->add('createdAt');
    }

    public function configureListFields(ListMapper $listMapper) {
        $listMapper
                ->add('getShortTitle')
                ->add('getRecipientsCount')
                ->add('getStartSendDateText')
                ->add('createdAt')
                ->add('_action', 'actions', array(
                    'actions' => array(
                        'view' => array(),
                        'delete' => array(),
                    )
                ));
    }

    public function configureDatagridFilters(DatagridMapper $datagridMapper) {
        $datagridMapper
                ->add('title');
    }

    public function getTemplate($name) {
        switch ($name) {
            case 'edit':
                return 'LmCmsBundle:Admin\NewsletterMessage:edit.html.twig';
            default:
                return parent::getTemplate($name);
        }
    }

    public function configureFormFields(FormMapper $formMapper) {
        $subscribersChoices = $this->getSubscribersChoices();

        $formMapper
            ->with('Główne dane')
            ->add('title', null, array('required' => true))
            ->add('message', null, array('required' => false, 'attr' => array('class' => 'sonata-medium wysiwyg-basic-with-img')))
            ->add('startSendDate')
            ->add('recipientIds', 'choice', array(
                'multiple' => true,
                'choices' => $subscribersChoices,
                'required' => false,
                'constraints' => array(
                    new Choice(array(
                        'choices' => array_keys($subscribersChoices),
                        'multiple' => true))
                ),
                'attr' => array(
                    'class' => 'recipientIds'
                )))
            ->add('fileWithEmails', 'file', array(
                'required' => false,
                'label' => 'Plik z adresami e-mail'
            ))
            ->setHelps(array(
                'fileWithEmails' => 'Wymagany jest plik <b>txt</b> lub <b>csv</b>, w których adresy e-mail są oddzielone przecinkami.'
            ));
    }

    private function getSubscribersChoices() {
        $newsletterSubscriberRepo = $this->modelManager
                ->getEntityManager('Lm\CmsBundle\Entity\NewsletterSubscriber')
                ->getRepository('Lm\CmsBundle\Entity\NewsletterSubscriber');

        $subscribers = $newsletterSubscriberRepo->getAllActiveAsArray();
        $choices = array();

        foreach ($subscribers as $subscriber) {
            $choices[$subscriber['id']] = $subscriber['email'];
        }

        return $choices;
    }
}
