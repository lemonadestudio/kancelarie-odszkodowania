<?php

namespace Lm\CmsBundle\Admin;

use Lm\CmsBundle\Entity\Offer;
use Lm\CmsBundle\Util\Province;
use Doctrine\ORM\EntityRepository;
use Sonata\AdminBundle\Admin\AdminInterface;
use Sonata\AdminBundle\Route\RouteCollection;
use Sonata\AdminBundle\Show\ShowMapper;
use Sonata\AdminBundle\Admin\Admin;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Validator\ErrorElement;
use Sonata\AdminBundle\Form\FormMapper;
use Knp\Menu\FactoryInterface as MenuFactoryInterface;
use Knp\Menu\ItemInterface as MenuItemInterface;
use Knp\Menu\MenuItem;

class OfferAdmin extends Admin 
{
    protected $translationDomain = 'LmCmsBundle';
    protected $datagridValues = array(
        '_page' => 1,
        '_per_page' => 25,
        '_sort_by' => 'publishDate',
        '_sort_order' => 'DESC',
    );

    protected function configureSideMenu(MenuItemInterface $menu, $action, AdminInterface $childAdmin = null) 
    {
        if (!$childAdmin && !in_array($action, array('edit'))) {
            return;
        }
        $admin = $this->isChild() ? $this->getParent() : $this;
        $id = $admin->getRequest()->get('id');
        // $_object = $this->getObject($id);
        $menu->addChild('Edytuj', array('uri' => $this->generateUrl('edit', array('id' => $id))));
    }

    protected function configureRoutes(RouteCollection $collection) 
    {
        $collection->add('exportEmails');
    }

    public function getTemplate($name) 
    {
        switch ($name) {
            case 'edit':
                return 'LmCmsBundle:Admin\Offer:edit.html.twig';
            case 'list':
                return 'LmCmsBundle:Admin\Offer:list.html.twig';
            default:
                return parent::getTemplate($name);
        }
    }

    protected function configureFormFields(FormMapper $formMapper) 
    {
        $this->getSubject()->setUpdated(new \DateTime('now'));

        $formMapper
                ->with('label.mainoptions')
                ->add('title', null, array('required' => true))
                ->add('content', null, array(
                    'label' => 'Opis firmy',
                    'required' => false, 
                    'attr' => array('class' => 'sonata-medium wysiwyg')))
                ->add('content2', null, array(
                    'label' => 'Oferta',
                    'required' => false, 
                    'attr' => array('class' => 'sonata-medium wysiwyg')))
                ->add('www', null, array('required' => false))
                ->add('active', null, array(
                    'required' => false
                ))
                ->add('categories', null, array('required' => false))
                ->add('mainAddress', 'sonata_type_admin', array(
                    'required' => false, 
                    'delete' => false,
                ))
                ->add('additionalAddresses', 'sonata_type_collection', array(
                    'required' => false,
                    'by_reference' => false
                ), array(
                    'edit' => 'inline',
                    'inline' => 'table'
                ))
                ->add('expirationOfPromotionForAdditionalAddresses', null, array('required' => false))
                ->add('visibleProperties', null, array('required' => false))
                ->add('expirationOfPromotionForVisibleProperties', null, array('required' => false))
                ->add('keywords', 'textarea', array('required' => false, 'label' => 'label.keywords', 'attr' => array('class' => 'span8', 'rows' => 3)))
                ->add('seoTitle', null, array(
                    'required' => false,
                    'label' => 'label.seoTitle'
                ))
                ->add('description', 'textarea', array('required' => false, 'label' => 'label.description', 'attr' => array('class' => 'span10', 'rows' => 10)))
                ->add('_file_image', 'file', array('required' => false, 'label' => 'label.main_photo'))
                ->add('slug', null, array('required' => false))
                ->add('automaticSeo')
                ->add('updated', null, array('label' => ' ', 'with_seconds' => 'true', 'attr' => array('style' => 'display:none;')))
                ->add('facebookUrl', null, array('required' => false))
                ->setHelps(array(
                    'title' => $this->trans('help.title'),
                    'automaticSeo' => $this->trans('help.automatic_seo'),
                    'slug' => $this->trans('help.slug'),
                    'publishDate' => $this->trans('Data realizacji'),
                ))
        ;
    }

    public function configureShowFields(ShowMapper $showMapper) 
    {
        $showMapper
                ->add('title')
                ->add('mainAddress.street')
                ->add('mainAddress.city')
                ->add('mainAddress.postCode')
                ->add('mainAddress.province')
                ->add('mainAddress.phone')
                ->add('mainAddress.email')
                ->add('www')
                ->add('created')
                ->add('updated')
        ;
    }

    protected function configureDatagridFilters(DatagridMapper $datagridMapper) 
    {
        $datagridMapper
                ->add('title')
                ->add('categories')
                ->add('mainAddress.email')
                ->add('isImportedFromSecondFile', 'doctrine_orm_callback', array(
                    'callback' => function($queryBuilder, $alias, $field, $value) {
                        if (empty($value['value'])) {
                            return;
                        }
                        
                        $queryBuilder->andWhere("{$alias}.importedFileNumber = 2");

                        return true;
                    },
                    'field_type' => 'checkbox'
                ))
        ;
    }

    protected function configureListFields(ListMapper $listMapper) 
    {
        $listMapper
                ->addIdentifier('title')
                ->add('mainAddress.email')
                ->add('photo', 'string', array('template' => 'LmCmsBundle:Admin\List:obrazek.html.twig'))
                ->add('isImportedFromSecondFile', 'boolean')
                ->add('created')
                ->add('updated')
                ->add('_action', 'actions', array(
                    'actions' => array(
                        'view' => array(),
                        'edit' => array(),
                        'delete' => array(),
                    )
                ))
        ;
    }

    public function getNewInstance()
    {
        $instance = parent::getNewInstance();
        $instance->setActive(true);

        return $instance;
    }
}