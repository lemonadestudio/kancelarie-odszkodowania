<?php

namespace Lm\CmsBundle\Admin;

use Sonata\AdminBundle\Admin\Admin;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Form\FormMapper;




class OfferCommentAdmin extends Admin {

    protected $translationDomain = 'LmCmsBundle';
    protected $datagridValues = array(
        '_page' => 1,
        '_per_page' => 50,
        '_sort_by' => 'created',
        '_sort_order' => 'DESC',
    );


    protected function configureRoutes(\Sonata\AdminBundle\Route\RouteCollection $collection)
    {
        $collection->remove('create');
    }
   

    protected function configureFormFields(FormMapper $formMapper) {
       

        $formMapper
                
                ->add('comment', 'textarea', array('required' => true))
                ->add('buy_date', null, array('required' => false))
                ->add('buy_object', null, array('required' => false))
                ->add('positive',  'choice', array('choices' => array('0' => 'Nie', '1' => 'Tak')))
                ->add('approved')
                ->add('client_ip')
              
              
                ->setHelps(array(
                    
                ))
        ;
    }


    protected function configureDatagridFilters(DatagridMapper $datagridMapper) {
        $datagridMapper
               
                ->add('positive', null, array(), 'choice', array('choices' => array('0' => 'Nie', '1' => 'Tak')))
                ->add('approved')

        ;
    }

    protected function configureListFields(ListMapper $listMapper) {
        $listMapper
                ->addIdentifier('comment')
                ->add('offer')
                ->add('positive', 'boolean')
                ->add('approved')
                ->add('created')
                ->add('client_ip')
                
                
                ->add('_action', 'actions', array(
                    'actions' => array(
                        // 'view' => array(),
                        'edit' => array(),
                        'delete' => array(),
                    )
                ))

        ;
    }

}