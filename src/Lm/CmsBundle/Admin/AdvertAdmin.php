<?php

namespace Lm\CmsBundle\Admin;

use Lm\CmsBundle\Entity\Advert;
use Lm\CmsBundle\Models\AdvertLocation;
use Doctrine\ORM\EntityRepository;
use Sonata\AdminBundle\Admin\AdminInterface;
use Sonata\AdminBundle\Route\RouteCollection;
use Sonata\AdminBundle\Show\ShowMapper;
use Sonata\AdminBundle\Admin\Admin;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Validator\ErrorElement;
use Sonata\AdminBundle\Form\FormMapper;
use Knp\Menu\FactoryInterface as MenuFactoryInterface;
use Knp\Menu\ItemInterface as MenuItemInterface;
use Knp\Menu\MenuItem;

class AdvertAdmin extends Admin {

    protected $translationDomain = 'LmCmsBundle';
    protected $datagridValues = array(
        '_page' => 1,
        '_per_page' => 25,
        '_sort_by' => 'publishDate',
        '_sort_order' => 'DESC',
    );

    protected function configureSideMenu(MenuItemInterface $menu, $action, AdminInterface $childAdmin = null) {
        if (!$childAdmin && !in_array($action, array('edit'))) {
            return;
        }
        $admin = $this->isChild() ? $this->getParent() : $this;
        $id = $admin->getRequest()->get('id');
        // $_object = $this->getObject($id);
        $menu->addChild('Edytuj', array('uri' => $this->generateUrl('edit', array('id' => $id))));
    }

    protected function configureRoutes(RouteCollection $collection) {

//     	$collection->add('edit_rows', '{id}/edit_rows');
    }

    public function getTemplate($title) {
        switch ($title) {
            case 'edit':
                return 'LmCmsBundle:Admin\Advert:edit.html.twig';
                break;
// 			case 'show':
// 				return 'LmCmsBundle:Admin\Aktualnosci:show.html.twig';
// 				break;

            default:
                return parent::getTemplate($title);
                break;
        }
    }

    protected function configureFormFields(FormMapper $formMapper) {

        $formMapper
                ->with('label.mainoptions')
                ->add('title', null, array('required' => true))
                ->add('url', null, array('required' => false))
                ->add('isActive', 'checkbox', array('required' => false, 'label' => 'Czy aktywne?'))
                ->add('_file_image', 'file', array('required' => false, 'label' => 'label.main_photo'))
                ->add('location', 'choice', array(
                    'required' => true, 
                    'label' => 'Miejsce występowania reklamy',
                    'choices' => $this->getAdvertLocations()
                ))
                ->setHelps(array(
                    'title' => $this->trans('help.title'),
                    'slug' => $this->trans('help.slug'),
                    '_file_image' => 'Jeżeli zdjęcie będzie gif-em, to zostanie przycięte do wymaganego rozmiaru. W innym przypadku zostanie automatycznie przeskalowane.',
                    'location' => 'Rozmiary, do których zdjęcie zostanie przycięte lub przeskalowane to: <br /> - dla "panelu po lewej stronie" 200x200 px <br /> - dla "strony głównej po okruszkami" 970x90 px'
                ))
        ;
    }

    public function configureShowFields(ShowMapper $showMapper) {

        $showMapper
                ->add('title')
                ->add('isActive')

        ;
    }

    protected function configureDatagridFilters(DatagridMapper $datagridMapper) {
        $datagridMapper
                ->add('title')

        ;
    }

    protected function configureListFields(ListMapper $listMapper) {
        $listMapper
                ->addIdentifier('title')
                ->add('slug')
                ->add('_action', 'actions', array(
                    'actions' => array(
                        'view' => array(),
                        'edit' => array(),
                        'delete' => array(),
                    )
                ))

        ;
    }
    
    private function getAdvertLocations() {
        return array(
            AdvertLocation::LEFT_SIDE_BAR => 'W panelu po lewej stronie.',
            AdvertLocation::HOMEPAGE_TOP_UNDER_BREADCRUMBS => 'Na górze strony głównej, pod okruszkami.'
        );
    }

}