<?php

namespace Lm\CmsBundle\Admin;

use Lm\CmsBundle\Entity\OfferCategoryAdvert;
use Doctrine\ORM\EntityRepository;
use Sonata\AdminBundle\Admin\AdminInterface;
use Sonata\AdminBundle\Route\RouteCollection;
use Sonata\AdminBundle\Show\ShowMapper;
use Sonata\AdminBundle\Admin\Admin;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Validator\ErrorElement;
use Sonata\AdminBundle\Form\FormMapper;

class OfferCategoryAdvertAdmin extends Admin {

    protected $translationDomain = 'LmCmsBundle';
    protected $datagridValues = array(
        '_page' => 1,
        '_per_page' => 25,
        '_sort_by' => 'publishDate',
        '_sort_order' => 'DESC',
    );

    protected function configureRoutes(RouteCollection $collection) {

//     	$collection->add('edit_rows', '{id}/edit_rows');
    }

    public function getTemplate($title) {
        switch ($title) {
            case 'edit':
                return 'LmCmsBundle:Admin\OfferCategoryAdvert:edit.html.twig';
                break;
            default:
                return parent::getTemplate($title);
                break;
        }
    }

    protected function configureFormFields(FormMapper $formMapper) {

        $formMapper
                ->with('label.mainoptions')
                ->add('title', null, array('required' => true))
                ->add('url', null, array('required' => false))
                ->add('isActive', 'checkbox', array('required' => false, 'label' => 'Czy aktywne?'))
                ->add('_file_image', 'file', array('required' => false, 'label' => 'label.main_photo'))
                ->setHelps(array(
                    'title' => $this->trans('help.title'),
                    'slug' => $this->trans('help.slug'),
                    '_file_image' => 'Jeżeli zdjęcie będzie gif-em, to zostanie przycięte do rozmiaru 728x90 px. W innym przypadku zostanie automatycznie przeskalowane do rozmiaru 728x90 px.'
                ))
        ;
    }

    public function configureShowFields(ShowMapper $showMapper) {

        $showMapper
                ->add('title')
                ->add('isActive')
        ;
    }

    protected function configureDatagridFilters(DatagridMapper $datagridMapper) {
        $datagridMapper
                ->add('title')
        ;
    }

    protected function configureListFields(ListMapper $listMapper) {
        $listMapper
                ->addIdentifier('title')
                ->add('isActive')
                ->add('_action', 'actions', array(
                    'actions' => array(
                        'view' => array(),
                        'edit' => array(),
                        'delete' => array(),
                    )
                ))

        ;
    }
    
}