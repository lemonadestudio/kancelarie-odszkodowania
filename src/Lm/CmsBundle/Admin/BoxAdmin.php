<?php

namespace Lm\CmsBundle\Admin;

use Sonata\AdminBundle\Admin\Admin;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Validator\ErrorElement;
use Sonata\AdminBundle\Form\FormMapper;

class BoxAdmin extends Admin {

//    protected $translationDomain = 'LmCmsBundle';


	public function getTemplate($name)
	{
		switch ($name) {
			case 'edit':
				return 'LmCmsBundle:Admin\Box:edit.html.twig';
				break;
			default:
				return parent::getTemplate($name);
				break;
		}
	}


    protected function configureFormFields(FormMapper $formMapper)
    {



        $formMapper

                ->add('title', null, array('required' => true))
                ->add('content', null, array('required' => false, 'attr' => array('class' => 'sonata-medium wysiwyg')))
                ->add('published', 'checkbox', array('required' => false))
                ->add('publishDate', 'datetime')
                ->add('slug', null, array('required' => false))
                ->add('lang', 'choice', array(
                    'choices' => array("pl"=>"Polski", "en"=>"English"),
                ))

        -> setHelps(array(
                    'title' => $this->trans('Enter page title'),
                    'slug' => $this->trans('help.slug'),
                    'publishDate' => $this->trans('help.publishDate'),

           ));
    }

    protected function configureDatagridFilters(DatagridMapper $datagridMapper)
    {
        $datagridMapper
            ->add('title')
            ->add('published')
        ;
    }

    protected function configureListFields(ListMapper $listMapper)
    {
        $listMapper
            ->addIdentifier('title')
            ->add('slug')
            ->add('published')
            ->add('publishDate')
        ;
    }


}