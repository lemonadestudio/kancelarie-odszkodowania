<?php

namespace Lm\CmsBundle\Admin;

use Sonata\AdminBundle\Admin\AdminInterface;
use Sonata\AdminBundle\Route\RouteCollection;
use Sonata\AdminBundle\Show\ShowMapper;
use Sonata\AdminBundle\Admin\Admin;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Validator\ErrorElement;
use Sonata\AdminBundle\Form\FormMapper;

class OfferAdditionalAddressAdmin extends Admin 
{
    protected $translationDomain = 'LmCmsBundle';
    
    protected function configureFormFields(FormMapper $formMapper) 
    {
        $formMapper
                ->add('city', null, array(
                    'required' => false, 
                    'attr' => array(
                        'class' => 'span12'
                    )
                ))
                ->add('street', null, array(
                    'required' => false, 
                    'attr' => array(
                        'class' => 'span12'
                    )
                ))
                ->add('postCode', null, array('required' => false))
                ->add('phone', null, array(
                    'required' => false, 
                    'attr' => array(
                        'class' => 'span8'
                    )
                ))
                ->add('email', null, array(
                    'required' => false, 
                    'attr' => array(
                        'class' => 'span12'
                    )
                ))
        ;
    }

    public function configureShowFields(ShowMapper $showMapper) 
    {
        $showMapper
                ->add('city')
        ;
    }

    protected function configureListFields(ListMapper $listMapper) 
    {
        $listMapper
                ->addIdentifier('city')
                ->add('_action', 'actions', array(
                    'actions' => array()
                ))
        ;
    }
}