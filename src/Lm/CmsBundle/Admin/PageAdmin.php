<?php

namespace Lm\CmsBundle\Admin;

use Lm\CmsBundle\Entity\Page;

use Doctrine\ORM\EntityRepository;
use Sonata\AdminBundle\Admin\AdminInterface;
use Sonata\AdminBundle\Route\RouteCollection;
use Sonata\AdminBundle\Show\ShowMapper;
use Sonata\AdminBundle\Admin\Admin;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Validator\ErrorElement;
use Sonata\AdminBundle\Form\FormMapper;

use Knp\Menu\FactoryInterface as MenuFactoryInterface;
use Knp\Menu\ItemInterface as MenuItemInterface;
use Knp\Menu\MenuItem;

class PageAdmin extends Admin {

    protected $translationDomain = 'LmCmsBundle';

    protected $datagridValues = array(
    		'_page'       => 1,
    		'_per_page'   => 25,
    		'_sort_by' => 'publishDate',
    		'_sort_order' => 'DESC',

    );

    protected function configureSideMenu(MenuItemInterface $menu, $action, AdminInterface $childAdmin = null)
    {
		if (!$childAdmin && !in_array($action, array('edit'))) {
        	return;
    	}
		    $admin = $this->isChild() ? $this->getParent() : $this;
		    $id = $admin->getRequest()->get('id');
		    // $_object = $this->getObject($id);
		    $menu->addChild('Edytuj', array('uri' => $this->generateUrl('edit', array('id' => $id))));






    }

    protected function configureRoutes(RouteCollection $collection)
    {

//     	$collection->add('edit_rows', '{id}/edit_rows');


    }

	public function getTemplate($name)
	{
		switch ($name) {
			case 'edit':
				return 'LmCmsBundle:Admin\Page:edit.html.twig';
				break;
// 			case 'show':
// 				return 'LmCmsBundle:Admin\Aktualnosci:show.html.twig';
// 				break;

			default:
				return parent::getTemplate($name);
				break;
		}
	}


    protected function configureFormFields(FormMapper $formMapper)
    {

    	$this->getSubject()->setUpdated( new \DateTime('now'));
        $formMapper

	        	->with('label.mainoptions')


	        		->add('title', null, array('required' => true))
	        		->add('content', null, array('required' => false, 'attr' => array('class' => 'sonata-medium wysiwyg')))
					->add('published', null, array('required' => false))
					->add('publishDate', null, array('required' => false))
					->add('keywords', 'textarea', array('required' => false, 'label' => 'label.keywords', 'attr' => array('class' => 'span8', 'rows' => 3)))
                	->add('description', 'textarea', array('required' => false, 'label' => 'label.description', 'attr' => array('class' => 'span10', 'rows' => 10)))

//					->add('galeria', null, array('required' => false, 'label' => 'label.gallery'))



					->add('_file_image', 'file', array('required' => false, 'label' => 'label.main_photo'))
					->add('slug', null, array('required' => false))
					->add('automaticSeo')
					->add('updated', null, array('label' => ' ', 'with_seconds' => 'true',  'attr'=>array('style'=>'display:none;')))


      	-> setHelps(array(
        				'title' => $this->trans('help.title'),
        				'automaticSeo' => $this->trans('help.automatic_seo'),
        				'slug' => $this->trans('help.slug'),
        				'publishDate' => $this->trans('help.publish_date'),

        		))
      ;

    }

    public function configureShowFields(ShowMapper $showMapper) {

    	$showMapper

	    	->add('title')
	    	->add('content', null, array('safe' => true))

	    	->add('published')
	    	->add('publishDate')

	    	->add('keywords')
	    	->add('description')
	    	->add('created')
	    	->add('updated')

    	;
    }

    protected function configureDatagridFilters(DatagridMapper $datagridMapper)
    {
        $datagridMapper

            ->add('title')

        ;
    }

    protected function configureListFields(ListMapper $listMapper)
    {
        $listMapper

            ->addIdentifier('title')
            ->add('photo', 'string', array('template' => 'LmCmsBundle:Admin\List:obrazek.html.twig'))
            ->add('slug')
            ->add('published')
            ->add('publishDate')
            ->add('updated')

            ->add('_action', 'actions', array(
            		'actions' => array(
            				'view' => array(),
            				'edit' => array(),
            				'delete' => array(),

            				)
            	))

        ;
    }




}