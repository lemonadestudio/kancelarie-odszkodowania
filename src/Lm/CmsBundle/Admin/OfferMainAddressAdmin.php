<?php

namespace Lm\CmsBundle\Admin;

use Sonata\AdminBundle\Admin\AdminInterface;
use Sonata\AdminBundle\Route\RouteCollection;
use Sonata\AdminBundle\Show\ShowMapper;
use Sonata\AdminBundle\Admin\Admin;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Validator\ErrorElement;
use Sonata\AdminBundle\Form\FormMapper;
use Lm\CmsBundle\Util\Province;

class OfferMainAddressAdmin extends Admin 
{
    protected $translationDomain = 'LmCmsBundle';
    
    protected function configureFormFields(FormMapper $formMapper) 
    {
        $formMapper
                ->add('city', null, array('required' => false))
                ->add('street', null, array('required' => false))
                ->add('postCode', null, array('required' => false))
                ->add('province', 'choice', array(
                    'required' => false,
                    'choices' => Province::getProvinces()
                ))
                ->add('phone', null, array('required' => false))
                ->add('phone2', null, array('required' => false))
                ->add('email', null, array('required' => false))
                ->add('email2', null, array('required' => false))
        ;
    }

    public function configureShowFields(ShowMapper $showMapper) 
    {
        $showMapper
                ->add('city')
        ;
    }

    protected function configureListFields(ListMapper $listMapper) 
    {
        $listMapper
                ->addIdentifier('city')
                ->add('_action', 'actions', array(
                    'actions' => array()
                ))
        ;
    }
}