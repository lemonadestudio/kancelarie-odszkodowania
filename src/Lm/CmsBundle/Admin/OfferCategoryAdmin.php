<?php

namespace Lm\CmsBundle\Admin;

use Lm\CmsBundle\Entity\OfferCategory;
use Doctrine\ORM\EntityRepository;
use Sonata\AdminBundle\Admin\AdminInterface;
use Sonata\AdminBundle\Route\RouteCollection;
use Sonata\AdminBundle\Show\ShowMapper;
use Sonata\AdminBundle\Admin\Admin;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Validator\ErrorElement;
use Sonata\AdminBundle\Form\FormMapper;
use Knp\Menu\FactoryInterface as MenuFactoryInterface;
use Knp\Menu\ItemInterface as MenuItemInterface;
use Knp\Menu\MenuItem;

class OfferCategoryAdmin extends Admin {

    protected $translationDomain = 'LmCmsBundle';
    protected $datagridValues = array(
        '_page' => 1,
        '_per_page' => 25,
        '_sort_by' => 'position',
        '_sort_order' => 'ASC',
    );

    protected function configureSideMenu(MenuItemInterface $menu, $action, AdminInterface $childAdmin = null) {
        if (!$childAdmin && !in_array($action, array('edit'))) {
            return;
        }
        $admin = $this->isChild() ? $this->getParent() : $this;
        $id = $admin->getRequest()->get('id');
        // $_object = $this->getObject($id);
        $menu->addChild('Edytuj', array('uri' => $this->generateUrl('edit', array('id' => $id))));
    }

    protected function configureRoutes(RouteCollection $collection) 
    {
        $collection->add('moveUp', $this->getRouterIdParameter() . '/moveup');
        $collection->add('moveDown', $this->getRouterIdParameter() . '/movedown');
    }

    public function getTemplate($title) {
        switch ($title) {
            case 'edit':
                return 'LmCmsBundle:Admin\OfferCategory:edit.html.twig';
            default:
                return parent::getTemplate($title);
        }
    }

    protected function configureFormFields(FormMapper $formMapper) {

        $formMapper
                ->with('label.mainoptions')
                ->add('name', null, array('required' => true))
                ->add('description', null, array('required' => false, 'label' => 'Krótki opis'))
                ->add('slug', null, array('required' => false))
                ->add('_file_image', 'file', array('required' => false, 'label' => 'label.main_photo'))
                ->add('advert', null, array('required' => false, 'label' => 'Reklama'))
                ->add('metaDescription', null, array('required' => false, 'label' => 'Meta description'))
                ->setHelps(array(
                    'name' => $this->trans('help.title'),
                    'slug' => $this->trans('help.slug')
                ))
        ;
    }

    public function configureShowFields(ShowMapper $showMapper) {

        $showMapper
                ->add('name')

        ;
    }

    protected function configureDatagridFilters(DatagridMapper $datagridMapper) {
        $datagridMapper
                ->add('name')

        ;
    }

    protected function configureListFields(ListMapper $listMapper) {
        $listMapper
                ->addIdentifier('name')
                ->add('slug')
                ->add('_action', 'actions', array(
                        'actions' => array(
                        'view' => array(),
                        'edit' => array(),
                        'delete' => array(),
                        'moveUp' => array(),
                        'moveDown' => array(),
                    )
                ))

        ;
    }

}