<?php 

namespace Lm\CmsBundle\Menu;

use Symfony\Component\DependencyInjection\ContainerInterface;

class Search {
	
	
	private $container;
	private $method;
	
	public function __construct(ContainerInterface $container, $method = '')
	{
		$this->container = $container;
		$this->method = $method;
	}
	
	public function search($search) {
		
		
		switch($this->method) {
			case 'podstrona' : 
				return $this->podstrona();
			
			default: 
				break;
		}
		
		return array();
	}
	
	public function podstrona($search = '') {
		
		$em = $this->container->get('doctrine')->getEntityManager();
		
		$search = mb_strtolower($search);
		
		$strony = $em->createQuery(
				"
				SELECT s
				FROM   LmCmsBundle:Page s
				WHERE  LOWER(s.title) LIKE :search
				ORDER  BY s.title ASC
				"
		) -> setParameter('search', '%'.$search.'%')->execute(); 
		
		
		$ret_arr = array();
		foreach($strony as $strona) {
			$ret_arr[] = array(
                            'parameters' => array('slug' => $strona->getSlug()), 
                            'title' => $strona->getTitle()
                        );
		}
		
		return $ret_arr;
	}
	
	
}