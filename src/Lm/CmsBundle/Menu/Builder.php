<?php

namespace Lm\CmsBundle\Menu;

use Lm\CmsBundle\Entity\MenuItem;

use Symfony\Component\Routing\RouteCollection;
use Symfony\Bundle\FrameworkBundle\Routing\Router;
use Knp\Menu\FactoryInterface;
use Knp\Menu\ItemInterface;
use Symfony\Component\DependencyInjection\ContainerAware;

/**
 * Description of Builder
 *
 * @author przemq
 */
class Builder extends ContainerAware {


	var $depth = 1;
	var $curr_depth = 1;


    public function managedMenu(FactoryInterface $factory, $options = array()) {


        $menu = $factory->createItem('root');

        $request = $this->container->get('request');
        $translator = $this->container->get('translator');
        $doctrine = $this->container->get('doctrine');
        $router = $this->container->get('router');


//         $current_uri = $this->container->get('request')->getRequestUri();



        if(array_key_exists('REDIRECT_URL', $_SERVER)) {
            $menu->setCurrentUri($_SERVER['REDIRECT_URL']);
        } else {
            $current_uri = $this->container->get('request')->getRequestUri();
            // $menu->setCurrentUri($current_uri);
        }


        $em = $this->container->get('doctrine')->getEntityManager();

        $location = $options['location'];
        $this->depth = array_key_exists('depth', $options) ? $options['depth'] : 1;

"
        		SELECT m
        		FROM LmCmsBundle:MenuItem m

        		WHERE
                        m.location = :location
        		AND m.depth = 1
        		ORDER BY m.lft ASC, m.title ASC";

        $menu_items = $em->createQuery("
        		SELECT m
        		FROM LmCmsBundle:MenuItem m

        		WHERE
                        m.location = :location
        		AND m.depth = 1
        		ORDER BY m.lft ASC, m.title ASC")
        ->setParameter('location', $location)
        ->execute();


        foreach($menu_items as $item)
        {
            $this->buildMenu($item, $menu);
        }


        return $menu;
    }

    /**
     *
     * @param MenuItem $item
     * @param  ItemInterface $menu
     */
    public function buildMenu($item, &$menu)
    {



        $item_arr = $this->getMenuItemArray($item);


        if(!$item_arr) {
                return;
        }


        $name = $menu -> addChild($item_arr['title'], array(
                'uri' => $item_arr['uri'],
	                	'linkAttributes' => array_merge(array('title' => $item_arr['title']), $item_arr['linkAttributes'])
	        			)
	        		)
                	->getName();
                ;


//               $ch = $menu->getChild($name);
//               var_dump($name, $ch);

                /*
                 * wczytywanie drzeewka w dol
                 *
                 */



                if($this->curr_depth < $this->depth) {

	               if($item->getChildItems())
	               {
	               	   $this->curr_depth ++;
	                   $ch = $menu->getChild($name);
	                   foreach($item->getChildItems() as $item1)
	                   {
	                       $this->buildMenu($item1, $ch);
	                   }

	                   $this->curr_depth --;

	               }

                }




    }


 public function getMenuItemArray($item) {

    	$router = $this->container->get('router');
    	if(!$item->getUrl() && !$item->getRoute()){

    		return false;

    	}

    	$uri = false;
    	switch($item->getType()) {

    		case 'url':
    			$uri = $item->getUrl();
    			break;

    		case 'route':

    			if($item->getRoute()) {

    				$route = $router->getRouteCollection()->get($item->getRoute());
    				if($route) {

    					$parameters = array();
    					foreach($item->getRouteParametersArray() as $k => $param) {
							if(preg_match('@\{'.$k.'\}@', $route->getPath())) {
								$parameters[$k] = $param;
							}
    					}

    					try{
    						$uri = $router->generate($item->getRoute(), $parameters );
    					} catch(\Exception $e) {
    						// return false;
    					}
    				}
    			}
    			break;

    		default:
    			break;

    	}

    	if($uri === false) {
    		return false;
    	}

    	return array(
    			'anchor' => $item->getAnchor(),
    			'title' => $item->getTitle(),
    			'uri' => $uri,

    			'linkAttributes' => array(),//$item->getAttributesArray(),
    	);


    }

}
