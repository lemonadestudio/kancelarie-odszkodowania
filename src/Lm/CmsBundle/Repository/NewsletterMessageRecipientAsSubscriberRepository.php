<?php

namespace Lm\CmsBundle\Repository;

use Doctrine\ORM\EntityRepository;

class NewsletterMessageRecipientAsSubscriberRepository extends EntityRepository 
{
    
    public function getRecipientsToSendMessage($limit = 1) 
    {
        return $this->getEntityManager()
                ->createQueryBuilder('r')
                ->from('LmCmsBundle:NewsletterMessageRecipientAsSubscriber', 'r')
                ->leftJoin('r.newsletterMessage', 'm')
                ->select('r, m')
                ->where('r.sentDate IS NULL')
                ->andWhere('m.startSendDate <= :now OR m.startSendDate IS NULL')
                ->orderBy('r.id', 'ASC')
                ->setMaxResults($limit)
                ->setParameter('now', new \DateTime('now'))
                ->getQuery()
                ->execute();
    }
}
