<?php

namespace Lm\CmsBundle\Repository;

use Doctrine\ORM\EntityRepository;

class NewsletterSubscriberRepository extends EntityRepository 
{

    public function getAllAsArray() 
    {
        return $this->getEntityManager()
                ->createQueryBuilder('c')
                ->select('c.id, c.email')
                ->from('LmCmsBundle:NewsletterSubscriber', 'c')
                ->orderBy('c.id', 'ASC')
                ->getQuery()
                ->getArrayResult();
    }
    
    public function getAllActiveAsArray() 
    {
        return $this->getEntityManager()
                ->createQueryBuilder('c')
                ->select('c.id, c.email')
                ->from('LmCmsBundle:NewsletterSubscriber', 'c')
                ->where('c.isActive = true')
                ->orderBy('c.id', 'ASC')
                ->getQuery()
                ->getArrayResult();
    }
}
