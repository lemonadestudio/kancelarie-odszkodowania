<?php

namespace Lm\CmsBundle\Extensions\Doctrine;

use Doctrine\ORM\Query\AST\Functions\FunctionNode,
    Doctrine\ORM\Query\Lexer;

class Year extends FunctionNode 
{
    public $datetimeExpression = null;

    public function parse(\Doctrine\ORM\Query\Parser $parser) 
    {
        $parser->match(Lexer::T_IDENTIFIER);
        $parser->match(Lexer::T_OPEN_PARENTHESIS);
        $this->datetimeExpression = $parser->ArithmeticPrimary();
        $parser->match(Lexer::T_CLOSE_PARENTHESIS);
    }

    public function getSql(\Doctrine\ORM\Query\SqlWalker $sql_walker) 
    {
        return sprintf('YEAR(%s)', $this->datetimeExpression->dispatch($sql_walker));
    }
}
