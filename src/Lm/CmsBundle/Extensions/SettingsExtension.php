<?php

namespace Lm\CmsBundle\Extensions;

use Lm\CmsBundle\Util\Config;


class SettingsExtension extends \Twig_Extension
{
    protected $cms_config;

    function __construct(Config $config) {
        $this->cms_config = $config;
    }

    public function getGlobals() {
        return array(
            'settings' => $this->cms_config->all(),
        	'cms_config' => $this->cms_config
        );
    }

    public function getName()
    {
        return 'cms_config';
    }

}