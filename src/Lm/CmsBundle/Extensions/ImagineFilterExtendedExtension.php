<?php

namespace Lm\CmsBundle\Extensions;

use Liip\ImagineBundle\Templating\ImagineExtension;
use Liip\ImagineBundle\Imagine\Cache\CacheManager;

class ImagineFilterExtendedExtension extends ImagineExtension
{
    /**
     * Constructor.
     *
     * @param CacheManager $cacheManager
     */
    public function __construct(CacheManager $cacheManager)
    {
        parent::__construct($cacheManager);
    }

    /**
     * {@inheritDoc}
     */
    public function getFilters()
    {
        return array(
            'imagine_filter_extended' => new \Twig_Filter_Method($this, 'filter'),
        );
    }

    /**
     * Gets the browser path for the image and filter to apply.
     *
     * @param string $path
     * @param string $filter
     * @param boolean $absolute
     *
     * @return string
     */
    public function filter($path, $filter, $absolute = false, $disallowedExtensions = array())
    {
        if ($disallowedExtensions) {
            if (!is_array($disallowedExtensions)) {
                $disallowedExtensions = array($disallowedExtensions);
            }

            $pathParts = explode('.', $path);
            $extension = $pathParts[count($pathParts) - 1];

            if (in_array($extension, $disallowedExtensions)) {
                return $path;
            }
        }
        
        return parent::filter($path, $filter, $absolute);
    }

    /**
     * {@inheritDoc}
     */
    public function getName()
    {
        return 'imagine_filter_extended_extension';
    }
}