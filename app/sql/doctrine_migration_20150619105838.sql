# Doctrine Migration File Generated on 2015-06-19 10:06:38

# Version 20150619105448
ALTER TABLE advert ADD location INT DEFAULT NULL;
UPDATE advert SET location = 1;
ALTER TABLE offer CHANGE active active TINYINT(1) DEFAULT '0' NOT NULL;
