# Doctrine Migration File Generated on 2016-03-17 10:03:18

# Version 20160317094432
ALTER TABLE offer_category ADD position INT DEFAULT '0' NOT NULL;

            SET @rownumber = 0;
            UPDATE offer_category 
            SET position = (@rownumber := @rownumber + 1)
            ORDER BY id ASC;;
ALTER TABLE offer_category CHANGE position position INT NOT NULL;
