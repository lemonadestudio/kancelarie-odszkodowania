# Doctrine Migration File Generated on 2016-03-18 12:03:12

# Version 20160318122149
CREATE TABLE offer_promotion_expiration_notification (id INT AUTO_INCREMENT NOT NULL, offer_id INT DEFAULT NULL, type INT NOT NULL, forExpirationDate DATETIME NOT NULL, created DATETIME NOT NULL, sentDate DATETIME DEFAULT NULL, INDEX IDX_E12FFE1A53C674EE (offer_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB;
ALTER TABLE offer_promotion_expiration_notification ADD CONSTRAINT FK_E12FFE1A53C674EE FOREIGN KEY (offer_id) REFERENCES offer (id);
ALTER TABLE offer ADD expirationOfPromotionForVisibleProperties DATETIME DEFAULT NULL, ADD expirationOfPromotionForAdditionalAddresses DATETIME DEFAULT NULL;
