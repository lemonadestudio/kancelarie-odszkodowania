# Doctrine Migration File Generated on 2016-02-17 14:02:14

# Version 20160217144639
CREATE TABLE offer_visible_property (offer_id INT NOT NULL, offerproperty_id INT NOT NULL, INDEX IDX_50FF858753C674EE (offer_id), INDEX IDX_50FF8587AB732040 (offerproperty_id), PRIMARY KEY(offer_id, offerproperty_id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB;
CREATE TABLE offer_property (id INT AUTO_INCREMENT NOT NULL, type INT NOT NULL, description VARCHAR(255) NOT NULL, created DATETIME DEFAULT NULL, updated DATETIME DEFAULT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB;
ALTER TABLE offer_visible_property ADD CONSTRAINT FK_50FF858753C674EE FOREIGN KEY (offer_id) REFERENCES offer (id) ON DELETE CASCADE;
ALTER TABLE offer_visible_property ADD CONSTRAINT FK_50FF8587AB732040 FOREIGN KEY (offerproperty_id) REFERENCES offer_property (id) ON DELETE CASCADE;
INSERT INTO offer_property (`type`, description, created) VALUES (1, 'Telefon', CURDATE());
INSERT INTO offer_property (`type`, description, created) VALUES (2, 'Dodatkowy telefon', CURDATE());
INSERT INTO offer_property (`type`, description, created) VALUES (3, 'Email', CURDATE());
INSERT INTO offer_property (`type`, description, created) VALUES (4, 'Dodatkowy email', CURDATE());
INSERT INTO offer_property (`type`, description, created) VALUES (5, 'Strona WWW', CURDATE());
INSERT INTO offer_property (`type`, description, created) VALUES (6, 'Adres do facebooka', CURDATE());
