# Doctrine Migration File Generated on 2014-02-19 12:02:36

# Version 20140218155748
CREATE TABLE offer_to_offer_category (offer_id INT NOT NULL, offercategory_id INT NOT NULL, INDEX IDX_1892971053C674EE (offer_id), INDEX IDX_18929710EDA7AE4E (offercategory_id), PRIMARY KEY(offer_id, offercategory_id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB;
CREATE TABLE offer_category (id INT AUTO_INCREMENT NOT NULL, name VARCHAR(255) NOT NULL, type SMALLINT DEFAULT NULL, slug VARCHAR(255) DEFAULT NULL, image VARCHAR(255) DEFAULT NULL, created DATETIME DEFAULT NULL, updated DATETIME DEFAULT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB;
ALTER TABLE offer_to_offer_category ADD CONSTRAINT FK_1892971053C674EE FOREIGN KEY (offer_id) REFERENCES offer (id) ON DELETE CASCADE;
ALTER TABLE offer_to_offer_category ADD CONSTRAINT FK_18929710EDA7AE4E FOREIGN KEY (offercategory_id) REFERENCES offer_category (id) ON DELETE CASCADE;
DROP TABLE offer_categories;
ALTER TABLE offer DROP category, CHANGE active active TINYINT(1) DEFAULT '0' NOT NULL;
CREATE INDEX offer_slug_index ON offer (slug);
CREATE INDEX offer_type_index ON offer (type);
CREATE INDEX offer_active_index ON offer (active);
CREATE INDEX offer_city_index ON offer (city);
CREATE INDEX offer_active_city_index ON offer (city, active);
CREATE INDEX offer_category_name_index ON offer_category (name);
CREATE INDEX offer_category_type_index ON offer_category (type);
