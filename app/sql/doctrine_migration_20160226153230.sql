# Doctrine Migration File Generated on 2016-02-26 15:02:30

# Version 20160226153046
ALTER TABLE coordinates ADD latitude DOUBLE PRECISION NOT NULL, ADD longitude DOUBLE PRECISION NOT NULL, DROP cord_1, DROP cord_2;
ALTER TABLE offer CHANGE createdUsingImport createdUsingImport TINYINT(1) NOT NULL;
