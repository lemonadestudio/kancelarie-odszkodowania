# Doctrine Migration File Generated on 2014-02-17 13:02:39

# Version 20140217124405
UPDATE offer SET province = 1 WHERE LOWER(province) = 'dolnośląskie';
UPDATE offer SET province = 2 WHERE LOWER(province) = 'kujawsko-pomorskie';
UPDATE offer SET province = 3 WHERE LOWER(province) = 'lubelskie';
UPDATE offer SET province = 4 WHERE LOWER(province) = 'lubuskie';
UPDATE offer SET province = 5 WHERE LOWER(province) = 'łódzkie';
UPDATE offer SET province = 6 WHERE LOWER(province) = 'małopolskie';
UPDATE offer SET province = 7 WHERE LOWER(province) = 'mazowieckie';
UPDATE offer SET province = 8 WHERE LOWER(province) = 'opolskie';
UPDATE offer SET province = 9 WHERE LOWER(province) = 'podkarpackie';
UPDATE offer SET province = 10 WHERE LOWER(province) = 'podlaskie';
UPDATE offer SET province = 11 WHERE LOWER(province) = 'pomorskie';
UPDATE offer SET province = 12 WHERE LOWER(province) = 'śląskie';
UPDATE offer SET province = 13 WHERE LOWER(province) = 'świętokrzyskie';
UPDATE offer SET province = 14 WHERE LOWER(province) = 'warmińsko-mazurskie';
UPDATE offer SET province = 15 WHERE LOWER(province) = 'wielkopolskie';
UPDATE offer SET province = 16 WHERE LOWER(province) = 'zachodniopomorskie';
ALTER TABLE offer CHANGE province province SMALLINT DEFAULT NULL;
