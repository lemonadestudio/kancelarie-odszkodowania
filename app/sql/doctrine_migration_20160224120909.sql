# Doctrine Migration File Generated on 2016-02-24 12:02:09

# Version 20160224114927
CREATE TABLE offer_existence_notification (id INT AUTO_INCREMENT NOT NULL, offer_id INT DEFAULT NULL, forYearsInSystem INT NOT NULL, created DATETIME NOT NULL, sentDate DATETIME DEFAULT NULL, INDEX IDX_B84ACEEE53C674EE (offer_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB;
ALTER TABLE offer_existence_notification ADD CONSTRAINT FK_B84ACEEE53C674EE FOREIGN KEY (offer_id) REFERENCES offer (id);
ALTER TABLE offer ADD yearsInSystemForLastExistenceNotification INT DEFAULT NULL;
