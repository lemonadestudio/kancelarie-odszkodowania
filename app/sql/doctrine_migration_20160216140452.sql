# Doctrine Migration File Generated on 2016-02-16 14:02:52

# Version 20160216140115
ALTER TABLE offer CHANGE active active TINYINT(1) NOT NULL;
DROP INDEX offer_category_type_index ON offer_category;
ALTER TABLE offer_category DROP type;
