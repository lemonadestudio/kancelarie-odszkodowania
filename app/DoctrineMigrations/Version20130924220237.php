<?php

namespace Application\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration,
    Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your need!
 */
class Version20130924220237 extends AbstractMigration
{
    public function up(Schema $schema)
    {
        // this up() migration is autogenerated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() != "mysql");
        
        $this->addSql("CREATE TABLE email_template (name VARCHAR(255) NOT NULL, topic VARCHAR(255) DEFAULT NULL, content LONGTEXT DEFAULT NULL, content_txt LONGTEXT DEFAULT NULL, description LONGTEXT DEFAULT NULL, PRIMARY KEY(name)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB");
        $this->addSql("CREATE TABLE offer_message (id INT AUTO_INCREMENT NOT NULL, senderName VARCHAR(255) DEFAULT NULL, senderEmail VARCHAR(255) DEFAULT NULL, senderMessage LONGTEXT DEFAULT NULL, sentAt DATETIME DEFAULT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB");
        $this->addSql("CREATE TABLE offer_comment (id INT AUTO_INCREMENT NOT NULL, offer_id INT DEFAULT NULL, comment VARCHAR(1024) DEFAULT NULL, positive INT DEFAULT NULL, approved TINYINT(1) DEFAULT NULL, INDEX IDX_357C946553C674EE (offer_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB");
        $this->addSql("ALTER TABLE offer_comment ADD CONSTRAINT FK_357C946553C674EE FOREIGN KEY (offer_id) REFERENCES offer (id)");
        $this->addSql("DROP TABLE gallery");
        $this->addSql("DROP TABLE offer_comments");
        $this->addSql("ALTER TABLE offer_categories CHANGE id id INT AUTO_INCREMENT NOT NULL, CHANGE title title VARCHAR(255) DEFAULT NULL, CHANGE slug slug VARCHAR(255) DEFAULT NULL, CHANGE count count INT NOT NULL");
        $this->addSql("ALTER TABLE offer ADD cord_2 DOUBLE PRECISION NOT NULL, CHANGE target target INT NOT NULL, CHANGE view view INT NOT NULL, CHANGE active active INT NOT NULL");
    }

    public function down(Schema $schema)
    {
        // this down() migration is autogenerated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() != "mysql");
        
        $this->addSql("CREATE TABLE gallery (id INT AUTO_INCREMENT NOT NULL, title VARCHAR(255) NOT NULL, slug VARCHAR(255) NOT NULL, published TINYINT(1) NOT NULL, publishDate DATETIME DEFAULT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB");
        $this->addSql("CREATE TABLE offer_comments (id INT UNSIGNED AUTO_INCREMENT NOT NULL, offer_id INT UNSIGNED NOT NULL, comment VARCHAR(1024) NOT NULL, positive TINYINT(1) NOT NULL, aproved TINYINT(1) NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB");
        $this->addSql("DROP TABLE email_template");
        $this->addSql("DROP TABLE offer_message");
        $this->addSql("DROP TABLE offer_comment");
        $this->addSql("ALTER TABLE offer DROP cord_2, CHANGE target target INT DEFAULT 0 NOT NULL, CHANGE view view INT DEFAULT 0 NOT NULL, CHANGE active active INT DEFAULT 0 NOT NULL");
        $this->addSql("ALTER TABLE offer_categories CHANGE id id INT UNSIGNED AUTO_INCREMENT NOT NULL, CHANGE count count INT DEFAULT 0, CHANGE title title VARCHAR(1024) NOT NULL, CHANGE slug slug VARCHAR(1024) NOT NULL");
    }
}
