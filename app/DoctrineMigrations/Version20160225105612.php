<?php

namespace Application\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration,
    Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your need!
 */
class Version20160225105612 extends AbstractMigration
{
    public function up(Schema $schema)
    {
        // this up() migration is autogenerated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() != "mysql");
        
        $this->addSql("CREATE TABLE offer_additional_address (id INT AUTO_INCREMENT NOT NULL, offer_id INT DEFAULT NULL, city VARCHAR(200) NOT NULL, street VARCHAR(200) DEFAULT NULL, postCode VARCHAR(6) DEFAULT NULL, latitude DOUBLE PRECISION DEFAULT NULL, longitude DOUBLE PRECISION DEFAULT NULL, checksumOfLastCoordsCheck VARCHAR(32) DEFAULT NULL, phone VARCHAR(20) DEFAULT NULL, email VARCHAR(254) DEFAULT NULL, created DATETIME DEFAULT NULL, updated DATETIME DEFAULT NULL, INDEX IDX_DF17B91C53C674EE (offer_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB");
        $this->addSql("CREATE TABLE offer_main_address (id INT AUTO_INCREMENT NOT NULL, province SMALLINT DEFAULT NULL, phone2 VARCHAR(20) DEFAULT NULL, email2 VARCHAR(254) DEFAULT NULL, city VARCHAR(200) NOT NULL, street VARCHAR(200) DEFAULT NULL, postCode VARCHAR(6) DEFAULT NULL, latitude DOUBLE PRECISION DEFAULT NULL, longitude DOUBLE PRECISION DEFAULT NULL, checksumOfLastCoordsCheck VARCHAR(32) DEFAULT NULL, phone VARCHAR(20) DEFAULT NULL, email VARCHAR(254) DEFAULT NULL, created DATETIME DEFAULT NULL, updated DATETIME DEFAULT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB");
        $this->addSql("ALTER TABLE offer_additional_address ADD CONSTRAINT FK_DF17B91C53C674EE FOREIGN KEY (offer_id) REFERENCES offer (id)");
        $this->addSql("DROP INDEX offer_city_index ON offer");
        $this->addSql("DROP INDEX offer_active_city_index ON offer");
        $this->addSql("ALTER TABLE offer ADD mainAddress_id INT DEFAULT NULL, DROP street, DROP city, DROP postCode, DROP phone, DROP email, DROP province, DROP phone2, DROP email2");
        $this->addSql("ALTER TABLE offer ADD CONSTRAINT FK_29D6873ED7C0AA1A FOREIGN KEY (mainAddress_id) REFERENCES offer_main_address (id)");
        $this->addSql("CREATE UNIQUE INDEX UNIQ_29D6873ED7C0AA1A ON offer (mainAddress_id)");
    }

    public function down(Schema $schema)
    {
        // this down() migration is autogenerated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() != "mysql");
        
        $this->addSql("ALTER TABLE offer DROP FOREIGN KEY FK_29D6873ED7C0AA1A");
        $this->addSql("DROP TABLE offer_additional_address");
        $this->addSql("DROP TABLE offer_main_address");
        $this->addSql("DROP INDEX UNIQ_29D6873ED7C0AA1A ON offer");
        $this->addSql("ALTER TABLE offer ADD street VARCHAR(255) DEFAULT NULL, ADD city VARCHAR(255) DEFAULT NULL, ADD postCode VARCHAR(255) DEFAULT NULL, ADD phone VARCHAR(255) DEFAULT NULL, ADD email VARCHAR(255) DEFAULT NULL, ADD province SMALLINT DEFAULT NULL, ADD phone2 VARCHAR(255) DEFAULT NULL, ADD email2 VARCHAR(255) DEFAULT NULL, DROP mainAddress_id");
        $this->addSql("CREATE INDEX offer_city_index ON offer (city)");
        $this->addSql("CREATE INDEX offer_active_city_index ON offer (city, active)");
    }
}
