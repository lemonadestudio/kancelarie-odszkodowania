<?php

namespace Application\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration,
    Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your need!
 */
class Version20160226153046 extends AbstractMigration
{
    public function up(Schema $schema)
    {
        // this up() migration is autogenerated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() != "mysql");
        
        $this->addSql("ALTER TABLE coordinates ADD latitude DOUBLE PRECISION NOT NULL, ADD longitude DOUBLE PRECISION NOT NULL, DROP cord_1, DROP cord_2");
        $this->addSql("ALTER TABLE offer CHANGE createdUsingImport createdUsingImport TINYINT(1) NOT NULL");
    }

    public function down(Schema $schema)
    {
        // this down() migration is autogenerated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() != "mysql");
        
        $this->addSql("ALTER TABLE coordinates ADD cord_1 DOUBLE PRECISION DEFAULT NULL, ADD cord_2 DOUBLE PRECISION DEFAULT NULL, DROP latitude, DROP longitude");
        $this->addSql("ALTER TABLE offer CHANGE createdUsingImport createdUsingImport TINYINT(1) DEFAULT '0' NOT NULL");
    }
}
